# game-like Angel Dust

__Keymapping;__
* ENTER = confirm
* ESC = Open Menu / cancel
* SPACE = Jump (double jump with item)
* UP = Interact in game world
* LEFT/RIGHT = Movement left right
* LEFT CTRL = Attack
* LEFT SHIFT = Ranged attack (needs axe in inventory)
* 'q' = Ultimate form

Operate Menus with arrow keys
Playable with Controller (only tested with PS4 controller)

__Features:__
* Tilebased Rendering (32x32 tiles at the moment) with 60fps
* Definition files (maps, items, objects, tiles, animations, monsters, sprites)
* Parallax Scrolling (background, 
* Inventory (use items)
* Music/Sound

used art from https://opengameart.org/
https://opengameart.org/content/dungeon-crawl-32x32-tiles
https://opengameart.org/content/castle-platformer


build with
~~~~
cmake .
cmake --build .
~~~~

You may have to correct bug of SDL see [instructions](https://stackoverflow.com/questions/45730098/cmake-leading-or-trailing-whitespace-policy-cmp0004)  
