BACKGROUND dungeon/bg.txt
MUSIC ost.mp3
TILES
  . = none
  w = dungeon/wall.txt
  f = dungeon/floor.txt
  c = dungeon/ceiling.txt
  s = dungeon/swall.txt
TILES_END
LAYOUT
wcccccccccccccccccccccccccccccw
s.............................s
s.............................s
s............................fw
s.............................s
wffffff...f...f...f....f......s
s.........................f...s
s............................fw
s.............................s
s..........................f..s
s.......................f.....s
s.............................s
s....................f........s
s...............f.............s
s..........f..................s
wffffffffffwffffffffffffffffffw
LAYOUT_END
MONSTERS
t;12;14;lava.txt;
t;13;14;lava.txt;
t;14;14;lava.txt;
t;15;14;lava.txt;
t;16;14;lava.txt;
t;17;14;lava.txt;
t;18;14;lava.txt;
t;19;14;lava.txt;
t;20;14;lava.txt;
t;21;14;lava.txt;
t;22;14;lava.txt;
t;23;14;lava.txt;
t;24;14;lava.txt;
t;25;14;lava.txt;
t;26;14;lava.txt;
t;27;14;lava.txt;
t;28;14;lava.txt;
t;29;14;lava.txt;
t;14;2.25;beeSmall.txt
t;7;3.25;beeSmall.txt
t;21;3.25;beeSmall.txt
t;3;4.25;beeSmall.txt
t;25;4.25;beeSmall.txt
MONSTERS_END
OBJECTS
t;2;13;door.txt;dungeon/02.txt 1664 96;
t;2;4;(C1)chest.txt;key.txt
t;29;2;(C3)chest.txt;ring.txt
OBJECTS_END
