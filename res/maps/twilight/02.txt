MAP Twilight01
BACKGROUND bgstars.txt
MUSIC ost3.mp3
TILES
  . = none
  w = twilight/wall.txt
  k = twilight/walltopright.txt
  j = twilight/walltopleft.txt
  b = twilight/wallbottomleft.txt
  v = twilight/wallbottomright.txt   
  r = twilight/wallright.txt
  l = twilight/wallleft.txt
  f = twilight/floor.txt
TILES_END
LAYOUT
wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
r.....................lwr...........l
r...................jfv.............l
wfffffffffk.......jfv..............jw
r...............jfv.................l
r.............jfv...................l
r...........jfv.....................l
r.........jfv.......................l
r.......jfv.........................l
r.jfffffv...........................l
r.l.................................l
r...jffk............................l
bfffvwwbffffffffffffffffffffffffffffv
LAYOUT_END
OBJECTS
t;35;1;door.txt;twilight/bossRoom.txt 32 32;
t;1;1;door.txt;twilight/saveRoom01.txt 512 128;
t;33;11;sign.txt;;TEXT:The smell of flowers remind me of her.\n
t;35;11;sign.txt;;TEXT:She turned the world around for me.\n
t;7;10;switch.txt
t;13;11;flower.txt
t;15;11;flower.txt
t;16;11;flower.txt
t;18;8;tree.txt
t;25;10;lamp.txt
t;3;8;fenceleft.txt
t;4;8;fencemiddle.txt
t;5;8;fencemiddle.txt
t;6;8;fenceright.txt
OBJECTS_END
