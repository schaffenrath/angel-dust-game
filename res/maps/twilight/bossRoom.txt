MAP Twilight01
BACKGROUND bgstars.txt
MUSIC maehh.mp3
TILES
  . = none
  o = twilight/wolf.txt
  w = twilight/wall.txt
  k = twilight/walltopright.txt
  j = twilight/walltopleft.txt
  b = twilight/wallbottomleft.txt
  v = twilight/wallbottomright.txt   
  r = twilight/wallright.txt
  l = twilight/wallleft.txt
  f = twilight/floor.txt
TILES_END
LAYOUT
wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
r...................................l
wffk................................l
r......................f............l
r...................................l
r..........................f........l
r....................f..............l
r........................f..........l
r...................................l
r.........f...f...f...f.............l
r............................f......l
r.......j........................k..l
bfffffffvwwwwwwwwwwwwwwwwwwwwwwwwbffv
LAYOUT_END
MONSTERS
t;9;11;lava.txt;
t;10;11;lava.txt;
t;11;11;lava.txt;
t;12;11;lava.txt;
t;13;11;lava.txt;
t;14;11;lava.txt;
t;15;11;lava.txt;
t;16;11;lava.txt;
t;17;11;lava.txt;
t;18;11;lava.txt;
t;19;11;lava.txt;
t;20;11;lava.txt;
t;21;11;lava.txt;
t;22;11;lava.txt;
t;23;11;lava.txt;
t;24;11;lava.txt;
t;25;11;lava.txt;
t;26;11;lava.txt;
t;27;11;lava.txt;
t;28;11;lava.txt;
t;29;11;lava.txt;
t;30;11;lava.txt;
t;31;11;lava.txt;
t;32;11;lava.txt;
t;32;1;(Boss3)wolf.txt;key.txt;
MONSTERS_END
OBJECTS
t;35;10;door.txt;end;;LOCKED:1
OBJECTS_END