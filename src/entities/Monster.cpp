#include "Monster.hpp"
#include "ResourceFacade.hpp"
#include "EventFunction.hpp"
#include "Room.hpp"
#include "EventDelayed.hpp"
#include "WeakAnimation.hpp"
#include "GameStateInGame.hpp"
#include "Switch.hpp"

#include <iostream>
#include <sstream>
#include <random>
#include <vector>

const std::vector<Point> magePositions{Point(864, 160), Point(864, 32), Point(864, 288),
                                       Point(480, 96), Point(1248, 96), Point(192, 160), Point(1600, 160)};
const std::vector<Point> wolfPositions{Point(1024, 32), Point(1024, 192), Point(1024, 112)};

Monster::Monster(const Point pos_, MonsterType::sPtr type_, const std::string &switchName_, const std::string &itemString_) : Entity(type_.get(), switchName_), Moveable(pos_), Collisionable(), type(std::move(type_)),
                                                                                                                              health(type->getMaxHP()), pendingAction(), itemString(itemString_)
{
  animationSet = type->getAnimationCopy();
  gravity = !type->isFlying();
}

void Monster::invoke(std::queue<Event::uPtr> &events, double timeStep)
{

  if (actualState == AnimationState::dead)
  {
    events.push(std::make_unique<EventFunction>(std::bind(&Monster::moveYOnly, this, std::placeholders::_1)));
    return;
  }

  //onDeath
  if (health.get() <= 0)
  {
    actualState = AnimationState::dead;
    events.push(std::make_unique<EventFunction>(std::bind(&Monster::onDeath, this, std::placeholders::_1)));
    return;
  }

  if (pendingAction.get() != nullptr)
  {
    events.push(std::make_unique<EventFunction>([this](double timeStep) {
      pendingAction->step(timeStep);
      if (pendingAction->done())
      {
        pendingAction.reset(nullptr);
      }
      moveYOnly(timeStep);
    }));
    return;
  }
  actualState = AnimationState::idle;

  //KI goes here
  if (type->getName().find("mage.txt") != std::string::npos)
  {
    mageKi(events, timeStep);
  }
  else if (type->getName().find("bigBird.txt") != std::string::npos)
  {
    double birdbasespeed = 0.07;
    if (velocity.getPosX() == 0.0)
    {
      velocity.setPosX(-birdbasespeed);
      velocity.setPosY(birdbasespeed);
    }
  }
  else if (type->getName().find("wolf.txt") != std::string::npos)
  {
    wolfKi(events, timeStep);
  }
  // if range attack is available
  else if (animationSet.find(AnimationState::rangeAttack) !=
           animationSet.end())
  {

    VectorD distance = room->getPlayer()->getRectangleInWorld().getCenter() - getRectangleInWorld().getCenter();
    if (distance.length() < type->getRange() && distance.length() > type->getRange() / 4)
    {
      Action *rangeAttack = new Action(1000);

      rangeAttack->addChild(std::make_shared<WeakAnimation>(&getAnimation(AnimationState::rangeAttack)));

      Event::uPtr createAttack = std::make_unique<EventFunction>([this, distance](double) mutable {
        auto animation = RF.animations.getResource(type->getBulletAnimation());
        Rectangle pos = animation->getActualSize();
        pos += getRectangleInWorld().getCenter();
        pos -= animation->getActualSize().getCenter();
        distance.normalize();
        distance *= type->getRangeVel();

        Attack::uPtr att = std::make_unique<Attack>(room, type->getRangeDmg(), pos, type->getRange() / distance.length(), true, false, type->isRangeInv(), false, false);
        att->setVelocity(distance);
        att->setAttackAnim(animation);
        att->setOrientation(orientationH);
        room->addAttack(std::move(att));
      });

      rangeAttack->addChild(std::make_shared<EventDelayed>(createAttack, 500.0));

      pendingAction.reset(rangeAttack);
      actualState = AnimationState::rangeAttack;
    }
    else if (animationSet.find(AnimationState::move) != animationSet.end())
    {
      double absDist = fabs(getDistanceToPlayer().getPosX());
      if (absDist < 1000.)
      {
        if (getDistanceToPlayer().getPosX() < 0)
        {
          velocity.setPosX(-0.14);
        }
        else if (fabs(getDistanceToPlayer().getPosX()) < 2)
        {
          velocity.setPosX(0.0);
        }
        else
        {
          velocity.setPosX(0.14);
        }
        if (distance.length() <= type->getRange() / 4)
        {
          velocity.setPosX(-velocity.getPosX());
        }
      }
      else
      {
        velocity.setPosX(0.0);
      }
    }
  }
  // simple moving monster
  else if (animationSet.find(AnimationState::move) != animationSet.end())
  {
    if (!gravity)
    { //dumb flying monster
      if (velocity.getPosX() == 0.0)
      {
        velocity.setPosX(-0.07);
      }
    }
    else
    { //non-flying monster

      double absDist = fabs(getDistanceToPlayer().getPosX());
      if (absDist < 1000.)
      {
        if (getDistanceToPlayer().getPosX() < 0)
        {
          velocity.setPosX(-0.07);
        }
        else if (absDist < 2)
        {
          velocity.setPosX(0.0);
        }
        else
        {
          velocity.setPosX(0.07);
        }
      }
      else
      {
        velocity.setPosX(0.0);
      }

      //dash attack
      if (type->getDashRange() != 0 &&
          fabs(getDistanceToPlayer().getPosX()) < type->getDashRange())
      {
        double duration = type->getDashRange() / fabs(velocity.getPosX()) / 2.;
        //dashing action, with 1 sec no attack
        Action *dash = new Action(duration + 1000);

        dash->addChild(std::make_shared<WeakAnimation>(&getAnimation(AnimationState::dash)));

        Event::uPtr createAttack = std::make_unique<EventFunction>([this, duration](double) {
          auto animation = RF.animations.getResource(type->getMeleeAttackAnimation());
          Rectangle pos = animation->getActualSize();
          pos += getRectangleInWorld().getCenter();
          pos -= animation->getActualSize().getCenter();
          if (orientationH == Orientation::Left)
          {
            pos -= Point(getRectangleInWorld().getWidth() / 2, 0);
          }
          else
          {
            pos += Point(getRectangleInWorld().getWidth() / 2, 0);
          }

          //attack has same duration as dash without
          Attack::uPtr att = std::make_unique<Attack>(room, type->getMeleeDmg(), pos, duration, true, false, false, false, false);
          //att->setVelocity(distance);
          att->setAttackAnim(animation);
          att->setOrientation(orientationH);
          att->setVelocity(velocity * VectorD(2., 1.));
          room->addAttack(std::move(att));
        });
        dash->addChild(std::make_shared<EventDelayed>(createAttack, 0.0));

        //move in double speed for dash duration
        dash->addChild(std::make_shared<EventDelayed>(std::make_unique<EventFunction>([this](double timeStep) {
                                                        move(timeStep);
                                                        move(timeStep);
                                                      }),
                                                      0.0, duration));

        pendingAction.reset(dash);
        actualState = AnimationState::dash;
        //check if distance to player lower, own size + size of attack
      }
      //melee attack
      else if (type->getMeleeDmg() != 0 &&
               fabs(getDistanceToPlayer().getPosX()) < getActualSize().getWidth() / 2 + RF.animations.getResource(type->getMeleeAttackAnimation())->getActualSize().getWidth())
      {
        Action *meleeAttack = new Action(500);

        meleeAttack->addChild(std::make_shared<WeakAnimation>(&getAnimation(AnimationState::meleeAttack)));

        Event::uPtr createAttack = std::make_unique<EventFunction>([this](double) {
          auto animation = RF.animations.getResource(type->getMeleeAttackAnimation());
          Rectangle pos = animation->getActualSize();
          pos += getRectangleInWorld().getCenter();
          pos -= animation->getActualSize().getCenter();
          if (orientationH == Orientation::Left)
          {
            pos -= Point(getRectangleInWorld().getWidth() / 2, 0);
          }
          else
          {
            pos += Point(getRectangleInWorld().getWidth() / 2, 0);
          }

          Attack::uPtr att = std::make_unique<Attack>(room, type->getMeleeDmg(), pos, 100, true, false, false, false, true);
          //att->setVelocity(distance);
          att->setAttackAnim(animation);
          att->setOrientation(orientationH);
          room->addAttack(std::move(att));
        });
        meleeAttack->addChild(std::make_shared<EventDelayed>(createAttack, 0.0));

        pendingAction.reset(meleeAttack);
        actualState = AnimationState::meleeAttack;
      }
    }
  }
  Entity::invoke(events, timeStep);
  events.push(std::make_unique<EventFunction>([this](double timeStep) {
    //getAnimation(actualState).step(timeStep);
    this->move(timeStep);
  }));
}

void Monster::wolfKi(std::queue<Event::uPtr> &events, double timeStep)
{
  orientationH = Orientation::Left;

  std::random_device rd;
  std::mt19937 gen(rd());
  //std::generate_canonical<> gen(gen);

  VectorD distance = room->getPlayer()->getRectangleInWorld().getCenter() - getRectangleInWorld().getCenter();
  auto playerHeight = room->getPlayer()->getCollisionRectWorld().getCenter().getPosY();
  bool inHeigth = playerHeight >= position.getPosY() && playerHeight < getCollisionRectWorld().getOppositePoint().getPosY();

  //33% to do a dash if player is in range
  if (inHeigth && std::generate_canonical<double, 10>(gen) < 0.33)
  {
    Action *dash = new Action(1000);
    Event::uPtr resetPosition = std::make_unique<EventFunction>([this](double) mutable {
      this->velocity.setPosX(0.0);
      this->position = newRndPosition(wolfPositions);
    });
    dash->addChild(std::make_shared<EventDelayed>(resetPosition, 999.0));
    dash->addChild(std::make_shared<EventDelayed>(std::make_unique<EventFunction>(std::bind(&Moveable::move, this, std::placeholders::_1)), 0.0, 999.9));
    pendingAction.reset(dash);
    actualState = AnimationState::dash;
    velocity.setPosX(-0.25);
  }
  else if (distance.length() < type->getRange())
  {
    Action *rangeAttack = new Action(1000);

    rangeAttack->addChild(std::make_shared<WeakAnimation>(&getAnimation(AnimationState::rangeAttack)));

    Event::uPtr createAttack = std::make_unique<EventFunction>([this, distance](double) mutable {
      auto animation = RF.animations.getResource(type->getBulletAnimation());
      Rectangle pos = animation->getActualSize();
      pos += getRectangleInWorld().getCenter();
      pos -= animation->getActualSize().getCenter();
      distance.normalize();
      distance *= type->getRangeVel();

      Attack::uPtr att = std::make_unique<Attack>(room, type->getRangeDmg(), pos, type->getRange() / distance.length(), true, false, type->isRangeInv(), false, false);
      att->setVelocity(distance);
      att->setAttackAnim(animation);
      att->setOrientation(orientationH);
      room->addAttack(std::move(att));
    });

    rangeAttack->addChild(std::make_shared<EventDelayed>(createAttack, 500.0));

    pendingAction.reset(rangeAttack);
    actualState = AnimationState::rangeAttack;
  }
}

void Monster::mageKi(std::queue<Event::uPtr> &events, double timeStep)
{

  VectorD distance = room->getPlayer()->getRectangleInWorld().getCenter() - getRectangleInWorld().getCenter();
  if (distance.length() < type->getRange())
  {
    Action *rangeAttack = new Action(500);

    rangeAttack->addChild(std::make_shared<WeakAnimation>(&getAnimation(AnimationState::rangeAttack)));
    Event::uPtr createAttack;

    std::random_device rd;
    std::mt19937 gen(rd());
    //std::generate_canonical<> gen(gen);

    if (std::generate_canonical<double, 10>(gen) < 0.25)
    {
      createAttack = std::make_unique<EventFunction>([this, distance](double) mutable {
        auto animation = RF.animations.getResource(type->getBulletAnimation());
        Rectangle pos = animation->getActualSize();
        pos += getRectangleInWorld().getCenter();
        pos -= animation->getActualSize().getCenter();
        distance.normalize();
        distance *= type->getRangeVel();

        Attack::uPtr att = std::make_unique<Attack>(room, type->getRangeDmg(), pos, type->getRange() / distance.length(), true, false, type->isRangeInv(), false, false);
        att->setVelocity(distance);
        att->setAttackAnim(animation);
        att->setOrientation(orientationH);
        room->addAttack(std::move(att));
      });
    }
    else
    {
      createAttack = std::make_unique<EventFunction>([this, distance](double) mutable {
        auto animation = RF.animations.getResource("magebullet2.txt");
        Rectangle pos = animation->getActualSize();
        pos += getRectangleInWorld().getCenter();
        pos -= animation->getActualSize().getCenter();
        distance.normalize();
        distance *= type->getRangeVel();

        Attack::uPtr att = std::make_unique<Attack>(room, type->getRangeDmg() * 4, pos, type->getRange() / distance.length(), true, false, false, false, false);
        att->setVelocity(distance);
        att->setAttackAnim(animation);
        att->setOrientation(orientationH);
        room->addAttack(std::move(att));
      });
    }

    rangeAttack->addChild(std::make_shared<EventDelayed>(createAttack, 200.0));

    pendingAction.reset(rangeAttack);
    actualState = AnimationState::rangeAttack;
  }
}

const Point &Monster::newRndPosition(const std::vector<Point> &positions) const
{
  //return magePositions[std::experimental::randInt(0, magePositions.length())];
  std::random_device rd;                                         // non-deterministic generator
  std::mt19937 gen(rd());                                        // to seed mersenne twister.
  std::uniform_int_distribution<> dist(0, positions.size() - 1); // distribute results between 1 and 6 inclusive.
  return positions[dist(gen)];
}

const Rectangle Monster::getActualSize() const
{
  return getAnimation(actualState).getActualSize();
}

void Monster::render(const Point &offset, const SDL_RendererFlip flip)
{
  Entity::render(position + offset, static_cast<SDL_RendererFlip>(flip ^ (orientationH == Orientation::Right ? SDL_FLIP_NONE : SDL_FLIP_HORIZONTAL)));
  if constexpr (false)
  {
    Rectangle rec = getRectangleInWorld();
    rec += offset;

    Renderer *ren = RF.getRenderer();
    ren->setDrawColor(Color::Red());
    ren->draw(rec);
    ren->setDrawColor(Color::Black());
  }
}

const MonsterType *Monster::getType() const
{
  return type.get();
}

int Monster::getTouchDmg() const
{
  if (actualState == AnimationState::dead)
  {
    return 0;
  }
  return type->getTouchDmg();
}

Rectangle Monster::getCollisionRectWorld() const
{
  return getRectangleInWorld();
}

Rectangle Monster::getCollisionRectWorld(double timeStep) const
{
  return getRectangleInWorldTestMove(timeStep);
}

void Monster::handleCollision(const Attack *attack)
{
  if (!type->isInvincible() && attack->harmsMonster() && latestHits.find(attack) == latestHits.end())
  {
    health -= attack->getTouchDmg();
    latestHits.insert(attack);
    if (type->getHitSound() != nullptr)
    {
      type->getHitSound()->play();
    }
    if (type->getName().find("mage.txt") != std::string::npos && !health.isMin())
    {
      position = newRndPosition(magePositions);
    }
    else if (type->getName().find("wolf.txt") != std::string::npos)
    {
      position = newRndPosition(wolfPositions);
    }
    else if (type->getName().find("bigBird.txt") != std::string::npos)
    {
      velocity.setPosY(getVelocity().getPosY() * 1.05);
      velocity.setPosX(getVelocity().getPosX() * 1.05);
    }
  }
}

void Monster::handleCollision(const DummyWall *wall)
{
  if (!gravity && type->getName().find("wolf.txt") == std::string::npos)
  {
    double newWidth = (getActualSize().getWidth() + wall->getCollisionRectWorld().getWidth()) / 2;
    double newHeight = (getActualSize().getHeight() + wall->getCollisionRectWorld().getHeight()) / 2;

    Point collisionPoint = getRectangleInWorld().getCenter() - wall->getCollisionRectWorld().getCenter();

    double wy = newWidth * collisionPoint.getPosY();
    double hx = newHeight * collisionPoint.getPosX();

    if (wy > hx)
    {
      if (wy > -hx)
      {
        if (velocity.getPosY() < 0.0)
          velocity.setPosY(-velocity.getPosY());
      }
      else
      {
        if (velocity.getPosX() > 0.0)
          velocity.setPosX(-velocity.getPosX());
      }
    }
    else
    {
      if (wy > -hx)
      {
        if (velocity.getPosX() < 0.0)
          velocity.setPosX(-velocity.getPosX());
      }
      else
      {
        if (velocity.getPosY() > 0.0)
          velocity.setPosY(-velocity.getPosY());
      }
    }
  }
}

void Monster::removeFadingAttacks(const Attack *attack)
{
  latestHits.erase(attack);
}

void Monster::onDeath(double)
{
  health = 0;
  gravity = true;
  velocity.setPosX(0.0);
  velocity.setPosY(0.0);
  room->getPlayer()->addExp(type->getXPGain());
  room->screenShake();

  if (mySwitch.get() != nullptr)
  {
    mySwitch->setOn();
  }

  if (itemString != "")
  {
    Item::uPtr spawnItem = Item::createItemFromString(itemString);
    spawnItem->setPosition(getPosition());
    room->moveItem(spawnItem);
  }
  //room->removeMonster(this);
  if (animationSet.find(AnimationState::dead) == animationSet.end())
  {
    room->addRemove(this);
  }
}

void Monster::removeMyself()
{
  room->addRemove(this);
}

VectorD Monster::getDistanceToPlayer() const
{
  return room->getPlayer()->getRectangleInWorld().getCenter() - getCollisionRectWorld().getCenter();
}
