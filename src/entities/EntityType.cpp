#include "EntityType.hpp"

#include "ResourceFacade.hpp"


EntityType::EntityType(const std::string &name_) : name(name_) {}

std::map<AnimationState, Animation> EntityType::getAnimationCopy() const
{
  std::map<AnimationState, Animation> copy{};
  for (auto &it : animationSet)
  {
    copy[it.first] = *it.second;
  }
  return copy;
}

/* Animation &EntityType::getAnimation(AnimationState state)
{
  std::map<AnimationState, Animation::sPtr>::iterator it;
  if ((it = animationSet.find(state)) != animationSet.end())
  {
    return *(it->second.get());
  }
  return *(animationSet[AnimationState::idle]);
} */

void EntityType::parseEntityData(EntityType *entity, const std::string &key, const std::string &value)
{
  try
  {
    if (key == "IDLE")
    {
      entity->animationSet[AnimationState::idle] = RF.animations.getResource(value);
    }
    else if (key == "DEAD")
    {
      entity->animationSet[AnimationState::dead] = RF.animations.getResource(value);
    }
    else if (key == "MOVE")
    {
      entity->animationSet[AnimationState::move] = RF.animations.getResource(value);
    }
    else if (key == "RANGEMV")
    {
      entity->animationSet[AnimationState::rangeAttack] = RF.animations.getResource(value);
    }
  }
  catch (const std::runtime_error &e)
  {
    if (key == "IDLE")
    {
      throw e;
    }
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
  }
}
