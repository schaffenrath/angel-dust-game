#include "ObjectType.hpp"

#include <fstream>

ObjectType::ObjectType(const std::string &name_) : EntityType(name_) {}

ObjectType::sPtr ObjectType::load(Renderer &, const std::string &filename)
{
  auto type = std::make_shared<ObjectType>(filename);

  std::ifstream objectTypeFile;
  std::string line;
  //std::shared_ptr<Animation> animation = std::make_shared<Animation>();

  objectTypeFile.open(filename);
  if (objectTypeFile.is_open())
  {
    while (std::getline(objectTypeFile, line))
    {
      size_t pos = line.find(" ");
      std::string keyA = line.substr(0, pos);
      if (keyA == "INTERACTION")
      {
        std::string value = line.substr(pos + 1);
        if (value == "WARP")
        {
          type->interactionType = Interaction::Warp;
        }
        else if (value == "HEAL")
        {
          type->interactionType = Interaction::Heal;
        }
        else if (value == "TEXT")
        {
          type->interactionType = Interaction::Text;
        }
        else if (value == "TAKE")
        {
          type->interactionType = Interaction::Take;
        }
        else if (value == "SAVE")
        {
          type->interactionType = Interaction::Save;
        }
        else if (value == "FLIP")
        {
          type->interactionType = Interaction::Flip;
        }
      }
      else if (keyA == "TEXT")
      {
        type->text = line.substr(pos + 1);
      }
      else if (keyA == "ONCE")
      {
        type->onlyOnce = true;
      }
      else
      {
        EntityType::parseEntityData(type.get(), keyA, line.substr(pos + 1));
      }
      //animation->push(RF.sprites.getResource(line.substr(0, pos).c_str()),std::stoi(line.substr(pos+1).c_str()));
    }
    objectTypeFile.close();
  }
  else
  {
    throw std::runtime_error("File \"" + filename + "\" not found!");
  }

  return type;
}
