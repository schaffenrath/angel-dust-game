#include "Attack.hpp"

#include "Room.hpp"
#include "EventFunction.hpp"
#include "ResourceFacade.hpp"

#include <iostream>

Attack::Attack(Room *room_, int touchDmg_, const Rectangle &positionSize_, double lifetime_, bool harmPlayer_, bool harmMonster_, bool invertible_, bool deflects_, bool persistent_)
    : Renderable(), Moveable(positionSize_.getBase()),
      room(room_), touchDmg(touchDmg_), size(0, 0, positionSize_.getWidth(), positionSize_.getHeight()),
      lifetime(lifetime_), harmPlayer(harmPlayer_), harmMonster(harmMonster_), invertible(invertible_), deflects(deflects_), persistent(persistent_) {}

void Attack::render(const Point &offset, const SDL_RendererFlip flip)
{
  if (attackAnim.get() != nullptr)
  {
    attackAnim->render(offset + position, static_cast<SDL_RendererFlip>(flip ^ (orientationH == Orientation::Right ? SDL_FLIP_NONE : SDL_FLIP_HORIZONTAL)));
  }
  if constexpr(false)
  {
    Rectangle rec(getRectangleInWorld());
    rec += offset;

    Renderer *ren = RF.getRenderer();
    ren->setDrawColor(Color::Red());
    ren->draw(rec);
    ren->setDrawColor(Color::Black());
  }
  return;
}

void Attack::invoke(std::queue<Event::uPtr> &events, double timeStep)
{
  lifetime -= timeStep;
  if (attackAnim.get() != nullptr)
  {
    attackAnim->step(timeStep);
  }
  if (lifetime < 0)
  {
    events.push(std::make_unique<EventFunction>([this](double) {
      this->room->addRemove(this);
    }));
  }
  if (!velocity.isZero())
  {
    events.push(std::make_unique<EventFunction>([this](double timeStep) {
      this->move(timeStep);
    }));
  }
}

const Rectangle Attack::getActualSize() const
{
  return size;
}

void Attack::handleCollision(const Player *)
{
  if (harmPlayer && !persistent)
    room->addRemove(this);
}

void Attack::handleCollision(const Monster *)
{
  //maybe attack can be persistent? and hit more than one monster?
  if (harmMonster && !persistent)
    room->addRemove(this);
}

void Attack::handleCollision(const DummyWall *)
{
  //std::cout << "attack died through wall\n";
  if (!persistent)
    room->addRemove(this);
}

#include <iostream>

void Attack::handleCollision(const Attack *attack)
{
  if (invertible && attack->deflects && latestHits.find(attack) == latestHits.end())
  {
    velocity *= -1.0;
    harmPlayer = !harmPlayer;
    harmMonster = !harmMonster;
    latestHits.insert(attack);
  }
}

Rectangle Attack::getCollisionRectWorld() const
{
  return getRectangleInWorld();
}

Rectangle Attack::getCollisionRectWorld(double timeStep) const
{
  return getRectangleInWorldTestMove(timeStep);
}

void Attack::setAttackAnim(Animation::sPtr attackAnim_)
{
  attackAnim = std::move(attackAnim_);
}

void Attack::removeFadingAttacks(const Attack *attack)
{
  latestHits.erase(attack);
}
