#include "MonsterType.hpp"

#include <fstream>
#include <iostream>

#include "ResourceFacade.hpp"

MonsterType::MonsterType(const std::string &name_) : EntityType(name_), bulletAnimation(), hitsound() {};

MonsterType::sPtr MonsterType::load(Renderer &, const std::string &filename)
{
  auto type = std::make_shared<MonsterType>(filename);

  std::ifstream animationfile;
  std::string line;
  //std::shared_ptr<Animation> animation = std::make_shared<Animation>();

  animationfile.open(filename);
  if (animationfile.is_open())
  {
    while (std::getline(animationfile, line))
    {
      size_t pos = line.find(" ");
      std::string keyA = line.substr(0, pos);
      if (keyA == "HP")
      {
        type->maxHP = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "TDMG")
      {
        type->touchDmg = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "XP")
      {
        type->XPGain = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "RANGE")
      {
        type->range = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "HITSOUND")
      {
        type->hitsound = RF.sounds.getResource(line.substr(pos +1));
      }
      else if (keyA == "RDMG")
      {
        type->rangeDmg = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "MDMG")
      {
        type->meleeDmg = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "DASH")
      {
        type->dashRange = std::stoi(line.substr(pos + 1));
      }
      else if (keyA == "RVEL")
      {
        type->rangeSpeed = std::stod(line.substr(pos + 1));
      }
      else if (keyA == "RINV")
      {
        type->rangeInvertible = true;
      }
      else if (keyA == "MELEE_ANIM")
      {
        type->meleeAttackAnimation = line.substr(pos + 1);
      }
      else if (keyA == "BULL_ANIM")
      {
        type->bulletAnimation = line.substr(pos + 1);
      }
      else if (keyA == "INTERACTION")
      {
        type->interaction = line.substr(pos + 1);
      }
      else if (keyA == "FLY")
      {
        type->flies = true;
      }
      else if (keyA == "INVINCIBLE")
      {
        type->invincible = true;
      }else{
        EntityType::parseEntityData(type.get(), keyA, line.substr(pos + 1));
      }
      //animation->push(RF.sprites.getResource(line.substr(0, pos).c_str()),std::stoi(line.substr(pos+1).c_str()));
    }
    animationfile.close();
  }
  else
  {
    throw std::runtime_error("File \"" + filename + "\" not found!");
  }

  return type;
}
