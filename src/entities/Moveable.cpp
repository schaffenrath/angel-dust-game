#include "Moveable.hpp"
#include <iostream>
/* Moveable::Moveable(const Point &startPos, int factor) : position(startPos), velocity(0, 0),
                                                        orientation(Orientation::Down), orientationH(Orientation::Left), factor(factor)
{
} */
Moveable::Moveable(const Point &startPos) : position(startPos) {}

void Moveable::move(double timeStep)
{
  if (velocity.getPosX() == 0)
  {
    orientation = Orientation::Down;
  }
  else if (velocity.getPosX() < 0)
  {
    orientationH = orientation = Orientation::Left;
  }
  else
  {
    orientationH = orientation = Orientation::Right;
  }

  if (jumping || secondJump)
  {
    if (velocity.getPosY() < 0.0)
    {
      jumpTime -= timeStep;
      if (velocity.getPosY() + 0.035 < 0.0){
        velocity += VectorD(0.0, 0.035) * 60.0 / 1000.0 * timeStep; //timedependent acceleration
      }
      else
      {
        velocity += VectorD(0.0, -velocity.getPosY());
        jumpTime = 40;
      }
    }
    else
    {
      if (velocity.getPosY() == 0 && jumpTime > 0)
        jumpTime -= timeStep;
      else
        stopJump();
    }
  }
  position += velocity * timeStep; // / factor;
}

void Moveable::moveYOnly(double timeStep)
{
  if (jumping || secondJump)
  {
    if (velocity.getPosY() < 0.0)
    {
      jumpTime -= timeStep;
      if (velocity.getPosY() + 0.035 < 0.0){
        velocity += VectorD(0.0, 0.035) * 60.0 / 1000.0 * timeStep; //timedependent acceleration
      }
      else
      {
        velocity += VectorD(0.0, -velocity.getPosY());
        jumpTime = 40;
      }
    }
    else
    {
      if (velocity.getPosY() == 0.0 && jumpTime > 0)
      {
        jumpTime -= timeStep;
      }
      else
      {
        stopJump();
      }
    }
  }

  position.setPosY(position.getPosY() + velocity.getPosY() * timeStep);
}

void Moveable::moveToPosY(const double posY)
{
  //position += VectorD(0, posY - position.getPosY());
  position.setPosY(posY - getActualSize().getBase().getPosY());
}

void Moveable::moveToPosX(const double posX)
{
  //position += VectorD(posX - position.getPosX(), 0);
  position.setPosX(posX - getActualSize().getBase().getPosX());
}

Point Moveable::testMove(double timeStep) const
{
  return position + velocity * timeStep; // / factor;
}

void Moveable::startGoLeft()
{
  velocity += VectorD(-0.25, 0);
}

void Moveable::startGoRight()
{
  velocity += VectorD(0.25, 0);
}

void Moveable::startGoUp(double gravity)
{
  velocity += VectorD(0, -0.25*gravity);
}

void Moveable::startGoDown(double gravity)
{
  velocity += VectorD(0, 0.25*gravity);
}

Rectangle Moveable::getRectangleInWorld() const
{
  Rectangle size = getActualSize();
  //return Rectangle(position, size.getWidth(), size.getHeight());
  size += position;
  return size;
}

Rectangle Moveable::getRectangleInWorldTestMove(double timeStep) const
{
  //Rectangle size = getActualSize();
  Rectangle size = getActualSize();
  //return Rectangle(testMove(dt), size.getWidth(), size.getHeight());
  size += testMove(timeStep);
  return size;
}

VectorD &Moveable::getVelocity()
{
  return velocity;
}

void Moveable::jump(double gravity)
{
  if (!jumping && velocity.getPosY() == 0.0)
  {
    jumping = true;
    velocity += VectorD(0., -0.6*gravity);
  }
  else if (!secondJump && secondJumpEnabled)
  {
    secondJump = true;
    velocity += VectorD(0, -velocity.getPosY()*gravity);
    velocity += VectorD(0, -0.5*gravity);
  }
}

void Moveable::stopJump(double gravity)
{
  if (jumping)
  {
    jumping = false;
    velocity += VectorD(0.0, -velocity.getPosY()*gravity);
  }
}

void Moveable::stopSecondJump()
{
  secondJump = false;
}

VectorD Moveable::getPosition()
{
  return position;
}

void Moveable::setVelocity(const VectorD &newVelocity)
{
  velocity = newVelocity;
}

void Moveable::setOrientation(Orientation orientation){
  if(orientation == Orientation::Left || orientation == Orientation::Right){
    orientationH = orientation;
  }
  this->orientation = orientation;
}

void Moveable::fallThrough()
{
  fallThroughPlatform = true;
}

void Moveable::stopFallThrough()
{
  fallThroughPlatform = false;
}

bool Moveable::shouldFallThrough() const
{
  return fallThroughPlatform;
}
