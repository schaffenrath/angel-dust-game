#include "ItemType.hpp"
#include <fstream>

ItemType::ItemType(const std::string &name_) : EntityType(name_){};

ItemType::sPtr ItemType::load(Renderer &, const std::string filename)
{
  auto type = std::make_shared<ItemType>(filename);

  std::ifstream animationfile;
  std::string line;

  animationfile.open(filename);
  if (animationfile.is_open())
  {
    while (std::getline(animationfile, line))
    {
      size_t pos = line.find(" ");
      std::string arg = line.substr(0, pos);
      std::string value = line.substr(pos + 1);

      if (arg == "HP")
      {
        type->typeClass = ItemCatagory::HP;
        type->value = std::stoi(value);
        type->consumable = true;
      }
      else if (arg == "XP")
      {
        type->typeClass = ItemCatagory::XP;
        type->value = std::stoi(value);
        type->consumable = true;
      }

      else if (arg == "KEY")
      {
        type->typeClass = ItemCatagory::KEY;
        type->value = std::stoi(value);
        type->consumable = false;
      }

      else if (arg == "DOUBLEJUMP")
      {
        type->typeClass = ItemCatagory::DOUBLEJUMP;
        type->value = std::stoi(value);
        type->consumable = false;
      }

      else if (arg == "AXE")
      {
        type->typeClass = ItemCatagory::AXE;
        type->value = std::stoi(value);
        type->consumable = false;
      }

      else if (arg == "RING")
      {
        type->typeClass = ItemCatagory::RING;
        type->value = std::stoi(value);
        type->consumable = false;
      }
      else if (arg == "SHORTDESCRIPTION")
      {
        type->shortDescription = value;
        type->consumable = false;
      }
      else if (arg == "DESCRIPTION")
      {
        type->descripton = value;
        type->consumable = false;
      }

      else
        parseEntityData(type.get(), arg, value);
    }
  }
  return type;
}
