#include "Entity.hpp"

#include "Room.hpp"
#include "GameStateInGame.hpp"
#include "EventFunction.hpp"
#include "Switch.hpp"
#include "Item.hpp"

/* Entity::Entity(EntityType *type_) : type(type_)
{
  animationSet = type->getAnimationCopy();
} */

Entity::Entity(EntityType *type_, const std::string &switchName_) : type(type_), switchName(switchName_)
{
  animationSet = type->getAnimationCopy();
}

/* Item *Entity::getItem()
{
  return item.get();
}

void Entity::createItem(const ItemType::sPtr itemType, const int itemValue)
{
  item = std::make_unique<Item>(itemType, itemValue);
} */

const Animation &Entity::getAnimation(AnimationState state) const
{
  std::map<AnimationState, Animation>::const_iterator it;
  if ((it = animationSet.find(state)) != animationSet.cend())
  {
    return it->second;
  }
  return animationSet.at(AnimationState::idle);
}

Animation &Entity::getAnimation(AnimationState state)
{
  std::map<AnimationState, Animation>::iterator it;
  if ((it = animationSet.find(state)) != animationSet.end())
  {
    return it->second;
  }
  return animationSet[AnimationState::idle];
}

void Entity::invoke(std::queue<Event::uPtr> &events, double)
{
  //getAnimation(actualState).step(timeStep);
  events.push(std::make_unique<EventFunction>(std::bind(&Animation::step, std::ref(getAnimation(actualState)), std::placeholders::_1)));
}

void Entity::render(const Point &offset, const SDL_RendererFlip flip)
{
  getAnimation(actualState).render(offset, flip);
}

void Entity::setRoom(Room *room_)
{
  room = room_;

  if (!switchName.empty())
  {
    mySwitch = std::make_unique<Switch>(switchName, room->getIngame()->getPersistentData());
    if (mySwitch->isOn())
    {
      removeMyself();
    }
  }
}

std::string Entity::getEntityString() const
{
  std::string name = type->getName();
#ifdef _WIN32
  std::string realName = name.substr(name.find_last_of('\\') + 1, name.size() - 1);
#else
  std::string realName = name.substr(name.find_last_of('/') + 1, name.size() - 1);
#endif

  return switchName + realName;
}