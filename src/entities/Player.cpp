#include "Player.hpp"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Attack.hpp"
#include "EventDelayed.hpp"
#include "EventFunction.hpp"
#include "GameStateInGame.hpp"
#include "ResourceFacade.hpp"
#include "TimeDependend.hpp"

Player::Player(GameStateInGame *gameState_, const Point &startPosition, const PersistentInfo &persistent)
    : Moveable(startPosition), Renderable(), Collisionable(),
      gameState(gameState_),
      health(persistent["Player/HPMax"].getInt(), persistent["Player/HP"].getInt(), 0),
      experience(persistent["Player/XPMax"].getInt(), persistent["Player/XP"].getInt(), 0),
      superCharge(persistent["Player/SprMax"].getDouble(), persistent["Player/Spr"].getDouble(), 0),
      colSize(12, 3, 25, 28),
      attDmg(persistent["Player/AttDmg"].getInt()),
      rngDmg(persistent["Player/AttDmg"].getInt() / 2),
      actualRoom(persistent["LastRoom"].get())
{
  loadInventoryFromString(persistent["Player/Inventory"].get());
  enableItemStats();

  loadRoomsFromString(persistent["Player/rooms"].get());

  rest = RF.animations.getResource("link/idle.txt");
  go = RF.animations.getResource("link/move.txt");
  attack = RF.animations.getResource("link/attack.txt");
  attackRange = RF.animations.getResource("link/attackRng.txt");
  superAttack = RF.animations.getResource("link/superAttack.txt");

  actualAnimation = rest.get();

  lastHit.start(INVUL_TIME_MS);
}

void Player::enableItemStats()
{
  for (auto &item : inventory)
  {
    switch (item->getItemCatagory())
    {
    case ItemCatagory::DOUBLEJUMP:
      enableSecondJump();
      break;
    case ItemCatagory::AXE:
      enableAxe();
      break;
    default:
      break;
    }
  }
}

void Player::invoke(std::queue<Event::uPtr> &events, double timeStep)
{

  interacts = false;
  if (health.isMin())
  {
    gameState->getGame()->transitionToDeath();
  }

  lastHit.step(timeStep);
  if (lastHit.isDone())
  {
    hitBlinker.setOff();
  }
  else
  {
    hitBlinker.setOn();
  }
  hitBlinker.step(timeStep);

  superDuration.step(timeStep);
  if (superActive && superDuration.isDone())
  {
    superActive = false;
    stopSuperAttack();
  }

  events.push(std::make_unique<EventFunction>([this](double timeStep) {
    superCharge += (timeStep * superChargeFactor);
  }));

  if (pendingAction.get() != nullptr)
  {
    events.push(
        std::make_unique<EventFunction>([this](double timeStep) {
          pendingAction->step(timeStep);
          if (pendingAction->done())
          {
            pendingAction.reset(nullptr);
          }
        }));
    events.push(std::make_unique<EventFunction>(std::bind(&Moveable::moveYOnly, this, std::placeholders::_1)));
    return;
  }

  if (velocity.isZero())
  {
    actualAnimation = rest.get();
  }
  else
  {
    actualAnimation = go.get();
  }

  events.push(std::make_unique<EventFunction>([this](double timeStep) {
    actualAnimation->step(timeStep);
    this->move(timeStep);
  }));
}

void Player::addExp(int newXP)
{
  experience += newXP;
  if (experience.isMax())
  {
    // increase level
    room->getIngame()->getHUD()->displayText("Level up!", 1000);
    health.changeMaxFactor(1.1f);
    health.set(health.getMax());
    experience.changeMaxFactor(1.1f);
    experience.reset();
    attDmg += 1;
    rngDmg = attDmg / 2;
  }
}

void Player::addHealth(int newHP)
{
  health += newHP;
}

void Player::addHealthPercent(int percent)
{
  health += (health.getMax() * percent / 100);
}

/* void Player::addItem(const ItemType::sPtr itemType, const int itemValue)
{
  inventory.push_back(std::make_unique<Item>(itemType, itemValue));
} */

Inventory &Player::getInventory()
{
  return inventory;
}

void Player::useKey(const int keyValue_)
{
  room->addAttack(std::make_unique<Attack>(room, keyValue_, this->getRectangleInWorld(), 0, false, false));
}

void Player::removeKey(const int keyValue)
{
  for (auto itemIt = inventory.begin(); itemIt != inventory.end(); itemIt++)
  {
    if ((*itemIt)->getItemCatagory() == ItemCatagory::KEY && (*itemIt)->getItemValue() == keyValue)
    {
      inventory.erase(itemIt);
      break;
    }
  }
}

void Player::removeItemFromInventory(const Item *toRemoveItem)
{
  for (auto itemIt = inventory.begin(); itemIt != inventory.end(); itemIt++)
  {
    if ((*itemIt).get() == toRemoveItem)
    {
      inventory.erase(itemIt);
      break;
    }
  }
}

std::string Player::inventoryToString()
{
  std::string inventoryString;
  for (auto &item : inventory)
  {
    inventoryString += item->getItemString() + ";";
  }
  return inventoryString;
}

std::string Player::knownRoomsToString()
{
  std::string knownRoomsString;
  for (auto &rooms : knownRooms)
  {
    knownRoomsString += rooms + ";";
  }
  return knownRoomsString;
}

void Player::setRoom(Room *room_)
{
  room = room_;
  actualRoom = room->getName();
  knownRooms.insert(room->getName().substr(RF.getResourcePath("maps").length()));
}

void Player::loadRoomsFromString(std::string savedRooms)
{
  if (!savedRooms.empty())
  {
    std::stringstream roomString(savedRooms);
    std::string roomStr;
    while (std::getline(roomString, roomStr, ';'))
    {
      knownRooms.insert(roomStr);
    }
  }
}

void Player::loadInventoryFromString(std::string savedString)
{
  if (!savedString.empty())
  {
    std::stringstream inventoryString(savedString);
    Inventory buildInventory{};
    std::string itemTypeStr;
    while (std::getline(inventoryString, itemTypeStr, ';'))
    {
      buildInventory.push_back(std::make_unique<Item>(RF.itemTypes.getResource(itemTypeStr), ""));
    }
    inventory = std::move(buildInventory);
  }
}

void Player::render(const Point &offset, const SDL_RendererFlip flip)
{

  if (hitBlinker.getState())
  {
    if (orientationH == Orientation::Right)
      actualAnimation->render(position + offset, flip);
    else
      actualAnimation->render(
          position + offset,
          SDL_RendererFlip(flip ^ SDL_RendererFlip::SDL_FLIP_HORIZONTAL));
  }
  // bounding box
  if (false)
  {
    Rectangle rec(getRectangleInWorld());
    rec += offset;

    Renderer *ren = RF.getRenderer();
    ren->setDrawColor(Color::Blue());
    ren->draw(rec);
    ren->setDrawColor(Color::Black());
  }
}

void Player::interact()
{
  interacts = true;
  /* room->addAttack(std::make_unique<Attack>(room, 0, this->getRectangleInWorld(),
                                           100, false, false)); */
}

void Player::startAttack()
{
  if (pendingAction.get() != nullptr)
  {
    return;
  }
  pendingAction = std::make_unique<Action>(attack->getAnimationTime());

  actualAnimation = attack.get();
  actualAnimation->init();
  pendingAction->addChild(std::static_pointer_cast<TimeDependend>(attack));

  Event::uPtr createAttack =
      std::make_unique<EventFunction>([this](double) {
        // from the middle of link the sword on the right
        Rectangle sword(16, -5, 16, 6);
        // Rectangle sword(getRectangleInWorld().getCenter(), 10,4);
        if (orientationH == Orientation::Left)
        {
          sword.setPosX(-sword.getPosX());
        }
        sword.setPosX(sword.getPosX() - sword.getWidth() / 2);
        sword.setPosY(sword.getPosY() - sword.getHeight() / 2);
        sword += getRectangleInWorld().getCenter();

        auto attack = std::make_unique<Attack>(room, attDmg, sword, 100.0, false, true, false, true, true);
        attack->getVelocity().setPosY(velocity.getPosY()); //take over vertical movement of player to the attack
        room->addAttack(std::move(attack));
      });
  pendingAction->addChild(std::make_shared<EventDelayed>(createAttack, 100.0));
}

void Player::enableAxe()
{
  axeEnabled = true;
}

void Player::startRangeAttack()
{
  if (pendingAction.get() != nullptr || !axeEnabled)
  {
    return;
  }

  pendingAction = std::make_unique<Action>(attackRange->getAnimationTime());
  actualAnimation = attackRange.get();
  actualAnimation->init();
  pendingAction->addChild(std::static_pointer_cast<TimeDependend>(attackRange));

  Event::uPtr createAttack =
      std::make_unique<EventFunction>([this](double) {
        auto axeAnim = RF.animations.getResource("axeRotating.txt");
        auto pos = axeAnim->getActualSize();
        Attack::uPtr attack;
        pos += getRectangleInWorld().getBase();
        //dpending on orientation, throw left right
        //and also start left or right of player
        if (orientationH == Orientation::Left)
        {
          pos += Point(-axeAnim->getActualSize().getWidth(), 0);
          attack = std::make_unique<Attack>(room, rngDmg, pos, 1000.0, false, true, false, false, false);
          attack->getVelocity().setPosX(-0.15);
        }
        else
        {
          pos += Point(getActualSize().getWidth(), 0);
          attack = std::make_unique<Attack>(room, rngDmg, pos, 1000.0, false, true, false, false, false);
          attack->getVelocity().setPosX(0.15);
        }
        attack->setAttackAnim(axeAnim);
        room->addAttack(std::move(attack));
      });
  pendingAction->addChild(std::make_shared<EventDelayed>(createAttack, 100));
}

void Player::startSuperAttack()
{
  if (pendingAction.get() != nullptr || superCharge.get() != superCharge.getMax())
  {
    return;
  }

  pendingAction = std::make_unique<Action>(superAttack->getAnimationTime());
  actualAnimation = superAttack.get();
  pendingAction->addChild(std::static_pointer_cast<TimeDependend>(superAttack));

  Event::uPtr createAttack = std::make_unique<EventFunction>([this](double) {
    auto superAnim = RF.animations.getResource("super.txt");
    auto pos = superAnim->getActualSize();
    Attack::uPtr attack;
    pos += getRectangleInWorld().getBase();
    pos += Point(-superAnim->getActualSize().getWidth() / 2 + getActualSize().getWidth() / 2, getActualSize().getHeight() - superAnim->getActualSize().getHeight() + 10);
    attack = std::make_unique<Attack>(room, sprDmg, pos, 1000.0, false, true, false, false, true);
    attack->setAttackAnim(superAnim);
    room->addAttack(std::move(attack));
  });

  pendingAction->addChild(std::make_shared<EventDelayed>(createAttack, 100));
  superCharge.set(0);

  Event::uPtr changeForm = std::make_unique<EventFunction>([this](double) {
    rest = RF.animations.getResource("nlink/idle.txt");
    go = RF.animations.getResource("nlink/move.txt");
    attack = RF.animations.getResource("nlink/attack.txt");
    attackRange = RF.animations.getResource("nlink/attackRng.txt");
    superAttack = RF.animations.getResource("nlink/superAttack.txt");
    actualAnimation = rest.get();
    superDuration.start(5000);
    superActive = true;
    attDmgPreSuper = attDmg;
    rngDmgPreSuper = rngDmg;
    attDmg = sprDmg;
    rngDmg = sprDmg / 2;
  });

  pendingAction->addChild(std::make_shared<EventDelayed>(changeForm, 400));
}

void Player::stopSuperAttack()
{
  pendingAction = std::make_unique<Action>(superAttack->getAnimationTime());
  actualAnimation = superAttack.get();
  pendingAction->addChild(std::static_pointer_cast<TimeDependend>(superAttack));

  Event::uPtr fireBack = std::make_unique<EventFunction>([this](double) {
    auto superAnim = RF.animations.getResource("super.txt");
    auto pos = superAnim->getActualSize();
    Attack::uPtr attack;
    pos += getRectangleInWorld().getBase();
    pos += Point(-superAnim->getActualSize().getWidth() / 2 + getActualSize().getWidth() / 2, getActualSize().getHeight() - superAnim->getActualSize().getHeight() + 10);
    attack = std::make_unique<Attack>(room, sprDmg, pos, 1000.0, false, true, false, false, true);
    attack->setAttackAnim(superAnim);
    room->addAttack(std::move(attack));
  });

  pendingAction->addChild(std::make_shared<EventDelayed>(fireBack, 100));

  Event::uPtr changeFormBack = std::make_unique<EventFunction>([this](double) {
    rest = RF.animations.getResource("link/idle.txt");
    go = RF.animations.getResource("link/move.txt");
    attack = RF.animations.getResource("link/attack.txt");
    attackRange = RF.animations.getResource("link/attackRng.txt");
    superAttack = RF.animations.getResource("link/superAttack.txt");
    actualAnimation = rest.get();
    attDmg = attDmgPreSuper;
    rngDmg = rngDmgPreSuper;
  });

  pendingAction->addChild(std::make_shared<EventDelayed>(changeFormBack, 400));
}

const Rectangle Player::getActualSize() const
{
  // return rest->getActualSize(); //alt
  return colSize; // neu
}

void Player::handleCollision(const Monster *monster)
{
  if (monster->getTouchDmg() > 0 && lastHit.isDone() && !superActive)
  {
    health -= monster->getTouchDmg();
    lastHit.start(INVUL_TIME_MS);
    SDL_Haptic *haptic = room->getIngame()->getGame()->getHaptic();
    // Initialize simple rumble
    if (SDL_HapticRumbleInit(haptic) != 0)
    {
      return;
    }
    // Play effect at 30% strength for 500ms
    if (SDL_HapticRumblePlay(haptic, 0.3, 500) != 0)
      return;
  }
}

void Player::handleCollision(const Attack *attack)
{
  if (attack->harmsPlayer() && attack->getTouchDmg() > 0 && lastHit.isDone() && !superActive)
  {
    health -= attack->getTouchDmg();
    lastHit.start(INVUL_TIME_MS);
  }
}

void Player::handleCollision(const Item *item)
{
  room->getIngame()->getHUD()->displayText("Obtained " + item->getItemShortDescription());
  inventory.push_back(room->transferItemFromRoom(item));
  if (!item->isConsumable())
  {
    switch (item->getItemCatagory())
    {
    case ItemCatagory::DOUBLEJUMP:
      enableSecondJump();
      break;

    case ItemCatagory::AXE:
      enableAxe();
      break;
    case ItemCatagory::RING:
      attDmg += item->getItemValue();
      rngDmg = attDmg / 2;
      break;
    default:
      break;
    }
  }

  // // inventory.push_back(std::make_unique<Item>(item->get, item->getItemValue()));
}

void Player::handleCollision(const Object *) {}

Rectangle Player::getCollisionRectWorld() const
{
  Rectangle actSize = colSize;
  actSize += position;
  return actSize;
}

void Player::save(PersistentInfo &saveData)
{
  saveData["Player/HPMax"] = health.getMax();
  saveData["Player/HP"] = health.get();
  saveData["Player/XPMax"] = experience.getMax();
  saveData["Player/XP"] = experience.get();
  saveData["Player/X"] = Point(position).getPosX();
  saveData["Player/Y"] = Point(position).getPosY();
  saveData["Player/Inventory"] = inventoryToString();
  saveData["Player/AttDmg"] = std::to_string(attDmg);
  saveData["Player/rooms"] = knownRoomsToString();
}

std::string Player::getActualRoom()
{
  return actualRoom.substr(RF.getResourcePath("maps").length());
}

void Player::flipRoomGravity()
{
  room->flipGravity();
}

std::set<std::string> &Player::getKnownRooms()
{
  return knownRooms;
}