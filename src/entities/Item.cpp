#include "Item.hpp"
#include "Player.hpp"
#include "EventFunction.hpp"
#include "EventCollision.hpp"
#include "ResourceFacade.hpp"

#include <sstream>

Item::Item(const ItemType::sPtr type_, const std::string &switchName) : Entity(type_.get(), switchName), type(std::move(type_)) {}

void Item::moveToX(const int posX)
{
  position.addPosX(posX - position.getPosX());
}

void Item::moveToY(const int posY)
{
  position.addPosY(posY - position.getPosY());
}

ItemCatagory Item::getItemCatagory() const
{
  return type->getTypeClass();
}

int Item::getItemValue() const
{
  return type->getValue();
}

bool Item::isConsumable() const
{
  return type->isConsumable();
}

const std::string &Item::getItemDescription() const
{
  return type->getDesciption();
}

const std::string Item::getItemShortDescription() const
{
  return type->getShort() + " " + std::to_string(getItemValue());
}

std::string Item::getItemString() const
{

  return getEntityString();
}

void Item::render(const Point &offset, const SDL_RendererFlip flip)
{
  Entity::render(offset + position, flip);
}

void Item::setPosition(Point position_)
{
  position = position_;
}

Item::uPtr Item::createItemFromString(const std::string &itemString)
{
  std::stringstream itemStream(itemString);
  std::string itemType, switchName;
  std::getline(itemStream, itemType, ';');
  Room::parseSwitch(itemType, switchName);

  return std::move(std::make_unique<Item>(RF.itemTypes.getResource(itemType), switchName));
}

void Item::removeMyself()
{
  room->addRemove(this);
}

void Item::handleCollision(const Player *)
{
  if (mySwitch.get() != nullptr)
    mySwitch->setOn();
}