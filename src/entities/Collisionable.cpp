#include "Collisionable.hpp"

//#include <memory>

#include "Player.hpp"
#include "Monster.hpp"
#include "Attack.hpp"
#include "Item.hpp"
#include "Object.hpp"
#include "DummyWall.hpp"

void Collisionable::handleCollision(const Collisionable *other)
{
  const Player *player = dynamic_cast<const Player *>(other);
  if (player != nullptr)
  {
    handleCollision(player);
    return;
  }
  const Monster *monster = dynamic_cast<const Monster *>(other);
  if (monster != nullptr)
  {
    handleCollision(monster);
    return;
  }
  const Attack *attack = dynamic_cast<const Attack *>(other);
  if (attack != nullptr)
  {
    handleCollision(attack);
    return;
  }
  const Item *item = dynamic_cast<const Item *>(other);
  if (item != nullptr)
  {
    handleCollision(item);
    return;
  }
  const DummyWall *wall = dynamic_cast<const DummyWall *>(other);
  if (wall != nullptr)
  {
    handleCollision(wall);
    return;
  }
  const Object *obj = dynamic_cast<const Object *>(other);
  if (wall != nullptr)
  {
    handleCollision(obj);
    return;
  }
}