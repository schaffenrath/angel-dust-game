#include "Object.hpp"

#include "Room.hpp"
#include "GameStateInGame.hpp"
#include "Player.hpp"
#include "ResourceFacade.hpp"

#include <regex>
#include <sstream>

Object::Object(ObjectType::sPtr type_, const Point &pos, const std::string &switchName_, const std::string &specialString_)
    : Entity(type_.get(), switchName_), position(pos), type(std::move(type_)), specialString(specialString_)
{
  //find LOCKED key phrase
  std::regex locked(";LOCKED:([\\d]+)"); //find e.g. LOCKED:12
  std::smatch res;
  if (std::regex_search(specialString, res, locked))
  {
    //number is is subgroup 1 paranthesis () give groups in regex
    lockedValue = stoi(res[1]);
    //remove keyphrase from special string
    specialString = res.prefix().str() + res.suffix().str();
  }

  std::regex textReg(";TEXT:(.+)\\\\n"); //find e.g. Text:this is a text\n
  if (std::regex_search(specialString, res, textReg))
  {
    //number is is subgroup 1 paranthesis () give groups in regex
    myText = res[1];
    //remove keyphrase from special string
    specialString = res.prefix().str() + res.suffix().str();
  }
}

Rectangle Object::getCollisionRectWorld() const
{
  Rectangle size = getAnimation(actualState).getActualSize();
  size += position;
  return size;
}

void Object::render(const Point &offset, const SDL_RendererFlip flip)
{
  Entity::render(position + offset, flip);

  if (lockedValue == 1)
  {
    Point lockOffset = position + offset;
    lockOffset.addPosY(32);
    RF.sprites.getResource("door/lock.txt")->render(lockOffset);
  }
}

void Object::onInteraction(Player *player)
{
  if (actualState == AnimationState::dead)
    return;

  switch (type->getInteractionType())
  {
  case Interaction::Warp:
    warp(specialString);
    break;
  case Interaction::Heal:
    try
    {
      player->addHealth(stoi(specialString));
    }
    catch (const std::invalid_argument &)
    {
      SDL_LogError(SDL_LOG_CATEGORY_ERROR, "%s", ("\"" + specialString + "\" is not a number in room " + room->getName()).c_str());
    }
    break;
  case Interaction::Text:
    showInteractionErrorText();
    break;
  case Interaction::Save:
    room->getIngame()->quicksave();
    showInteractionErrorText();
    break;
  case Interaction::Take:
  {
    auto spawnItem = Item::createItemFromString(specialString);
    spawnItem->setPosition(getCollisionRectWorld().getBase());
    room->moveItem(spawnItem);
    if (mySwitch.get() != nullptr)
    {
      mySwitch->setOn();
    }
  }
  break;
  case Interaction::Flip:
    player->flipRoomGravity();
    break;
  default:
    break;
  }
  if (type->isOnlyOnceInteractable())
  {
    actualState = AnimationState::dead;
  }
}

void Object::warp(const std::string warpTarget)
{
  //std::string lvlandposition = warpTarget.substr(pos + 1);

  std::vector<std::string> tokens;
  std::string token;
  std::stringstream tokenStream(warpTarget);

  if (warpTarget == "end;")
  {
    room->getIngame()->getGame()->transitionToEnd();
  }

  while (std::getline(tokenStream, token, ' '))
  {
    tokens.push_back(token);
  }
  if (tokens.size() == 3)
  {
    /* std::cout << "Warp " << line << "\n";
    std::cout << tokens[0] << "\n";
    std::cout << tokens[1] << "\n";
    std::cout << tokens[2] << "\n"; */

    room->getIngame()->registerLevelChange(Point(stoi(tokens[1]), stoi(tokens[2])), tokens[0]);
  }
}

void Object::handleCollision(const Player *player)
{
  if (player->isInteracting())
  {
    if (interactionIsAllowed(const_cast<Player *>(player)))
    {
      onInteraction(const_cast<Player *>(player));
    }
    else
    {
      showInteractionErrorText();
    }
  }
}

bool Object::interactionIsAllowed(Player *player)
{
  if (lockedValue != 0)
  {
    //look for key with fitting value in players inventory
    Inventory &inv = player->getInventory();
    auto gotIt = std::find_if(inv.begin(), inv.end(), [this](Item::uPtr &item) { return item->getItemCatagory() == ItemCatagory::KEY && item->getItemValue() == lockedValue; });
    if (gotIt == inv.end())
    {
      return false;
    }
    room->getIngame()->getHUD()->displayText("Used " + (*gotIt)->getItemShortDescription(), 2000);
    inv.erase(gotIt);
    //remember unlocked doors
    if (mySwitch.get() != nullptr)
    {
      mySwitch->setOn();
    }
  }
  return true;
}

void Object::showInteractionErrorText()
{
  std::string textTemplate(myText);
  textTemplate.reserve(60);
  if (myText.empty())
  {
    textTemplate = type->getText();
  }
  else if (textTemplate.empty())
  {
    textTemplate = "Cannot interact";
  }
  switch (lockedValue)
  {
  case 1:
    textTemplate.append(" you need key " + std::to_string(lockedValue));
    break;
  case 2:
    break;
  }
  room->getIngame()->getHUD()->displayText(textTemplate, 3000);
}

void Object::removeMyself()
{
  if (lockedValue != 0)
  {
    lockedValue = 0;
  }
  if (animationSet.find(AnimationState::dead) != animationSet.end())
  {
    actualState = AnimationState::dead;
  }
  room->addRemove(this);
}
