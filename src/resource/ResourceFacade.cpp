#include "ResourceFacade.hpp"
#include <iostream>
#include <string>
#include <SDL.h>

ResourceFacade RF{};

void ResourceFacade::init(Renderer::sPtr ren)
{
  texture.setRenderer(ren);
  sprites.setRenderer(ren);
  monsterTypes.setRenderer(ren);
  itemTypes.setRenderer(ren);
  sounds.setRenderer(ren);
  musics.setRenderer(ren);
  rooms.setRenderer(ren);
  animations.setRenderer(ren);

  actualRenderer = std::move(ren);
  fontMenu = Font(fontsPath + "statFont.ttf", 24);
  fontStat = Font(fontsPath + "statFont.ttf", 16);
  fontText = Font(fontsPath + "statFont.ttf", 20);
}

/* ResourceFacade &ResourceFacade::getI()
{
  static ResourceFacade fac;
  return fac;
}*/

Renderer *ResourceFacade::getRenderer() const
{
  return actualRenderer.get();
}

/* ResourceManager<Texture> &ResourceFacade::textures()
{
	static ResourceManager<Texture> text(getResourcePath("texture"));
  return text;
}

ResourceManager<Sprite> &ResourceFacade::sprites()
{
	static ResourceManager<Sprite> spr(getResourcePath("sprite"));
  return spr;
} */

std::string ResourceFacade::getResourcePath(const std::string &subDir)
{
  //We need to choose the path separator properly based on which
  //platform we're running on, since Windows uses a different
  //separator than most systems
#ifdef _WIN32
  const std::string PATH_SEP("\\");
#else
  const std::string PATH_SEP("/");
#endif
  //This will hold the base resource path: Lessons/res/
  //We give it static lifetime so that we'll only need to call
  //SDL_GetBasePath once to get the executable path
  static std::string baseRes;
  if (baseRes.empty())
  {
    //SDL_GetBasePath will return NULL if something went wrong in retrieving the path
    char *basePath = SDL_GetBasePath();
    if (basePath)
    {
      baseRes = basePath;
      SDL_free(basePath);
    }
    else
    {
      std::cerr << "Error getting resource path: " << SDL_GetError() << std::endl;
      return "";
    }
    //We replace the last bin/ with res/ to get the the resource path
    size_t pos = baseRes.rfind(PATH_SEP + "build");
    if (pos == std::string::npos)
    {
      pos = baseRes.rfind(PATH_SEP + "bin");
    }
    baseRes = baseRes.substr(0, pos) + PATH_SEP + "res" + PATH_SEP;
  }
  //If we want a specific subdirectory path in the resource directory
  //append it to the base path. This would be something like Lessons/res/Lesson0
  return subDir.empty() ? baseRes : baseRes + subDir + PATH_SEP;
}
