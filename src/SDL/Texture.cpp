#include "Texture.hpp"
#include <SDL_image.h>


Texture::Texture(Renderer *renderer, const Uint32 format, const int access, const int w, const int h) : renderer(renderer)
{
  texture = SDL_CreateTexture(**renderer, format, access, w, h);
  if (texture == nullptr)
  {
    throw std::runtime_error("Texture could not be created");
  }
}

Texture::Texture(Renderer *renderer, const std::string &file) : renderer(renderer)
{
  texture = IMG_LoadTexture(**renderer, file.c_str());
  if (texture == nullptr)
  {
    throw std::runtime_error("File \"" + file + "\" not found!");
  }
}

Texture::Texture(Renderer *renderer, SDL_Texture *tex) : renderer(renderer), texture(tex){}

Texture::Texture(Texture &&copy)
{
  this->texture = copy.texture;
  this->renderer = copy.renderer;
  copy.texture = nullptr;
}

Texture::~Texture()
{
  if (texture != nullptr)
  {
    SDL_DestroyTexture(texture);
  }
}

Rectangle Texture::getSize() const
{
  SDL_Rect info;
  info.x = 0;
  info.y = 0;
  SDL_QueryTexture(texture, NULL, NULL, &info.w, &info.h);
  return Rectangle(info);
}

void Texture::render(const Rectangle &dst, const Rectangle &clip)
{
  SDL_RenderCopy(**renderer, texture, *clip, *dst);
}

void Texture::render(const Point &position, const Rectangle &clip)
{
  render(Rectangle(position, clip.getWidth(), clip.getHeight()), clip);
}

void Texture::render(const Point &position, const Rectangle &clip, const SDL_RendererFlip flip)
{
  render(Rectangle(position, clip.getWidth(), clip.getHeight()), clip, flip);
}

void Texture::render(const Rectangle &dst, const Rectangle &clip, const double angle, const Point &center, const SDL_RendererFlip flip)
{
  SDL_RenderCopyEx(**renderer, texture, *clip, *dst, angle, *center, flip);
}

void Texture::render(const Rectangle &dst, const Rectangle &clip, const SDL_RendererFlip flip)
{
  SDL_RenderCopyEx(**renderer, texture, *clip, *dst, 0.0, NULL, flip);
}

int Texture::getCenterPosX(int size)
{
  return getCenterPos(getSize().getWidth(), size);
}

int Texture::getCenterPosY(int size)
{
  return getCenterPos(getSize().getHeight(), size);
}

int Texture::getCenterPos(int actualSize, int centerSize)
{
  return (centerSize - actualSize) / 2;
}

Texture::sPtr Texture::load(Renderer &ren, const std::string &filename)
{
  return std::make_shared<Texture>(&ren, filename);
}
