#include "Point.hpp"

#include <algorithm>

/* Point::Point(const int x, const int y)
{
  point.x = x;
  point.y = y;
}

void Point::setPosX(int value)
{
  point.x = value;
}
void Point::setPosY(int value)
{
  point.y = value;
}

void Point::addPosX(int value)
{
  point.x += value;
}

void Point::addPosY(int value)
{
  point.y += value;
}

Point Point::operator+(const Point othPoint) const
{
  return Point(point.x + othPoint.point.x, point.y + othPoint.point.y);
}

Point Point::operator-(const Point othPoint) const
{
  return Point(point.x - othPoint.point.x, point.y - othPoint.point.y);
}

Point Point::operator*(const int scalar) const
{
  return Point(point.x * scalar, point.y * scalar);
}

Point Point::operator/(const int scalar) const
{
  return Point(point.x / scalar, point.y / scalar);
}

void Point::operator+=(const Point &othPoint)
{
  point.x += othPoint.getPosX();
  point.y += othPoint.getPosY();
}

void Point::operator-=(const Point &othPoint)
{
  point.y -= othPoint.getPosY();
  point.x -= othPoint.getPosX();
}

Point Point::operator-() const
{
  return Point(-point.x, -point.y);
}
Point Point::max(const Point &othPoint) const
{
  return Point(std::max(point.x, othPoint.getPosX()), std::max(point.y, othPoint.getPosY()));
}

Point Point::min(const Point &othPoint) const
{
  return Point(std::min(point.x, othPoint.getPosX()), std::min(point.y, othPoint.getPosY()));
} */