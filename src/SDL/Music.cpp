#include "Music.hpp"


Music::Music(const std::string &file)
{
  music = Mix_LoadMUS(file.c_str());
  if (music == nullptr)
  {
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO,"Mix_LoadMUS: %s\n", Mix_GetError());
  }
}

Music::Music(Music &&copy)
{
  this->music = copy.music;
  copy.music = nullptr;
}

Music::~Music()
{
  if (music != nullptr)
  {
    Mix_FreeMusic(music);
  }
}

/* Play music with number of loops (-1 plays forever)  */
void Music::play(const int loops){
  if(Mix_PlayMusic(music, loops) == -1){
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO,"Mix_PlayMusic: %s\n", Mix_GetError());
  }
}

void Music::pauseResume(){
  if (Mix_PausedMusic()){
    Mix_PauseMusic();
  } else {
    Mix_ResumeMusic();
  }
}

void Music::stop(){
  Mix_HaltMusic();
}

void Music::volume(const int volume){
  SDL_LogInfo(SDL_LOG_CATEGORY_AUDIO, "changed music volume from %d to %d \n", Mix_VolumeMusic(-1), volume);
  Mix_VolumeMusic(volume);
}

Music::sPtr Music::load(Renderer &, const std::string filename){
  return std::make_shared<Music>(filename);
}
