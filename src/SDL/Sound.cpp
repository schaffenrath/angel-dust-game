#include "Sound.hpp"

Sound::Sound(const std::string &file)
{
  sound = Mix_LoadWAV(file.c_str());
  if (sound == nullptr)
  {
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO, "Mix_LoadWAV: %s\n", Mix_GetError());
  }
}

Sound::Sound(Sound &&copy)
{
  this->sound = copy.sound;
  copy.sound = nullptr;
}

Sound::~Sound()
{
  if (sound != nullptr)
  {
    Mix_FreeChunk(sound);
  }
}

void Sound::play(const int channel, const int loops){
  if (Mix_PlayChannel(channel, sound, loops) == -1){
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO, "Mix_PlayChannel: %s\n", Mix_GetError());
  }
}

Sound::sPtr Sound::load(Renderer &, const std::string filename){
  return std::make_shared<Sound>(filename);
}
