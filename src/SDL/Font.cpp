#include "Font.hpp"

Font::Font(const std::string &fontFile, const int fontSize)
{
  font = TTF_OpenFont(fontFile.c_str(), fontSize);
  if (!font)
  {
    //SDL_LogError(SDL_LOG_CATEGORY_RENDER, "TTF_OpenFont: %s\n", TTF_GetError());
    throw std::runtime_error("File \"" + fontFile + "\" not found! Error " + TTF_GetError());
  }
}

Font::Font(Font &&oth)
{
  font = oth.font;
  oth.font = nullptr;
}

Font &Font::operator=(Font &&oth)
{
  font = oth.font;
  oth.font = nullptr;
  return *this;
}

Font::~Font()
{
  if (!font)
    TTF_CloseFont(font);
}
