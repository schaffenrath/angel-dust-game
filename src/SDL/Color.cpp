#include "Color.hpp"

const Color &Color::White()
{
  static Color c(255, 255, 255);
  return c;
}
const Color &Color::Blue()
{
  static Color c(0, 0, 255);
  return c;
}
const Color &Color::Red()
{
  static Color c(255, 0, 0);
  return c;
}
const Color &Color::Green()
{
  static Color c(0, 255, 0);
  return c;
}
const Color &Color::Black()
{
  static Color c(0, 0, 0);
  return c;
}
const Color &Color::Gray()
{
  static Color c(100, 100, 100, 50);
  return c;
}