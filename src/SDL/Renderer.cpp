#include "Renderer.hpp"

Renderer::Renderer(Window::sPtr window, const int index, const Uint16 flags) : window(std::move(window))
{
  ren = SDL_CreateRenderer(**(this->window), index, flags);
  SDL_SetRenderDrawBlendMode(ren, SDL_BLENDMODE_BLEND);
}

Renderer::~Renderer()
{
  SDL_DestroyRenderer(ren);
}

void Renderer::clear()
{
  SDL_RenderClear(ren);
}

void Renderer::present()
{
  SDL_RenderPresent(ren);
}

void Renderer::setLogicalSize(const int xPixels, const int yPixels)
{
  SDL_RenderSetLogicalSize(ren, xPixels, yPixels);
}

int Renderer::getPixelWidth() const
{
  int w;
  SDL_RenderGetLogicalSize(ren, &w, nullptr);
  return w;
}

int Renderer::getPixelHeight() const
{
  int h;
  SDL_RenderGetLogicalSize(ren, nullptr, &h);
  return h;
}

void Renderer::draw(const Rectangle &rect, bool fill)
{
  SDL_RenderDrawRect(ren, *rect);
  if (fill)
  {
    SDL_RenderFillRect(ren, *rect);
  }
}

void Renderer::draw(const Point &pt)
{
  SDL_RenderDrawPoint(ren, pt.getPosX(), pt.getPosY());
}

void Renderer::setDrawColor(const Color &color)
{
  SDL_SetRenderDrawColor(ren, color.red, color.green, color.blue, color.alpha);
}