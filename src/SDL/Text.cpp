#include "Text.hpp"

Text::Text(const std::string &text) : text(text)
{}

Texture Text::toTexture(Renderer &renderer, const Font &font, const Color &color)
{
  SDL_Surface *surf = TTF_RenderText_Blended(*font, text.c_str(), color.toSDL());

  SDL_Texture *texture = SDL_CreateTextureFromSurface(*renderer, surf);

  Texture tex(&renderer, texture);
  SDL_FreeSurface(surf);
  return tex;
}