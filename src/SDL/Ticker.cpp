#include "Ticker.hpp"
#include "SDL_timer.h"

void Ticker::start()
{
  startTime = SDL_GetTicks();
}

unsigned int Ticker::getTicks() const
{
  return SDL_GetTicks() - startTime;
}

bool Ticker::areTicksPassed(const unsigned int ticks) const{
  return SDL_TICKS_PASSED(getTicks() , ticks);
}

void Ticker::delay(const unsigned int totalTicks) const{
  if(!areTicksPassed(totalTicks)){
    SDL_Delay(totalTicks - getTicks());
  }
}