#include "Window.hpp"

Window::Window(const std::string &windowText, const int x, const int y, const int width, const int height, const Uint16 flags) : size(x, y, width, height)
{
  window = SDL_CreateWindow(windowText.c_str(), x, y, width, height, flags);
}

Window::~Window()
{
  SDL_DestroyWindow(window);
}

int Window::getPixelWidth() const
{
  return size.getWidth();
}

int Window::getPixelHeight() const
{
  return size.getHeight();
}

float Window::getCurrentDisplayRatio(){
  SDL_DisplayMode info;
  SDL_GetCurrentDisplayMode(0, &info);
  return (float) info.w/info.h;
}