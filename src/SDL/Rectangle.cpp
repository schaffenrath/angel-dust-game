#include "Rectangle.hpp"
#include <SDL_render.h>

Rectangle::Rectangle(const int x, const int y, const int w, const int h)
{
  rec.x = x;
  rec.y = y;
  rec.w = w;
  rec.h = h;
}

Rectangle::Rectangle(const Point &position, const int w, const int h)
{
  rec.x = position.getPosX();
  rec.y = position.getPosY();
  rec.w = w;
  rec.h = h;
}

Rectangle::Rectangle(const Point &position, const Point &otherCorner)
{
  //Todo: check if position < otherCorner?
  rec.x = position.getPosX();
  rec.y = position.getPosY();
  rec.w = otherCorner.getPosX() - position.getPosX();
  rec.h = otherCorner.getPosY() - position.getPosY();
}

Rectangle::Rectangle(const SDL_Rect rec)
{
  this->rec = rec;
}

SDL_bool Rectangle::intersects(const Rectangle &otherRectangle)
{
  return SDL_HasIntersection(&this->rec, &otherRectangle.rec);
}

Rectangle Rectangle::getIntersetion(const Rectangle &otherRectangle)
{
  Rectangle r;
  SDL_IntersectRect(&this->rec, &otherRectangle.rec, &r.rec);
  return r;
}

void Rectangle::setPosX(const int val)
{
  rec.x = val;
}

void Rectangle::setPosY(const int val)
{
  rec.y = val;
}

void Rectangle::setWidth(const int val)
{
  rec.w = val;
}

void Rectangle::setHeight(const int val)
{
  rec.h = val;
}

Point Rectangle::getBase() const{
  return Point(rec.x, rec.y);
}

Point Rectangle::getOppositePoint() const{
  return Point(rec.x + rec.w, rec.y + rec.h);
}

Point Rectangle::getCenter() const{
  return Point(rec.x + rec.w/2, rec.y + rec.h/2);
}

VectorI Rectangle::getSize() const{
  return VectorI(rec.w, rec.h);
}

void Rectangle::move(const int dx, const int dy){
  this->operator+=(Point(dx,dy));
}

void Rectangle::operator+=(const Point offset){
  rec.x += offset.getPosX();
  rec.y += offset.getPosY();
}

void Rectangle::operator-=(const Point offset){
  rec.x -= offset.getPosX();
  rec.y -= offset.getPosY();
}