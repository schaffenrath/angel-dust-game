#include "Timer.hpp"

void Timer::start(double timeDelay)
{
  restTime = timeDelay;
}

void Timer::step(double timeStepMS)
{
  if (!isDone())
  {
    restTime -= timeStepMS;
  }
}

bool Timer::isDone() const
{
  return restTime <= 0;
}