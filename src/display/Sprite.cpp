#include "Sprite.hpp"
#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include "ResourceFacade.hpp"

Sprite::Sprite(const Texture::sPtr texture) : area(texture->getSize()), texture(texture)
{
}

Sprite::Sprite(const Rectangle area, const Texture::sPtr texture) : area(area), texture(texture)
{
}

void Sprite::render(const Point &point, const SDL_RendererFlip flip_)
{
  auto newFlip = static_cast<SDL_RendererFlip>(flip_ ^ flip);
  texture->render(Rectangle(point, area.getWidth() * scaling.getPosX(), area.getHeight() * scaling.getPosY()), area, newFlip);
}

void Sprite::render(const Point &point, const double angle, const Point anglepoint, const SDL_RendererFlip flip_)
{
  auto newFlip = static_cast<SDL_RendererFlip>(flip_ ^ flip);
  texture->render(Rectangle(point, area.getWidth() * scaling.getPosX(), area.getHeight() * scaling.getPosY()), area, angle, anglepoint,  newFlip);
}

Sprite::sPtr Sprite::load(Renderer &, const std::string &fileName)
{
  std::ifstream file(fileName);
  if (!file.is_open())
  {
    throw std::runtime_error("File \"" + fileName + "\" not found!");
  }
  std::string textureFileName;
  std::getline(file, textureFileName);

  try
  {
    if (file.eof())
    {
      return std::make_shared<Sprite>(RF.texture.getResource(textureFileName));
    }
    else
    {
      int vals[4];
      for (int i = 0; i < 4; i++)
      {
        std::string integer;
        std::getline(file, integer);
        if (integer == "")
          return std::make_shared<Sprite>(RF.texture.getResource(textureFileName));
        vals[i] = std::stoi(integer);
      }
      auto sprite = std::make_shared<Sprite>(Rectangle(vals[0], vals[1], vals[2], vals[3]), RF.texture.getResource(textureFileName));
      if (!file.eof())
      {
        std::string line;
        while (std::getline(file, line))
        {
          size_t pos = line.find(" ");
          std::string keyA = line.substr(0, pos);
          if (keyA == "XS")
          {
            sprite->setScalingX(std::stof(line.substr(pos + 1)));
          }
          else if (keyA == "YS")
          {
            sprite->setScalingY(std::stof(line.substr(pos + 1)));
          }
          else if (keyA == "US")
          {
            sprite->setScalingUniform(std::stof(line.substr(pos + 1)));
          }
          else if (keyA == "FLIPH")
          {
            sprite->addFlip(SDL_RendererFlip::SDL_FLIP_HORIZONTAL);
          }
          else if (keyA == "FLIPV")
          {
            sprite->addFlip(SDL_RendererFlip::SDL_FLIP_VERTICAL);
          }
        }
      }
      return sprite;
    }
  }
  catch (const std::runtime_error &e)
  {
    throw std::runtime_error(std::string(e.what()) + " caused in file \"" + fileName + "\"");
  }
}

const Rectangle Sprite::getActualSize() const
{
  return Rectangle(0, 0, area.getWidth() * scaling.getPosX(), area.getHeight() * scaling.getPosY());
}
