#include "Camera.hpp"
#include "Point2.hpp"
#include "EventFunction.hpp"


Camera::Camera(const Renderer &ren) : position(0, 0, ren.getPixelWidth(), ren.getPixelHeight()),
                                      playerArea(position.getWidth()/3, position.getHeight()/3, position.getWidth() - 2*position.getWidth()/3, position.getHeight() - 2*position.getHeight()/3),
                                      shakeIntensity(0),
                                      //movement(0, 0),
                                      followPlayer(true)
{
}

void Camera::setRoom(const std::shared_ptr<Room> room)
{
  this->room = std::move(room);
}

void Camera::clearRoom()
{
  room.reset();
}

std::shared_ptr<Room> Camera::getRoom()
{
  return room;
}

void Camera::render()
{
  if (shakeIntensity <= 0)
  {
    shakeIntensity = 0;
  }
  else
  {
    shakeIntensity--;
  }
  room->render(-Point(position.getPosX() + shakeIntensity, position.getPosY() + shakeIntensity));
}

/*bool Camera::move2()
{
  if (!movement.isZero())
  {
    move(movement.getPosX(), movement.getPosY());
    movement -= movement;
  }
  return false;
}*/

void Camera::move(const int dx, const int dy)
{
  position += Point(dx, dy);
}

void Camera::setPosition(const int x, const int y)
{
  position.setPosX(x);
  position.setPosY(y);
}

void Camera::shake(unsigned int shake)
{
  shakeIntensity = shake;
}

void Camera::pollEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  room->invoke(events, timeStep);

  if (!followPlayer)
  {
    return;
  }

  Point movement{0, 0};

  Rectangle actualView(playerArea);
  actualView += position.getBase();
  Rectangle playerPos = room->getPlayer()->getRectangleInWorld();
  VectorI leftTop = playerPos.getBase() - actualView.getBase();
  if (leftTop.getPosX() < 0)
  {
    movement.addPosX(leftTop.getPosX());
  }
  if (leftTop.getPosY() < 0)
  {
    movement.addPosY(leftTop.getPosY());
  }
  VectorI rightBottom = playerPos.getOppositePoint() - actualView.getOppositePoint();
  if (rightBottom.getPosX() > 0)
  {
    movement.addPosX(rightBottom.getPosX());
  }
  if (rightBottom.getPosY() > 0)
  {
    movement.addPosY(rightBottom.getPosY());
  }

  movement = movement.max(Point(0, 0) - position.getBase()).min(room->getFarestPoint() - position.getOppositePoint());

  if (!movement.isZero())
  {
    events.push(std::make_unique<EventFunction>([this, movement](double ) { move(movement.getPosX(), movement.getPosY()); }));
  }
}
