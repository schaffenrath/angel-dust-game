#include "Room.hpp"
#include "ResourceFacade.hpp"
#include "EventCollision.hpp"
#include "EventFunction.hpp"
#include "GameStateInGame.hpp"
#include "Color.hpp"
#include "Text.hpp"
#include "Font.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <regex>

Room::Room(const std::string &name_,
           std::unique_ptr<std::vector<std::string>> &structure,
           std::unique_ptr<std::map<char, Sprite::sPtr>> &tilemap,
           const Animation::sPtr background,
           const std::shared_ptr<Texture> foreground,
           const std::shared_ptr<Music> bgmusic,
           unsigned long maxlevel,
           std::list<Monster> &monsters_,
           std::list<Object::uPtr> &objects_)
    : Renderable(),
      name(name_),
      structure(std::move(structure)),
      tilemap(std::move(tilemap)),
      background(std::move(background)),
      foreground(std::move(foreground)),
      bgmusic(std::move(bgmusic)),
      size(0, 0, maxlevel, this->structure->size()),
      player(),
      maxlevel(maxlevel),
      objects(std::move(objects_)),
      monsters(std::move(monsters_))
{
  if (foreground)
  {
    fganim = foreground->getSize().getWidth(); //fganimation always active
  }
  //items.push_back(Item::createItemInstance(Point(26 * 32, 4 * 32), Effect::lock, 1));
}

Room::~Room()
{
  //bgmusic->stop();
}

void Room::setMusicVolume(int volume)
{
  bgmusic->volume(volume);
}

void Room::setIngame(GameStateInGame *_ingame)
{
  ingame = _ingame;

  for (Monster &mon : monsters)
  {
    mon.setRoom(this);
  }
  for (auto &obj : objects)
  {
    obj->setRoom(this);
  }
  //Remove objects obsolete due to switches
  removeObsoleteObjects();
}

void Room::invoke(std::queue<Event::uPtr> &events, double timeStep)
{
  background->step(timeStep);

  for (auto &monster : monsters)
  {
    if (monster.getRectangleInWorld().intersects(player->getRectangleInWorld()))
    {
      events.push(std::make_unique<EventCollision>(&monster, player.get()));
    }
    for (auto &att : attacks)
    {
      if (monster.getRectangleInWorld().intersects(att->getCollisionRectWorld()))
      {
        events.push(std::make_unique<EventCollision>(&monster, att.get()));
      }
    }
    checkWallCollisions(events, &monster, timeStep);

    monster.invoke(events, timeStep);
  }

  //go over all attacks
  for (auto it1 = attacks.begin(); it1 != attacks.end(); it1++)
  {
    if (player->getRectangleInWorld().intersects((*it1)->getCollisionRectWorld()))
    {
      events.push(std::make_unique<EventCollision>(player.get(), (*it1).get()));
    }
    for (auto it2 = it1; it2 != attacks.end(); it2++)
    {
      if (it2 == it1)
        continue;
      if ((*it2)->getRectangleInWorld().intersects((*it1)->getCollisionRectWorld()))
      {
        events.push(std::make_unique<EventCollision>((*it2).get(), (*it1).get()));
      }
    }
    checkWallCollisions(events, it1->get(), timeStep);
    (*it1)->invoke(events, timeStep);
  }

  for (auto &item : items)
  {
    if (player->getRectangleInWorld().intersects(item->getCollisionRectWorld()))
    {
      events.push(std::make_unique<EventCollision>(player.get(), item.get()));
      toRemoveItems.push_back(item.get());
    }
  }

  for (auto &obj : objects)
  {
    if (player->getRectangleInWorld().intersects(obj->getCollisionRectWorld()))
    {
      events.push(std::make_unique<EventCollision>(player.get(), obj.get()));
    }
    obj->invoke(events, timeStep);
  }

  player->invoke(events, timeStep);
  events.push(std::make_unique<EventFunction>(std::bind(&Room::collisionRoom, this, static_cast<Moveable *>(player.get()), std::placeholders::_1)));
  for (auto &monster : monsters)
    events.push(std::make_unique<EventFunction>(std::bind(&Room::collisionRoom, this, static_cast<Moveable *>(&monster), std::placeholders::_1)));
  for (auto &item : items)
    events.push(std::make_unique<EventFunction>(std::bind(&Room::itemGravity, this, item.get(), std::placeholders::_1)));
}

void Room::renderMap(const Point &offset, int i_start, int j_start, double scalingFactor, const SDL_RendererFlip flip)
{
  // render room for mapdisplay
  int i = i_start;
  for (auto line : *structure)
  {
    int j = j_start;
    for (auto charect : line)
    {
      if (charect != '.')
      {
        auto it = tilemap->find(charect);
        if (it != tilemap->end())
        {
          it->second->setScalingX(it->second->getScalingX() / scalingFactor);
          it->second->setScalingY(it->second->getScalingY() / scalingFactor);
          it->second->render(offset + Point(j * TILESIZE * scalingFactor / 4.0, i * TILESIZE * scalingFactor / 4.0), flip);
          it->second->setScalingX(it->second->getScalingX() * scalingFactor);
          it->second->setScalingY(it->second->getScalingY() * scalingFactor);
        }
      }
      j++;
    }
    i++;
  }
  getDoors();
}

void Room::renderDeath(const Point &offset, const double angle, const SDL_RendererFlip flip)
{
  // render room itself
  int i = 0;
  for (auto line : *structure)
  {
    int j = 0;
    for (auto charect : line)
    {
      if (charect != '.')
      {
        auto it = tilemap->find(charect);
        if (it != tilemap->end())
        {
          it->second->render(offset + Point(j * TILESIZE, i * TILESIZE), angle, Point(TILESIZE / 2, TILESIZE / 2), flip);
        }
      }
      j++;
    }
    i++;
  }
  std::list<std::string> text{
      "GAME OVER"};

  int statOffset = 0;
  while (!text.empty())
  {
    Texture(Text(text.front()).toTexture(*RF.getRenderer(), RF.fontMenu, Color::White())).render(Point(100, 100 + statOffset));
    text.pop_front();
    statOffset += 20;
  }
}

void Room::render(const Point &offset, const SDL_RendererFlip flip)
{
  // render background
  if (background)
  {
    auto bgwidth = background->getActualSize().getWidth();
    auto bgheight = background->getActualSize().getHeight();

    for (unsigned int j = 0; j < structure->size() * TILESIZE / bgheight + 2; j++)
    {
      for (unsigned int i = 0; i < maxlevel * TILESIZE / bgwidth + 2; ++i)
      { //+2 to render longer than viewable
        background->render(offset / 2 + Point(i * bgwidth, j * bgheight), flip);
      }
    }
  }

  // render room itself
  int i = 0;
  for (auto line : *structure)
  {
    int j = 0;
    for (auto charect : line)
    {
      if (charect != '.')
      {
        auto it = tilemap->find(charect);
        if (it != tilemap->end())
        {
          it->second->render(offset + Point(j * TILESIZE, i * TILESIZE), flip);
        }
      }
      j++;
    }
    i++;
  }
  int flip_ = SDL_FLIP_NONE;
  if (gravity < 0)
  {
    flip_ = flip ^ SDL_FLIP_VERTICAL;
  }

  for (auto &obj : objects)
    obj->render(offset, flip);
  // render objects
  for (auto &monster : monsters)
    monster.render(offset, static_cast<SDL_RendererFlip>(flip_));
  //render items
  for (auto &item : items)
    item->render(offset, flip);
  // render player
  player->render(offset, static_cast<SDL_RendererFlip>(flip_));

  for (auto &att : attacks)
  {
    att->render(offset, flip);
  }

  //render foreground
  if (foreground)
  {
    fganim--;
    if (fganim < 0)
    {
      fganim = foreground->getSize().getWidth();
    }
    auto fgwidth = foreground->getSize().getWidth();
    //std::cout << "maxlevel =" << maxlevel << " bgwidth= " << bgwidth << "loops" <<  maxlevel*32/bgwidth +1 << std::endl;
    for (unsigned long i = 0; i < maxlevel * TILESIZE / fgwidth * 2 + 4; ++i)
    { //+4 to render before and after
      foreground->render(Point((i - 2) * fgwidth + fganim + 2 * offset.getPosX(), offset.getPosY() + 32), flip);
    }
  }
}

void Room::setPlayer(Player::sPtr player_)
{
  player = std::move(player_);
  player->setRoom(this);
  bgmusic->play(-1);
}

Player *Room::getPlayer() { return player.get(); }

int Room::getPixelWidth() const
{
  return size.getWidth() * TILESIZE;
}

int Room::getPixelHeight() const
{
  return size.getHeight() * TILESIZE;
}

Point Room::getFarestPoint() const
{
  return size.getOppositePoint() * TILESIZE;
}

void Room::collisionRoom(Moveable *character, double)
{
  if (character == nullptr)
    return;

  const auto position{character->getRectangleInWorld()};

  // if character is outside of defined structure
  if (position.getPosX() <= 0 ||
      (((std::size_t)position.getPosY() + character->getRectangleInWorld().getHeight()) / TILESIZE >= structure->size() ||
       (std::size_t)((position.getPosX() + 1) / TILESIZE) >= structure->at((position.getPosY()) / TILESIZE).size()))
  {
    if (character->getVelocity().getPosY() == 0 && character->isGravityAffected())
      character->startGoDown(gravity);
    return;
  }

  // if character collides with floor or roof
  if (areObjectsBellowAbove(character->getRectangleInWorld(), character->getVelocity()))
  {
    Rectangle testRec = character->getRectangleInWorld();
    if (character->getVelocity().getPosX() < 0)
      testRec += Point(5, 0);

    else if (character->getVelocity().getPosX() > 0)
      testRec += Point(-5, 0);

    if (character->shouldFallThrough() && structure->at((position.getPosY() + character->getActualSize().getHeight()) / TILESIZE).at(position.getPosX() / TILESIZE) == 'p')
    {
      if (character->getVelocity().getPosY() == 0)
        character->startGoDown(gravity);
      if (structure->at((position.getPosY() + character->getActualSize().getHeight() + 5) / TILESIZE).at(position.getPosX() / TILESIZE) != 'p')
        character->stopFallThrough();
    }

    else if (structure->at((position.getPosY() + character->getActualSize().getHeight()) / TILESIZE).at(position.getPosX() / TILESIZE) == 'p' && structure->at((position.getPosY() + character->getActualSize().getHeight() - 5) / TILESIZE).at(position.getPosX() / TILESIZE) == 'p')
    {
      if (character->getVelocity().getPosY() == 0)
        character->startGoDown(gravity);
    }

    else if (areObjectsBellowAbove(testRec, character->getVelocity()))
    {
      // if falling and colliding with floor
      if (!character->isJumping() && character->getVelocity().getPosY() > 0)
      {
        character->stopSecondJump();
        character->startGoUp(gravity);
        character->moveToPosY(((position.getPosY() + character->getActualSize().getHeight()) / TILESIZE) * TILESIZE - character->getActualSize().getHeight());
        //character->moveToPosY(((character->getRectangleInWorld().getOppositePoint().getPosY()) / TILESIZE) * TILESIZE + 1);
      }

      // if jumping and colliding with roof
      else if (character->getVelocity().getPosY() < 0 && character->isGravityAffected() && structure->at((position.getPosY()) / TILESIZE).at(position.getPosX() / TILESIZE) != 'p')
      {
        character->setVelocity(VectorD(character->getVelocity().getPosX(), 0.0));
        character->stopJump();
        character->startGoDown(gravity);
        character->moveToPosY(((position.getPosY()) / TILESIZE + 1) * TILESIZE);
      }
    }
    else if (character->getVelocity().getPosY() == 0 && !character->isJumping() && character->isGravityAffected())
      character->startGoDown(gravity);
  }

  //otherwise fall
  else if (character->getVelocity().getPosY() == 0 && !character->isJumping() && character->isGravityAffected())
  {
    character->startGoDown(gravity);
  }

  // if character collides right or left
  if (areObjectsLeftRight(character->getRectangleInWorld(), character->getVelocity()))
  {
    // if character moves left
    if (character->getVelocity().getPosX() < 0)
    {
      character->moveToPosX(((position.getPosX()) / TILESIZE + 1) * TILESIZE);
    }
    // if character moves right
    else if (character->getVelocity().getPosX() > 0)
    {
      character->moveToPosX(((position.getPosX() + character->getActualSize().getWidth()) / TILESIZE) * TILESIZE - character->getActualSize().getWidth() - 1);
    }
  }
}

void Room::itemGravity(Item *item, double)
{
  if (item == nullptr)
    return;

  int i = (item->getCollisionRectWorld().getPosY() + item->getCollisionRectWorld().getHeight()) / TILESIZE;
  while ((size_t)i < structure->size() && structure->at(i).at(item->getCollisionRectWorld().getPosX() / TILESIZE) == '.')
  {
    i++;
  }
  if ((size_t)i < structure->size() - 1)
    item->moveToY(i * TILESIZE - item->getCollisionRectWorld().getHeight());
  return;
}

void Room::checkWallCollisions(std::queue<Event::uPtr> &events, Collisionable *collisionable, double timeStep)
{
  Point topLeft = collisionable->getCollisionRectWorld(timeStep).getBase();
  Point bottomRight = collisionable->getCollisionRectWorld(timeStep).getOppositePoint();
  for (int j = topLeft.getPosY() / TILESIZE; j <= bottomRight.getPosY() / TILESIZE; j++)
  {
    for (int i = topLeft.getPosX() / TILESIZE; i <= bottomRight.getPosX() / TILESIZE; i++)
    {
      if ((*structure)[j][i] != '.')
      {
        DummyWall::uPtr wall = std::make_unique<DummyWall>(Rectangle(i * TILESIZE, j * TILESIZE, TILESIZE, TILESIZE));
        events.push(std::make_unique<EventCollision>(collisionable, wall.get()));
        tempWalls.push_front(std::move(wall));
      }
    }
  }
}

void Room::addAttack(Attack::uPtr &&newAttack)
{
  attacks.push_front(std::move(newAttack));
}

void Room::applyEffect(const std::string &type, const int value)
{
  if (type == "HP")
    player->addHealth(value);
  else if (type == "XP")
    player->addExp(value);
}

void Room::moveItem(Item::uPtr &item)
{
  item->setRoom(this);
  items.push_back(std::move(item));
}

Item::uPtr Room::transferItemFromRoom(const Item *item)
{
  for (auto &it : items)
  {
    if (it.get() == item)
    {
      //removing empty unique_ptr because content was moved
      toRemoveItems.push_back(nullptr);
      return std::move(it);
    }
  }
  return Item::uPtr(nullptr);
}

void Room::removeAttack(Attack *attack)
{
  for (auto &mon : monsters)
  {
    mon.removeFadingAttacks(attack);
  }
  for (auto &att : attacks)
  {
    att->removeFadingAttacks(attack);
  }
  attacks.remove_if([attack](Attack::uPtr &att) { return att.get() == attack; });
}

void Room::removeMonster(Monster *monster)
{
  monsters.erase(std::find_if(monsters.begin(), monsters.end(), [monster](Monster &mon) { return (&mon) == monster; }));
}

void Room::removeItem(Item *item)
{
  for (auto it = items.begin(); it != items.end(); it++)
  {
    if ((*it).get() == item)
    {
      items.erase(it);
      break;
    }
  }
}

Room::sPtr Room::load(Renderer &, std::string filename)
{
  std::ifstream roomfile;
  std::string line;
  std::unique_ptr<std::vector<std::string>> structure =
      std::make_unique<std::vector<std::string>>();

  Animation::sPtr background;
  Texture::sPtr foreground;
  Music::sPtr bgmusic;

  std::unique_ptr<std::map<char, Sprite::sPtr>> tilemap =
      std::make_unique<std::map<char, Sprite::sPtr>>();
  std::list<Monster> monsters{};
  std::list<Object::uPtr> objects{};

  size_t maxlevel = 0;

  roomfile.open(filename);
  if (roomfile.is_open())
  {
    while (std::getline(roomfile, line))
    {
      if (line.find("BACKGROUND") == 0)
      {

        SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Room: %s", line.c_str());

        size_t pos = line.find(" ");
        std::string call = line.substr(pos + 1);
        try
        {
          background = RF.animations.getResource(line.substr(pos + 1));
        }
        catch (const std::runtime_error &e)
        {
          SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
        }
      }
      if (line.find("FOREGROUND") == 0)
      {

        SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Room: %s", line.c_str());

        size_t pos = line.find(" ");
        try
        {
          foreground = RF.texture.getResource(line.substr(pos + 1));
        }
        catch (const std::runtime_error &e)
        {
          SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
        }
      }
      if (line.find("MUSIC") == 0)
      {

        SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Room: %s", line.c_str());

        size_t pos = line.find(" ");
        try
        {
          bgmusic = RF.musics.getResource(line.substr(pos + 1));
        }
        catch (const std::runtime_error &e)
        {
          SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
        }
      }
      if (line.find("TILES") == 0)
      {
        std::string tiles;
        if (std::getline(roomfile, tiles))
        {
          while (!(tiles.find("TILES_END") == 0))
          {
            tiles.erase(remove_if(tiles.begin(), tiles.end(), isspace), tiles.end());
            size_t pos = tiles.find("=");

            SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Room: %s", tiles.substr(pos + 1).c_str());
            std::string tilename = tiles.substr(pos + 1);
            if (tiles[0] != '.')
            {
              try
              {
                tilemap->insert(std::pair<char, Sprite::sPtr>(
                    tiles[0], RF.sprites.getResource(
                                  tilename.c_str())));
              }
              catch (const std::runtime_error &e)
              {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
              }
            }

            if (!std::getline(roomfile, tiles))
            {
              break;
            }
          }
        }
      }
      if (line.find("LAYOUT") == 0)
      {
        std::string layout;
        if (std::getline(roomfile, layout))
        {
          while (!(layout.find("LAYOUT_END") == 0))
          {
            structure->push_back(layout);

            SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Room: %s", layout.c_str());
            if (layout.length() > maxlevel)
            {
              maxlevel = layout.length();
            }

            if (!std::getline(roomfile, layout))
            {
              break;
            }
          }
        }
      }
      if (line.find("MONSTERS") == 0)
      {
        std::string entity;
        if (std::getline(roomfile, entity))
        {
          while (!(entity.find("MONSTERS_END") == 0))
          {
            std::stringstream ent(entity);
            Point monsterPos = parsePosition(ent);
            std::string monType;
            std::string switchName;
            std::getline(ent, monType, ';');
            parseSwitch(monType, switchName);
            std::string itemType;
            try
            {
              if (std::getline(ent, itemType, ';'))
              {
                monsters.emplace_back(monsterPos, RF.monsterTypes.getResource(monType), switchName, itemType);
              }
              else
              {
                monsters.emplace_back(monsterPos, RF.monsterTypes.getResource(monType), switchName);
              }
            }
            catch (const std::runtime_error &e)
            {
              SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
            }

            std::getline(roomfile, entity);
          }
        }
      }
      else if (line.find("OBJECTS") == 0)
      {
        std::string object;
        if (std::getline(roomfile, object))
        {
          while (!(object.find("OBJECTS_END") == 0))
          {
            std::stringstream ent(object);
            Point objPos = parsePosition(ent);
            std::string objType;
            std::getline(ent, objType, ';');
            std::string switchName;
            parseSwitch(objType, switchName);
            std::string special;
            std::getline(ent, special);

            try
            {
              objects.push_back(std::make_unique<Object>(RF.objectTypes.getResource(objType), objPos, switchName, special));
            }
            catch (const std::runtime_error &e)
            {
              SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s in file %s", e.what(), filename.c_str());
            }

            std::getline(roomfile, object);
          }
        }
      }
    }
    roomfile.close();
  }
  else
  {
    throw std::runtime_error("File \"" + filename + "\" not found!");
  }

  return std::make_shared<Room>(filename, structure, tilemap, background, foreground, bgmusic, maxlevel, monsters, objects);
}

Point Room::parsePosition(std::stringstream &ent)
{
  int pixelMultiplicator = 1;
  std::string part;
  std::getline(ent, part, ';');
  if (part == "t")
  {
    pixelMultiplicator = TILESIZE;
  }
  std::getline(ent, part, ';');
  float posX = std::stof(part);
  std::getline(ent, part, ';');
  float posY = std::stof(part);
  return Point(posX * pixelMultiplicator, posY * pixelMultiplicator);
}

void Room::parseSwitch(std::string &type, std::string &mSwitch)
{
  std::regex switchPattern("\\(.*\\)"); //find text in paranthesis
  std::smatch res;
  if (std::regex_search(type, res, switchPattern))
  //if(false)
  {
    mSwitch = res.str();
    type = res.suffix();
    mSwitch.erase(0, 1);
    mSwitch.erase(mSwitch.size() - 1, 1);
  }
}

bool Room::areObjectsBellowAbove(const Rectangle &character, const VectorD &vel)
{
  Point position = character.getBase();
  for (int i = position.getPosX() / TILESIZE; i <= (position.getPosX() + character.getWidth()) / TILESIZE; i++)
  {
    const char structureBelow = structure->at((position.getPosY() + character.getHeight()) / TILESIZE).at(i);
    const char structureAbove = structure->at((position.getPosY()) / TILESIZE).at(i);
    if ((vel.getPosY() >= 0.0 && (structureBelow != '.')) ||
        (vel.getPosY() < 0.0 && (structureAbove != '.')))
      return true;
  }
  return false;
}

bool Room::areObjectsLeftRight(const Rectangle &character, const VectorD &vel)
{
  Point position = character.getBase();
  for (int i = (position.getPosY()) / TILESIZE; i <= (position.getPosY() - 1 + character.getHeight()) / TILESIZE; i++)
  {
    const char structureLeft = structure->at(i).at(position.getPosX() / TILESIZE);
    const char structureRight = structure->at(i).at((position.getPosX() + character.getWidth()) / TILESIZE);

    if (!(structureLeft == 'f' && structureRight == 'f') &&
        ((vel.getPosX() < 0.0 && (structureLeft != '.')) ||
         (vel.getPosX() > 0.0 && (structureRight != '.'))))
    {
      return true;
    }
  }
  return false;
}

void Room::screenShake(int intensity)
{
  ingame->screenShake(intensity);
}

void Room::removeObsoleteObjects()
{

  for (auto monster : toRemoveMonsters)
  {
    removeMonster(monster);
  }
  for (auto attack : toRemoveAttacks)
  {
    removeAttack(attack);
  }
  for (auto item : toRemoveItems)
    removeItem(item);

  toRemoveMonsters.clear();
  toRemoveAttacks.clear();
  toRemoveItems.clear();
  tempWalls.clear();
}

void Room::addRemove(Monster *monster)
{
  toRemoveMonsters.push_front(monster);
}

void Room::addRemove(Attack *attack)
{
  toRemoveAttacks.push_front(attack);
}

void Room::addRemove(Object *)
{
  //needed?
}

void Room::addRemove(Item *item)
{
  toRemoveItems.push_back(item);
}

std::list<Object *> Room::getDoors()
{
  std::list<Object *> retlist;
  for (auto &obj : objects)
  {
    std::string objstring = obj->getEntityString();
    if (objstring.find("door") != objstring.npos)
    {
      //std::cout << objstring << "\n";
      retlist.push_back(obj.get());
    }
  }
  return retlist;
}

void Room::flipGravity()
{
  if (gravity < 0)
  {
    gravity = 1.0;
  }
  else
  {
    gravity = -1.0;
  }
}

std::list<Object *> Room::getSaveStatues()
{
  std::list<Object *> retlist;
  for (auto &obj : objects)
  {
    std::string objstring = obj->getEntityString();
    if (objstring.find("saveStatue") != objstring.npos)
    {
      //std::cout << objstring << "\n";
      retlist.push_back(obj.get());
    }
  }
  return retlist;
}
