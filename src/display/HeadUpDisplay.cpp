#include "HeadUpDisplay.hpp"

#include "Renderer.hpp"
#include "Player.hpp"
#include "Color.hpp"
#include "Text.hpp"
#include "ResourceFacade.hpp"

const Point barOffset(26, 3);
constexpr int barHeight = 10;

HeadUpDisplay::HeadUpDisplay(Renderer &ren_, const Player *player_) : ren(&ren_), player(player_), uiFont(RF.fontsPath + "pauseFont.ttf", 10),
                                                                      textHP(Text("HP").toTexture(*RF.getRenderer(), uiFont, Color::Red())),
                                                                      textEXP(Text("EXP").toTexture(*RF.getRenderer(), uiFont, Color::White())),
                                                                      textATT(Text("ATT").toTexture(*RF.getRenderer(), uiFont, Color::Green())),
                                                                      textbox(RF.sprites.getResource("textbox.txt"))
{
}

void HeadUpDisplay::render()
{

  textHP.render(Point(3, 3));
  textEXP.render(Point(3, 16));
  textATT.render(Point(3, 29));

  auto health = player->getHealth();

  Point offset = barOffset;

  ren->setDrawColor(Color(255, 0, 0, 150));
  ren->draw(Rectangle(offset, health.get() + 1, barHeight), true);
  ren->setDrawColor(Color(255, 255, 255, 150));
  ren->draw(Rectangle(offset, health.getMax() + 1, barHeight));
  ren->setDrawColor(Color::Black());

  offset.addPosY(barHeight + 3);

  auto experience = player->getExp();

  ren->setDrawColor(Color(255, 255, 255, 150));
  ren->draw(Rectangle(offset, 100 * experience.get() / experience.getMax() + 1, barHeight), true);
  ren->setDrawColor(Color(255, 255, 255, 150));
  ren->draw(Rectangle(offset, 100 + 1, barHeight));
  ren->setDrawColor(Color::Black());

  offset.addPosY(barHeight + 3);

  auto superAttack = player->getSuperCharge();

  ren->setDrawColor(Color::Green());
  ren->draw(Rectangle(offset, superAttack.get() + 1, barHeight), true);
  ren->setDrawColor(Color(255, 255, 255, 150));
  ren->draw(Rectangle(offset, 100 + 1, barHeight));
  ren->setDrawColor(Color::Black());

  if (someText.get() != nullptr)
  {
    Point textPos(20, ren->getPixelHeight() - textbox->getActualSize().getHeight());
    textbox->render(textPos);
    someText->render(textPos + Point(20, 30));
    if (textTimer.areTicksPassed(timeToPass))
    {
      someText.reset();
    }
  }
}

void HeadUpDisplay::displayText(const std::string &text, int timeMS)
{
  Texture::sPtr a(new Texture(Text(text).toTexture(*RF.getRenderer(), RF.fontText, Color::Black())));
  someText.swap(a);
  timeToPass = timeMS;
  textTimer.start();
}
