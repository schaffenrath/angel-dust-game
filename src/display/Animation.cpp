#include "Animation.hpp"
#include "ResourceFacade.hpp"
#include <fstream>
#include <memory>

Animation::Animation() : TimeDependend(), Renderable(), sprites(), times()
{
  actualSprite = sprites.end();
  actualTime = times.end();
}

Animation::Animation(const Animation &copy) : TimeDependend(), Renderable(), sprites(copy.sprites), times(copy.times)
{
  actualSprite = sprites.begin();
  actualTime = times.begin();
}

Animation::Animation(std::vector<Sprite::sPtr> &sprites_, std::vector<double> &times_) : TimeDependend(), Renderable(), sprites(std::move(sprites_)), times(std::move(times_))
{
  actualSprite = sprites.begin();
  actualTime = times.begin();
}

void Animation::step(double timeStepMS)
{
  counter += timeStepMS;
  if (counter >= *actualTime)
  {
    counter -= *actualTime;
    actualTime++;
    actualSprite++;
    if (actualTime == times.end())
    {
      actualTime = times.begin();
      actualSprite = sprites.begin();
    }
  }
}

void Animation::render(const Point &point, const SDL_RendererFlip flip)
{
  (*actualSprite)->render(point, flip);
}

void Animation::init()
{
  actualSprite = sprites.begin();
  actualTime = times.begin();
  counter = 0;
}

void Animation::push(Sprite::sPtr sprite, double time)
{
  sprites.push_back(std::move(sprite));
  times.push_back(time);
  actualSprite = sprites.begin();
  actualTime = times.begin();
}

double Animation::getAnimationTime()
{
  double totaltime = 0;
  for (auto time : times)
  {
    totaltime += time;
  }
  return totaltime;
}

Animation::sPtr Animation::load(Renderer &, std::string filename)
{
  std::ifstream animationfile;
  std::string line;
  std::shared_ptr<Animation> animation = std::make_shared<Animation>();

  animationfile.open(filename);
  if (animationfile.is_open())
  {
    while (std::getline(animationfile, line))
    {
      size_t pos = line.find(" ");
      try
      {
        animation->push(RF.sprites.getResource(line.substr(0, pos).c_str()), std::stod(line.substr(pos + 1).c_str()));
      }
      catch (const std::runtime_error &e)
      {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s in file \"%s\"", e.what(), filename.c_str());
      }
    }
    animationfile.close();
  }
  else
  {
    throw std::runtime_error("File \"" + filename + "\" not found!");
  }
  if(animation->sprites.size() == 0){
    throw std::runtime_error("File \"" + filename + "\" has errors!");
  }

  return animation;
}

const Rectangle Animation::getActualSize() const
{
  return (*actualSprite)->getActualSize();
}

Animation &Animation::operator=(const Animation &other)
{
  sprites = other.sprites;
  times = other.times;
  actualSprite = sprites.begin();
  actualTime = times.begin();
  counter = 0;
  return *this;
}
