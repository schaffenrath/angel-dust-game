#include "Blinker.hpp"

Blinker::Blinker(double blinkTime_) : restTime(blinkTime_), blinkTime(blinkTime_){}

void Blinker::step(double timeStepMS){
  restTime -= timeStepMS;
  if(restTime < 0.0){
    blink = !blink;
    restTime += blinkTime;
  }
}

bool Blinker::getState() const{
  return !isOn || blink;
}