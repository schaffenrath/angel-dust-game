#include <iostream>
#include <string>
#include "Game.hpp"


int main(int argc, char **argv)
{

  bool fullscreen = true;

  if (argc == 2){
    std::string argv1 = argv[1];
    if (argv1 == "-info"){
      SDL_LogSetAllPriority(SDL_LOG_PRIORITY_INFO);
    }
    else if (argv1 == "-warn"){
      SDL_LogSetAllPriority(SDL_LOG_PRIORITY_WARN);
    }
    else if (argv1 == "-h" || argv1 == "-help"){
      std::cout << "use -info or -warn to set error level output"  << "\n";
      return EXIT_SUCCESS;
    }
    else {
      SDL_LogSetAllPriority(SDL_LOG_PRIORITY_ERROR);
    }      
  } else {
    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_ERROR);  //SET to desired OUTPUT: INFO, WARN, ERROR
  }
	Game game;

	game.init(fullscreen);

	game.gameLoop();

/* 	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		logSDLError(std::cout, "SDL_Init");
		return 1;
	}
	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
	{
		logSDLError(std::cout, "IMG_Init");
		SDL_Quit();
		return 1;
	}
	if (TTF_Init() != 0)
	{
		logSDLError(std::cout, "TTF_Init");
		SDL_Quit();
		return 1;
	}

	Window win("Lesson 6", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

	Renderer ren(&win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	const std::string resPath = ResourceFacade::getResourcePath();
	Texture background(&ren, resPath + "background.png");
	Texture image(&ren, resPath + "image.png");

	//iW and iH are the clip width and height
	//We'll be drawing only clips so get a center position for the w/h of a clip
	int iW = 100, iH = 100;
	int x = SCREEN_WIDTH / 2 - iW / 2;
	int y = SCREEN_HEIGHT / 2 - iH / 2;

	//Setup the clips for our image
	SDL_Rect clips[4];
	for (int i = 0; i < 4; ++i)
	{
		clips[i].x = i / 2 * iW;
		clips[i].y = i % 2 * iH;
		clips[i].w = iW;
		clips[i].h = iH;
	}
	//Specify a default clip to start with
	int useClip = 0;

	SDL_Event e;
	bool quit = false;

	Animation anim = Animation::createTest(&ren, resPath);

	Room::sPtr room = Room::test(&ren);
	Camera cam(&win);
	cam.setRoom(room);

	Texture textTexture = Text("Farewell cruel Christmas").toTexture(ren, Font(resPath + "sample.ttf", 64), {255,255,255,255} );
	Ticker tck;
	//A sleepy rendering loop, wait for 3 seconds and render and present the screen each time
	while (!quit)
	{
		tck.start();
		//e is an SDL_Event variable we've declared before entering the main loop
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
				quit = true;
			//Use number input to select which clip should be drawn
			if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_1:
					cam.move(-32,0);
					break;
				case SDLK_2:
					cam.move(32,0);
					break;
				case SDLK_3:
					cam.move(0,32);
					break;
				case SDLK_4:
					cam.move(0,-32);
					break;
				case SDLK_ESCAPE:
					quit = true;
					break;
				default:
					break;
				}
			}
		}

		ren.clear();

		//Determine how many tiles we'll need to fill the screen
		int xTiles = SCREEN_WIDTH / TILE_SIZE;
		int yTiles = SCREEN_HEIGHT / TILE_SIZE;

		//Draw the tiles by calculating their positions
		for (int i = 0; i < xTiles * yTiles; ++i)
		{
			int x = i % xTiles;
			int y = i / xTiles;
			//background.render(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			//renderTexture(background, renderer, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
		}
		cam.render();
		anim.show(1000/60, Point(0,0));
		textTexture.render(Point(SCREEN_WIDTH / 4,SCREEN_HEIGHT - 100));
		//image.render(x, y, clips + useClip);
		//textTexture.render(x, y);

		tck.delay(1000/60);	

		ren.present();
	} */

	SDL_Quit();
	return 0;
}
