#include "GameStateEnd.hpp"

#include <memory>
#include <iostream>

#include "EventKey.hpp"
#include "ResourceFacade.hpp"
#include "Text.hpp"

GameStateEnd::GameStateEnd(Game *game) : GameState(game)
{
  steadyText.start(3);
  displayText = finalText;
}

void GameStateEnd::pollEvents(std::queue<Event::uPtr> &, double) {}

void GameStateEnd::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Function:
      evt->handle(timeStep);
      break;
    case EventType::Key:
      const EventKey *key = static_cast<EventKey *>(evt);
      if (key->getInfo().type != SDL_KEYDOWN && key->getInfo().type != SDL_CONTROLLERBUTTONDOWN)
        break;
      if (key->getInfo().type == SDL_KEYDOWN)
      {
        switch (key->getInfo().key.keysym.sym)
        {
        case SDLK_RETURN:
          enter();
          break;
        case SDLK_ESCAPE:
          game->doQuit(timeStep);
        }
        break;
      }
      if (key->getInfo().type == SDL_CONTROLLERBUTTONDOWN)
      {
        switch (key->getInfo().cbutton.button)
        {
        case SDL_CONTROLLER_BUTTON_A:
          enter();
          break;
        default:
          break;
        }
      }
    };
    events.pop();
  }
}

void GameStateEnd::render()
{
  steadyText.step(1.0 / 60.0);
  if (steadyText.isDone())
  {
    displayText.pop_front();
    if (displayText.empty())
      enter();
    steadyText.start(3);
  }
  if (!displayText.empty())
  {
    Texture thankText = Texture(Text(displayText.front()).toTexture(*RF.getRenderer(), RF.fontMenu, Color::White()));
    thankText.render(Point(game->getRenderer().getPixelWidth() / 2 - thankText.getSize().getWidth() / 2, game->getRenderer().getPixelHeight() / 2));
  }
}

void GameStateEnd::enter()
{
  game->transitionToMainMenu();
}