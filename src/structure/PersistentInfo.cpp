#include "PersistentInfo.hpp"

#include <fstream>
#include <string>
#include <SDL.h>
#include <iostream>

PersistentInfo::PersistentInfo() : values() {}

PersistentInfo::PersistentInfo(std::map<std::string, PersistentValue> &values_) : values(std::move(values_)) {}

PersistentInfo::uPtr PersistentInfo::loadFromFile(const std::string &filename)
{
  std::ifstream saveFile;
  std::map<std::string, PersistentValue> values;

  saveFile.open(filename);
  if (saveFile.is_open())
  {
    std::string line;
    while (std::getline(saveFile, line))
    {
      size_t pos = line.find(": ");
      if (pos == std::string::npos)
        continue;
      values.insert(std::make_pair(line.substr(0, pos), PersistentValue(line.substr(pos + 2))));
    }
  }
  else
  {
    throw std::runtime_error("Could not open file: " + filename);
  }

  return std::make_unique<PersistentInfo>(PersistentInfo(values));
}

PersistentValue &PersistentInfo::operator[](const std::string &key)
{
  return values[key];
}

const PersistentValue &PersistentInfo::operator[](const std::string &key) const
{
  if (values.count(key) != 0)
    return values.at(key);
  return empty;
}

const std::map<std::string, PersistentValue> &PersistentInfo::getValues() const
{
  return values;
}

std::ostream &operator<<(std::ostream &stream, const PersistentInfo &info)
{
  for (const auto &pair : info.getValues())
  {
    stream << pair.first << ": " << pair.second.get() << '\n';
  }
  return stream;
}

/*std::string PersistentInfo::getValueString(const std::string &key) const
{
  return values.at(key);
}

bool PersistentInfo::getValueB(const std::string &key) const
{
  return std::stoi(values.at(key));
}

int PersistentInfo::getValueI(const std::string &key) const
{
  return std::stoi(values.at(key));
}

float PersistentInfo::getValueF(const std::string &key) const
{
  return std::stof(values.at(key));
}

double PersistentInfo::getValueD(const std::string &key) const
{
  return std::stod(values.at(key));
} */