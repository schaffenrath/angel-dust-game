#include "GameStatePauseMenu.hpp"

#include <memory>
#include <iostream>
#include <string>

#include "EventKey.hpp"
#include "ResourceFacade.hpp"
#include "Text.hpp"
#include "Value.hpp"

GameStatePauseMenu::GameStatePauseMenu(Game *game) : GameState(game), entries()
{
  entries.push_back(MenuEntry("Continue", std::bind(&GameStatePauseMenu::continueGame, this)));
  entries.push_back(MenuEntry("Inventory", std::bind(&GameStatePauseMenu::openInventory, this)));
  entries.push_back(MenuEntry("Map", std::bind(&GameStatePauseMenu::openMap, this)));
  entries.push_back(MenuEntry("Main Menu", std::bind(&GameStatePauseMenu::gotoMainMenu, this)));
  actualEntry = entries.begin();
}

void GameStatePauseMenu::pollEvents(std::queue<Event::uPtr> &events, double timeStep) {}

void GameStatePauseMenu::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Function:
      evt->handle(timeStep);
      break;
    case EventType::Key:
      const EventKey *key = static_cast<EventKey *>(evt);
      if (key->getInfo().type != SDL_KEYDOWN && key->getInfo().type != SDL_CONTROLLERBUTTONDOWN)
        break;
      if (key->getInfo().type == SDL_KEYDOWN){
        switch (key->getInfo().key.keysym.sym) {
        case SDLK_UP:
          up();
          break;
        case SDLK_DOWN:
          down();
          break;
        case SDLK_RETURN:
          enter();
          break;
        case SDLK_ESCAPE:
          game->transitionContinue();
          break;
        }
      }
      if (key->getInfo().type == SDL_CONTROLLERBUTTONDOWN) {
        switch (key->getInfo().cbutton.button)
        {
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
          up();
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
          down();
          break;
        case SDL_CONTROLLER_BUTTON_A:
          enter();
          break;
        case SDL_CONTROLLER_BUTTON_B:
          game->transitionContinue();
          break;
        default:
          break;
        }
      }
      
      break;
    };
    events.pop();
  }
}

void GameStatePauseMenu::render()
{
  std::list<std::string> text{
      "Robert",
      "HP: " + std::to_string(player->getHealth().get()),
      "EXP: " + std::to_string(player->getExp().get()),
      "Dmg: " + std::to_string(player->getAttackDamage()),
      "RDmg: " + std::to_string(player->getRangeDamage())
  };

  game->getRenderer().setDrawColor(Color::Gray());
  game->getRenderer().draw(Rectangle(0, 0, 768, 320), true);

  int statOffset = 0;
  while (!text.empty())
  {
    Texture(Text(text.front()).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(Point(20, 20 + statOffset));
    text.pop_front();
    statOffset += 20;
  }
  int offset = game->getRenderer().getPixelHeight() / 2;
  for (auto &ent : entries)
  {
    ent.render(game->getRenderer().getPixelWidth(), offset);
    Rectangle r = ent.getTexture()->getSize();
    r.move(ent.getTexture()->getCenterPosX(game->getRenderer().getPixelWidth()), offset);
    if (&(*actualEntry) == &ent)
    {
      game->getRenderer().setDrawColor(Color::White());
      game->getRenderer().draw(r);
      game->getRenderer().setDrawColor(Color::Gray());
    }
    ent.adjustOffsetByHeight(offset);
  }
}

void GameStatePauseMenu::up()
{
  if (actualEntry == entries.begin())
  {
    actualEntry = entries.end();
  }
  actualEntry--;
}

void GameStatePauseMenu::down()
{
  actualEntry++;
  if (actualEntry == entries.end())
  {
    actualEntry = entries.begin();
  }
}

void GameStatePauseMenu::enter()
{
  actualEntry->run();
}

void GameStatePauseMenu::continueGame()
{
  game->transitionContinue();
}

void GameStatePauseMenu::openInventory()
{
  game->transitionToInventory();
}

void GameStatePauseMenu::openMap()
{
  game->transitionToMap();
}

void GameStatePauseMenu::restart()
{
  game->transitionNewGame();
}

void GameStatePauseMenu::gotoMainMenu()
{
  game->transitionToMainMenu();
}
