#include "MenuEntry.hpp"

#include "ResourceFacade.hpp"
#include "Text.hpp"

MenuEntry::MenuEntry(const std::string text_, std::function<void()> callback) : callback(callback)
{
  textTexture = std::make_unique<Texture>(Text(text_).toTexture(*RF.getRenderer(), RF.fontMenu, Color::White()));
}

void MenuEntry::run()
{
  callback();
}

void MenuEntry::render(const int width, const int offset)
{
  textTexture->render(Point(textTexture->getCenterPosX(width), offset));
}

void MenuEntry::adjustOffsetByHeight(int &offset)
{
  offset += textTexture->getSize().getHeight();
}

Texture *MenuEntry::getTexture()
{
  return textTexture.get();
}
