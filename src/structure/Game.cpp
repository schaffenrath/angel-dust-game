#include "Game.hpp"
#include "ResourceFacade.hpp"
#include "GameStateInGame.hpp"
#include "GameStateMainMenu.hpp"
#include "GameStatePauseMenu.hpp"
#include "GameStateInventory.hpp"
#include "GameStateDeathMenu.hpp"
#include "GameStateEnd.hpp"
#include "GameStateMap.hpp"
#include "EventKey.hpp"
#include "EventFunction.hpp"

Game::Game() : transition(Transition::None)
{
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS | SDL_INIT_AUDIO | SDL_INIT_GAMECONTROLLER | SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC) != 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "SDL_Init Error: %s", SDL_GetError());
  }
  if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
  {
    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "IMG_Init Error: %s", SDL_GetError());
  }
  if (TTF_Init() != 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "TTF_Init Error: %s", SDL_GetError());
  }

  /* print the audio driver we are using */
  SDL_LogInfo(SDL_LOG_CATEGORY_AUDIO, "[SDL] Audio driver: %s\n", SDL_GetCurrentAudioDriver());
  int i, count = SDL_GetNumAudioDevices(0);

  for (i = 0; i < count; ++i)
  {
    SDL_LogInfo(SDL_LOG_CATEGORY_AUDIO, "Audio device %d: %s", i, SDL_GetAudioDeviceName(i, 0));
  }

  // open 44.1KHz, signed 16bit, system byte order,
  //      stereo audio, using 1024 byte chunks
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
  {
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO, "Mix_OpenAudio: %s\n", Mix_GetError());
    exit(2);
  }
  int channels = Mix_AllocateChannels(16);
  SDL_LogInfo(SDL_LOG_CATEGORY_AUDIO, "channels= %d", channels);

  Mix_Init(MIX_INIT_MP3);

  /* Open the first available controller. */
  SDL_GameController *controller = NULL;
  for (int i = 0; i < SDL_NumJoysticks(); ++i)
  {
    if (SDL_IsGameController(i))
    {
      controller = SDL_GameControllerOpen(i);
      if (controller)
      {
        SDL_LogInfo(SDL_LOG_CATEGORY_INPUT, "found gamecontroller %i \n", i);
        haptic = SDL_HapticOpen(0);
        break;
      }
      else
      {
        SDL_LogError(SDL_LOG_CATEGORY_INPUT, "Could not open gamecontroller %i: %s\n", i, SDL_GetError());
      }
    }
  }
}

Game::~Game()
{
  SDL_Quit();
}

void Game::init(bool fullscreen)
{
  const int width = (int)(Window::getCurrentDisplayRatio() * 768);

  int flags = 0;
  if(fullscreen) flags = flags | SDL_WINDOW_FULLSCREEN_DESKTOP;

  win = std::make_shared<Window>("OurGame", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, 768, flags);
  ren = std::make_shared<Renderer>(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  ren->setLogicalSize((int)(Window::getCurrentDisplayRatio() * 320), 320);

  RF.init(ren);

  mainMenu = std::make_unique<GameStateMainMenu>(this);
  pauseMenu = std::make_unique<GameStatePauseMenu>(this);

  actualState = mainMenu.get();
}

void Game::gameLoop()
{
  while (!quit)
    gameLoopStep();
}

void Game::gameLoopStep()
{
  //bool quit = false;

  std::queue<Event::uPtr> events;

  ticker.start();
  //handle events
  handleEvent(events);
  actualState->pollEvents(events, dt);
  actualState->handleEvents(events, dt);

  //render everything
  ren->clear();
  actualState->render();

  //wait for framerate
  ticker.delay((int)dt);

  ren->present();

  actualState = checkStateTransition();
  //return quit;
}

GameState *Game::checkStateTransition()
{
  Transition temp = transition;
  transition = Transition::None;
  std::unique_ptr<GameStateInGame> inGameTmpPtr;

  switch (temp)
  {
  case Transition::NewGame:
    inGame.reset(nullptr);
    inGameTmpPtr = std::make_unique<GameStateInGame>(this, PersistentInfo::loadFromFile(RF.getResourcePath() + "start.txt"));
    /* player = inGameTmpPtr->getPlayer();
    player->setVelocity(VectorD(0.0, 0.0)); */
    inGame = std::move(inGameTmpPtr);
    return inGame.get();

  case Transition::LoadGame:
    try
    {

      inGameTmpPtr = std::make_unique<GameStateInGame>(this, PersistentInfo::loadFromFile(loadFile));
      //player = inGameTmpPtr->getPlayer();
      inGame = std::move(inGameTmpPtr);
      return inGame.get();
    }
    catch (std::runtime_error &e)
    {
      SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "%s", e.what());
    }
    break;

  case Transition::Pause:
    dt = 1000.0 / 600.0;
    break;

  case Transition::Unpause:
    dt = 1000.0 / 60.0;
    break;

  case Transition::ToDeath:
    deathMenu = std::make_unique<GameStateDeathMenu>(this, static_cast<GameStateInGame *>(inGame.get())->getRoom(), static_cast<GameStateInGame *>(inGame.get())->getCamera()->getPosition());
    return deathMenu.get();

  case Transition::ToMainMenu:
    dt = 1000.0 / 60.0;
    static_cast<GameStateInGame *>(inGame.get())->getRoom()->setMusicVolume(MIX_MAX_VOLUME);
    static_cast<GameStateInGame *>(inGame.get())->getRoom()->stopMusic();
    inGame.reset(nullptr);
    return mainMenu.get();

  case Transition::ToPauseMenu:
    pauseMenu = std::make_unique<GameStatePauseMenu>(this);
    static_cast<GameStateInGame *>(inGame.get())->getRoom()->setMusicVolume(30);
    //dt = 0.0;
    //reset velocities so they don't add up
    //player->setVelocity(VectorD(0.0, 0.0));
    static_cast<GameStatePauseMenu *>(pauseMenu.get())->setPlayer(static_cast<GameStateInGame *>(inGame.get())->getPlayer());
    //pauseMenuTmpPtr->setPlayer(player);
    return pauseMenu.get();

  case Transition::Inventory:
    inventory = std::make_unique<GameStateInventory>(this, static_cast<GameStateInGame *>(inGame.get())->getPlayer());
    return inventory.get();

  case Transition::Map:
    map = std::make_unique<GameStateMap>(this, static_cast<GameStateInGame *>(inGame.get())->getPlayer(), static_cast<GameStateInGame *>(inGame.get())->getRoom());
    return map.get();

  case Transition::Continue:
    //dt = 1000.0 / 60;
    //player->setVelocity(VectorD(0.0, 0.0));
    static_cast<GameStateInGame *>(inGame.get())->getRoom()->setMusicVolume(MIX_MAX_VOLUME);
    return inGame.get();

    /*   case Transition::changeLevel:
    inGame.reset(nullptr);
    inGame = std::make_unique<GameStateInGame>(this, PersistentInfo::loadFromFile(RF.getResourcePath() + "lvlchange.txt"));
    return inGame.get(); */

  case Transition::End:
    end = std::make_unique<GameStateEnd>(this);
    return end.get();

  case Transition::None:
    break;
  }

  return actualState;
}

void Game::handleEvent(std::queue<Event::uPtr> &events)
{
  SDL_Event e;
  while (SDL_PollEvent(&e))
  {
    if (e.type == SDL_QUIT)
    {
      events.push(std::make_unique<EventFunction>(std::bind(&Game::doQuit, this, std::placeholders::_1)));
    }
    if ((e.type == SDL_KEYDOWN && e.key.repeat == 0) || e.type == SDL_KEYUP || e.type == SDL_CONTROLLERBUTTONDOWN || e.type == SDL_CONTROLLERBUTTONUP)
    {
      events.push(std::make_unique<EventKey>(e));
    }
  }
}

void Game::doQuit(double)
{
  quit = true;
}

void Game::transitionNewGame()
{
  transition = Transition::NewGame;
}

void Game::transitionToMainMenu()
{
  transition = Transition::ToMainMenu;
}

void Game::transitionToPauseMenu()
{
  transition = Transition::ToPauseMenu;
}

void Game::transitionToInventory()
{
  transition = Transition::Inventory;
}

void Game::transitionToMap()
{
  transition = Transition::Map;
}

void Game::transitionContinue()
{
  transition = Transition::Continue;
}

void Game::transitionToPause()
{
  transition = Transition::Pause;
}

void Game::transitionToUnpause()
{
  transition = Transition::Unpause;
}

void Game::transitionToDeath()
{
  transition = Transition::ToDeath;
}

void Game::transitionToEnd()
{
  transition = Transition::End;
}
/* void Game::transitionToChangeLevel()
{
  transition = Transition::changeLevel;
} */

void Game::transitionLoadGame(const std::string &loadFile_)
{
  loadFile = loadFile_;
  transition = Transition::LoadGame;
}

Window &Game::getWindow()
{
  return *win;
}

Renderer &Game::getRenderer()
{
  return *ren;
}

SDL_Haptic *Game::getHaptic()
{
  return haptic;
}
