#include "Switch.hpp"

#include "PersistentInfo.hpp"
#include "PersistentValue.hpp"

Switch::Switch(const std::string &name_, PersistentInfo *info_) : name(name_)
{
  myInfo = &(*info_)[name];
  if(myInfo->get().empty()){
    *myInfo = false;
  }
}

bool Switch::isOn() const
{
  return myInfo->getBool();
}

void Switch::setOn()
{
  *myInfo = (true);
}

void Switch::setOff()
{
  *myInfo = false;
}