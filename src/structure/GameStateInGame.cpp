#include "GameStateInGame.hpp"
#include "ResourceFacade.hpp"
#include <fstream>

GameStateInGame::GameStateInGame(Game *game, PersistentInfo::uPtr &&gameData_) : GameState(game), camera(game->getRenderer()), persistenceData(std::move(gameData_))
{
  player = std::make_shared<Player>(this, Point((*persistenceData)["Player/X"].getInt(), (*persistenceData)["Player/Y"].getInt()), *persistenceData.get());
  auto room = RF.rooms.getResource((*persistenceData)["LastRoom"].get());

  room->setPlayer(player);
  room->setIngame(this);
  camera.setRoom(room);
  hud = std::make_unique<HeadUpDisplay>(game->getRenderer(), player.get());
}

void GameStateInGame::pollEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  if (!newLevel.empty())
  {
    changeLevel(lvlChangePoint, newLevel);
    newLevel.clear();
  }

  //invoke collision detection here?
  camera.pollEvents(events, timeStep);
}

void GameStateInGame::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Key:
      handleEventKey((const EventKey *)evt);
      break;
    case EventType::Function:
    case EventType::Collision:
      evt->handle(timeStep);
      break;
    };
    events.pop();
  }
  camera.getRoom()->removeObsoleteObjects();
}

bool GameStateInGame::handleEventKey(const EventKey *keyEvent)
{

  if (keyEvent->getInfo().type == SDL_KEYDOWN)
  {
    switch (keyEvent->getInfo().key.keysym.sym)
    {
    /* case SDLK_1:
      camera.move(-32, 0);
      break;
    case SDLK_2:
      camera.move(32, 0);
      break;
    case SDLK_3:
      camera.move(0, 32);
      break;
    case SDLK_4:
      camera.move(0, -32);
      break;
    case SDLK_5:
      camera.shake(5);
      break; */
    case SDLK_F1:
      if (paused == Transition::Pause)
      {
        game->transitionToUnpause();
        paused = Transition::None;
      }
      else
      {
        game->transitionToPause();
        paused = Transition::Pause;
      }
      break;
    /* case SDLK_F5:
      quicksave();
      break;
    case SDLK_F7:
      registerLevelChange(Point(100, 100), "twilight/bossRoom.txt");
      break;
    case SDLK_F8:
      registerLevelChange(Point(100, 100), "twilight/01.txt");
      game->transitionToEnd();
      break;
    case SDLK_F9:
      getRoom()->flipGravity();
      break; */
    case SDLK_ESCAPE:
      game->transitionToPauseMenu();
      break;
    case SDLK_RIGHT:
      player->startGoRight();
      break;
    case SDLK_LEFT:
      player->startGoLeft();
      break;
    case SDLK_SPACE:
      if (getRoom()->getGravity() > 0)
        player->jump(getRoom()->getGravity());
      break;
    case SDLK_LCTRL:
      player->startAttack();
      break;
    case SDLK_LSHIFT:
      player->startRangeAttack();
      break;
    case SDLK_UP:
      player->interact();
      break;
    case SDLK_DOWN:
      player->fallThrough();
      break;
    case SDLK_q:
      player->startSuperAttack();
      break;
    default:
      break;
    }
  }
  if (keyEvent->getInfo().type == SDL_KEYUP)
  {
    switch (keyEvent->getInfo().key.keysym.sym)
    {
    case SDLK_RIGHT:
      player->startGoLeft();
      break;
    case SDLK_LEFT:
      player->startGoRight();
      break;
    case SDLK_SPACE:
      if (getRoom()->getGravity() > 0)
        player->stopJump(getRoom()->getGravity());
      break;
    default:
      break;
    }
  }

  if (keyEvent->getInfo().type == SDL_CONTROLLERBUTTONDOWN)
  {
    switch (keyEvent->getInfo().cbutton.button)
    {
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
      player->startGoLeft();
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
      player->startGoRight();
      break;
    case SDL_CONTROLLER_BUTTON_A:
      if (getRoom()->getGravity() > 0)
        player->jump(getRoom()->getGravity());
      break;
    case SDL_CONTROLLER_BUTTON_START:
      game->transitionToPauseMenu();
      break;
    case SDL_CONTROLLER_BUTTON_X:
      player->startAttack();
      break;
    case SDL_CONTROLLER_BUTTON_B:
      player->startRangeAttack();
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_UP:
      player->interact();
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
      player->fallThrough();
      break;
    case SDL_CONTROLLER_BUTTON_Y:
      player->startSuperAttack();
      break;
    default:
      break;
    }
  }
  if (keyEvent->getInfo().type == SDL_CONTROLLERBUTTONUP)
  {
    switch (keyEvent->getInfo().cbutton.button)
    {
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
      player->startGoRight();
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
      player->startGoLeft();
      break;
    case SDL_CONTROLLER_BUTTON_A:
      if (getRoom()->getGravity() > 0)
        player->stopJump(getRoom()->getGravity());
    default:
      break;
    }
  }
  return false;
}

void GameStateInGame::render()
{
  camera.render();
  hud->render();
}

void GameStateInGame::screenShake(int intensity)
{
  camera.shake(intensity);
}

void GameStateInGame::registerLevelChange(Point entryPoint, const std::string &map)
{
  lvlChangePoint = entryPoint;
  newLevel = map;
}

void GameStateInGame::changeLevel(Point entryPoint, const std::string &map)
{
  //load new room
  //room.reset();
  camera.clearRoom();
  auto room = RF.rooms.getResource(map);
  //room = ResourceFacade::getI().rooms.getResource(map);
  (*persistenceData)["LastRoom"] = map;
  room->setPlayer(player);
  room->setIngame(this);
  camera.setRoom(room);
  player->moveToPosX(entryPoint.getPosX());
  player->moveToPosY(entryPoint.getPosY());
}

void GameStateInGame::quicksave()
{
  player->save(*(persistenceData.get()));
  std::ofstream saveFile{ResourceFacade::getResourcePath() + "quick.txt"};
  if (saveFile.is_open())
  {
    saveFile << *persistenceData;
  }
}

Player::sPtr GameStateInGame::getPlayer()
{
  return player;
}

Room::sPtr GameStateInGame::getRoom()
{
  return camera.getRoom();
}
