#include "GameStateMainMenu.hpp"

#include <memory>
#include <iostream>

#include "EventKey.hpp"
#include "ResourceFacade.hpp"
#include "Text.hpp"

GameStateMainMenu::GameStateMainMenu(Game *game) : GameState(game), entries()
{
  entries.push_back(MenuEntry("Start new Game", std::bind(&GameStateMainMenu::startNewGame, this)));
  entries.push_back(MenuEntry("Load Game", std::bind(&GameStateMainMenu::loadGame, this)));
  entries.push_back(MenuEntry("Quit", std::bind(&GameStateMainMenu::exitGame, this)));
  actualEntry = entries.begin();
  titlescreen = RF.texture.getResource("titlescreen.png");
}

void GameStateMainMenu::pollEvents(std::queue<Event::uPtr> &, double) {}

void GameStateMainMenu::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Function:
      evt->handle(timeStep);
      break;
    case EventType::Key:
      const EventKey *key = static_cast<EventKey *>(evt);
      if (key->getInfo().type != SDL_KEYDOWN && key->getInfo().type != SDL_CONTROLLERBUTTONDOWN)
        break;
      if (key->getInfo().type == SDL_KEYDOWN){
        switch (key->getInfo().key.keysym.sym) {
        case SDLK_UP:
          up();
          break;
        case SDLK_DOWN:
          down();
          break;
        case SDLK_RETURN:
          enter();
          break;
        case SDLK_ESCAPE:
          game->doQuit(timeStep);
        }
        break;
      }
      if (key->getInfo().type == SDL_CONTROLLERBUTTONDOWN) {
        switch (key->getInfo().cbutton.button)
        {
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
          up();
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
          down();
          break;
        case SDL_CONTROLLER_BUTTON_A:
          enter();
          break;
        default:
          break;
        }
      }
      
    };
    events.pop();
  }
}

void GameStateMainMenu::render()
{

  titlescreen->render(Rectangle(0, 0, game->getRenderer().getPixelWidth(), game->getRenderer().getPixelHeight()), titlescreen->getSize());
  int offset = game->getRenderer().getPixelHeight() / 2;
  for (auto &ent : entries)
  {
    ent.render(game->getRenderer().getPixelWidth(), offset);
    Rectangle r = ent.getTexture()->getSize();
    r.move(ent.getTexture()->getCenterPosX(game->getRenderer().getPixelWidth()), offset);
    if (&(*actualEntry) == &ent)
    {
      game->getRenderer().setDrawColor(Color::White());
      game->getRenderer().draw(r);
      game->getRenderer().setDrawColor(Color::Black());
    }
    ent.adjustOffsetByHeight(offset);
  }
}

void GameStateMainMenu::up()
{
  if (actualEntry == entries.begin())
  {
    actualEntry = entries.end();
  }
  actualEntry--;
}

void GameStateMainMenu::down()
{
  actualEntry++;
  if (actualEntry == entries.end())
  {
    actualEntry = entries.begin();
  }
}

void GameStateMainMenu::enter()
{
  actualEntry->run();
}

void GameStateMainMenu::startNewGame()
{
  game->transitionNewGame();
}

void GameStateMainMenu::loadGame()
{
  game->transitionLoadGame(ResourceFacade::getResourcePath() + "quick.txt");
}

void GameStateMainMenu::exitGame()
{
  game->doQuit(0.0);
}
