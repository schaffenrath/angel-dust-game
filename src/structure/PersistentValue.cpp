#include "PersistentValue.hpp"

std::string PersistentValue::get() const
{
  return value;
}

float PersistentValue::getFloat() const
{
  return std::stof(value);
}
bool PersistentValue::getBool() const
{
  return std::stoi(value);
}
int PersistentValue::getInt() const
{
  return std::stoi(value);
}
double PersistentValue::getDouble() const
{
  return std::stod(value);
}

PersistentValue &PersistentValue::operator=(bool b){
  if(b){
    value = "1";
  }else{
    value = "0";
  }
  return *this;
}

PersistentValue &PersistentValue::operator=(float f)
{
  value = std::to_string(f);
  return *this;
}
PersistentValue &PersistentValue::operator=(int i)
{
  value = std::to_string(i);
  return *this;
}
PersistentValue &PersistentValue::operator=(const std::string &string)
{
  value = string;
  return *this;
}
