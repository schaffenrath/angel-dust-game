#include "GameStateInventory.hpp"

#include <memory>
#include <iostream>
#include <string>

#include "EventKey.hpp"
#include "ResourceFacade.hpp"
#include "Text.hpp"
#include "Value.hpp"

GameStateInventory::GameStateInventory(Game *game, Player::sPtr player) : GameState(game), entries(), player(player)
{
  for (auto &item : player->getInventory())
  {
    entries.push_back(Entry(item.get(), std::bind(&GameStateInventory::useItem, this, item.get())));
  }
  entries.push_back(Entry("Back", std::bind(&GameStateInventory::gotoPauseMenu, this)));
  actualEntry = entries.begin();
  if (entries.size() > 1)
  {
    auto it = entries.begin();
    while (it != entries.end())
    {
      if (!(*it).isText() && (*it).getItem()->isConsumable())
      {
        actualEntry = it;
        break;
      }
      it++;
    }
  }
}

void GameStateInventory::pollEvents(std::queue<Event::uPtr> &, double) {}

void GameStateInventory::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Function:
      evt->handle(timeStep);
      break;
    case EventType::Key:
      const EventKey *key = static_cast<EventKey *>(evt);
      if (key->getInfo().type != SDL_KEYDOWN && key->getInfo().type != SDL_CONTROLLERBUTTONDOWN)
        break;
      if (key->getInfo().type == SDL_KEYDOWN){
        switch (key->getInfo().key.keysym.sym)
        {
        case SDLK_UP:
          up();
          break;
        case SDLK_DOWN:
          down();
          break;
        case SDLK_LEFT:
          left();
          break;
        case SDLK_RIGHT:
          right();
          break;
        case SDLK_RETURN:
          enter();
          break;
        case SDLK_ESCAPE:
          game->transitionToPauseMenu();
          break;
        }
      }
      if (key->getInfo().type == SDL_CONTROLLERBUTTONDOWN) {
        switch (key->getInfo().cbutton.button)
        {
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
          up();
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
          down();
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
          left();
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
          right();
          break;
        case SDL_CONTROLLER_BUTTON_A:
          enter();
          break;
        case SDL_CONTROLLER_BUTTON_B:
          game->transitionToPauseMenu();
          break;
        default:
          break;
        }
      }
      break;
    };
    events.pop();
  }
}

void GameStateInventory::render()
{
  std::list<std::string> text{
      "Robert",
      "HP: " + std::to_string(player->getHealth().get()),
      "EXP: " + std::to_string(player->getExp().get()),
  };

  game->getRenderer().setDrawColor(Color::Gray());
  game->getRenderer().draw(Rectangle(0, 0, 768, 320), true);
  Sprite::sPtr frame = RF.sprites.getResource("frame.txt");

  int statOffset = 0;
  while (!text.empty())
  {
    Texture(Text(text.front()).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(Point(20, 20 + statOffset));
    text.pop_front();
    statOffset += 20;
  }

  if (!actualEntry->isText())
  {
    Texture(Text(actualEntry->getItem()->getItemDescription()).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(Point(160, 200));
  }

  int offset = game->getRenderer().getPixelHeight() / 4 * 3;

  int consumableCounter = 0;
  for (auto &entry : entries)
    if (!entry.isText() && entry.getItem()->isConsumable())
      consumableCounter++;

  const int itemHorizontalOffset = entries.size() == 1 ? 0 : game->getRenderer().getPixelWidth() / 2 - consumableCounter * frame->getActualSize().getWidth() / 2;
  const int itemHorizontalOffset2 = entries.size() == 1 ? 0 : game->getRenderer().getPixelWidth() / 2 - (entries.size() - consumableCounter - 1) * frame->getActualSize().getWidth() / 2;
  Point itemOffset{itemHorizontalOffset, game->getRenderer().getPixelHeight() / 2};
  Point itemOffset2{itemHorizontalOffset2, game->getRenderer().getPixelHeight() / 4};

  std::string notconsumableHeading = {"Not consumable"};
  std::string consumableHeading = {"Consumable"};
  Point headingOffset2 = Point(100, 50);
  Point headingOffset = Point(100, 120);
  Texture(Text(notconsumableHeading).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(headingOffset2);
  Texture(Text(consumableHeading).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(headingOffset);

  for (auto &ent : entries)
  {
    Rectangle r;
    if (ent.isText())
    {
      ent.render(game->getRenderer().getPixelWidth(), offset);
      r = ent.getTexture()->getSize();
      r.move(ent.getTexture()->getCenterPosX(game->getRenderer().getPixelWidth()), offset);
      ent.adjustOffsetByHeight(offset);
    }
    else
    {
      if (ent.getItem() != nullptr && !ent.getItem()->isConsumable())
      {
        frame->render(itemOffset2);
        r = frame->getActualSize();
        r.move(itemOffset2.getPosX(), itemOffset2.getPosY());
        const Point centerItemOffset{itemOffset2.getPosX() + frame->getActualSize().getWidth() / 2 - ent.getItem()->getCollisionRectWorld().getWidth() / 2, itemOffset2.getPosY() + frame->getActualSize().getHeight() / 2 - ent.getItem()->getCollisionRectWorld().getHeight() / 2};
        ent.render(centerItemOffset);
        ent.adjustOffset(itemOffset2, frame);
      }
      else
      {
        frame->render(itemOffset);
        r = frame->getActualSize();
        r.move(itemOffset.getPosX(), itemOffset.getPosY());
        const Point centerItemOffset{itemOffset.getPosX() + frame->getActualSize().getWidth() / 2 - ent.getItem()->getCollisionRectWorld().getWidth() / 2, itemOffset.getPosY() + frame->getActualSize().getHeight() / 2 - ent.getItem()->getCollisionRectWorld().getHeight() / 2};
        ent.render(centerItemOffset);
        ent.adjustOffset(itemOffset, frame);
      }
    }
    if (&(*actualEntry) == &ent)
    {
      game->getRenderer().setDrawColor(Color::White());
      game->getRenderer().draw(r);
      game->getRenderer().setDrawColor(Color::Black());
    }
  }
}

void GameStateInventory::up()
{
  if (!(*actualEntry).isText() && !(*actualEntry).getItem()->isConsumable())
  {
    actualEntry = entries.end();
    actualEntry--;
  }
  else if (!(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
  {
    if (actualEntry == entries.begin())
    {
      actualEntry = entries.end();
      actualEntry--;
    }
    else
    {
      while (!(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
        actualEntry--;
    }
  }
  else if ((*actualEntry).isText() && entries.size() > 1)
    actualEntry--;
}

void GameStateInventory::down()
{
  if (!(*actualEntry).isText() && !(*actualEntry).getItem()->isConsumable())
  {
    while (!(*actualEntry).isText() && !(*actualEntry).getItem()->isConsumable())
    {
      actualEntry++;
    }
  }
  else if (!(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
  {
    actualEntry = entries.end();
    actualEntry--;
  }
  else if ((*actualEntry).isText())
    actualEntry = entries.begin();
}

void GameStateInventory::left()
{
  bool shouldBeconsumable = false, isOk = ((*actualEntry).isText()) ? true : false;
  if (!(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
    shouldBeconsumable = true;
  while (!isOk)
  {
    if (actualEntry == entries.begin())
      actualEntry = entries.end();

    actualEntry--;
    if (shouldBeconsumable && !(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
      isOk = true;
    else if (!shouldBeconsumable && !(*actualEntry).isText() && !(*actualEntry).getItem()->isConsumable())
      isOk = true;
  }
}

void GameStateInventory::right()
{
  bool shouldBeconsumable = false, isOk = ((*actualEntry).isText()) ? true : false;
  if (!(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
    shouldBeconsumable = true;
  while (!isOk)
  {
    actualEntry++;
    if (actualEntry == entries.end())
    {
      actualEntry = entries.begin();
    }
    if (shouldBeconsumable && !(*actualEntry).isText() && (*actualEntry).getItem()->isConsumable())
      isOk = true;
    else if (!shouldBeconsumable && !(*actualEntry).isText() && !(*actualEntry).getItem()->isConsumable())
      isOk = true;
  }
}

void GameStateInventory::enter()
{
  actualEntry->run();
}

void GameStateInventory::useItem(Item *item)
{
  switch (item->getItemCatagory())
  {
  case ItemCatagory::HP:
    //player->addHealth(item->getItemValue());
    player->addHealthPercent(item->getItemValue());
    player->removeItemFromInventory(item);
    entries.erase(actualEntry);
    game->transitionContinue();
    actualEntry = entries.begin();
    break;
  case ItemCatagory::XP:
    player->addExp(item->getItemValue());
    player->removeItemFromInventory(item);
    entries.erase(actualEntry);
    game->transitionContinue();
    actualEntry = entries.begin();
    break;
  default:
    break;
  }
}

void GameStateInventory::gotoPauseMenu()
{
  game->transitionToPauseMenu();
}

GameStateInventory::Entry::Entry(const std::string text_, std::function<void()> callback) : callback(callback)
{
  isEntryText = true;
  textTexture = std::make_unique<Texture>(Text(text_).toTexture(*RF.getRenderer(), RF.fontMenu, Color::White()));
}

GameStateInventory::Entry::Entry(Item *item_, std::function<void()> callback) : callback(callback)
{
  isEntryText = false;
  item = item_;
}

void GameStateInventory::Entry::run()
{
  callback();
}

void GameStateInventory::Entry::render(const Point offset)
{
  item->setPosition(Point{0, 0});
  item->render(offset);
}

void GameStateInventory::Entry::render(const int width, const int offset)
{
  textTexture->render(Point(textTexture->getCenterPosX(width), offset));
}

void GameStateInventory::Entry::adjustOffsetByHeight(int &offset)
{
  offset += textTexture->getSize().getHeight();
}

void GameStateInventory::Entry::adjustOffset(Point &offset, Sprite::sPtr frame)
{
  offset += Point(frame->getActualSize().getWidth(), 0);
}

Texture *GameStateInventory::Entry::getTexture()
{
  return textTexture.get();
}
