#include "EventCollision.hpp"

EventCollision::EventCollision(Collisionable *a_, Collisionable *b_) : Event(EventType::Collision), a(a_), b(b_) {}

void EventCollision::handle(double)
{
  a->handleCollision(b);
  b->handleCollision(a);
}
