#include "GameStateMap.hpp"

#include <memory>
#include <iostream>
#include <string>

#include "EventKey.hpp"
#include "ResourceFacade.hpp"
#include "Text.hpp"
#include "Value.hpp"

GameStateMap::GameStateMap(Game *game, Player::sPtr player_, Room::sPtr room_) : GameState(game), player(player_), room(room_)
{
  //std::cout << "entering mapscreen with " << room->getName() << "\n";
  //std::cout << "player LastRoom " << player->getActualRoom() << "\n";
  mapIterator = player->getKnownRooms().find(room->getName().substr(RF.getResourcePath("maps").length()));
  mapposj = -player->getPosition().getPosX() / 32 + 320/32; //fuzzing with screen resolution
  mapposi = -player->getPosition().getPosY() / 32 + 200/32;
}

void GameStateMap::pollEvents(std::queue<Event::uPtr> &, double) {}

void GameStateMap::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Function:
      evt->handle(timeStep);
      break;
    case EventType::Key:
      const EventKey *key = static_cast<EventKey *>(evt);
      if (key->getInfo().type != SDL_KEYDOWN && key->getInfo().type != SDL_CONTROLLERBUTTONDOWN)
        break;
      if (key->getInfo().type == SDL_KEYDOWN){
        switch (key->getInfo().key.keysym.sym)
        {
        case SDLK_ESCAPE:
          game->transitionToPauseMenu();
          break;
        case SDLK_LCTRL:
          if (player->getKnownRooms().size() > 1){
            if(++mapIterator == player->getKnownRooms().end())
              mapIterator = player->getKnownRooms().begin();
            switchRoom(*mapIterator);
          }
          break;
        case SDLK_LEFT:
          mapposj++;
          break;
        case SDLK_RIGHT:
          mapposj--;
          break;
        case SDLK_UP:
          mapposi++;
          break;
        case SDLK_DOWN:
          mapposi--;
          break;
        }
        
      }
      if (key->getInfo().type == SDL_CONTROLLERBUTTONDOWN) {
        switch (key->getInfo().cbutton.button)
        {
        case SDL_CONTROLLER_BUTTON_B:
          game->transitionToPauseMenu();
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_UP:
          mapposi++;
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
          mapposi--;
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
          mapposj++;
          break;
        case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
          mapposj--;
          break;
        case SDL_CONTROLLER_BUTTON_A:
          if (player->getKnownRooms().size() > 1){
            if(++mapIterator == player->getKnownRooms().end())
              mapIterator = player->getKnownRooms().begin();
            switchRoom(*mapIterator);
          }
          break;
        default:
          break;
        }
      }
      break;
    };
    events.pop();
  }
}

void GameStateMap::render()
{
  auto mapoffset = Point(100,100);
  double scalingfactor = 2.0;
  room->renderMap(mapoffset,mapposi,mapposj, scalingfactor);
  auto playerpos = player->getPosition();
  
  game->getRenderer().setDrawColor(Color::Gray());
  game->getRenderer().draw(Rectangle(0, 0, 768, 320), true);

  auto foundpos = room->getName().find(player->getActualRoom());
  if (foundpos != room->getName().npos) {  //player in actual room
    game->getRenderer().setDrawColor(Color::Green());
    game->getRenderer().draw(
      Rectangle(playerpos.getPosX() / scalingfactor + mapoffset.getPosX() +
                32 * mapposj / scalingfactor,
                playerpos.getPosY() / scalingfactor + mapoffset.getPosY() +
                32 * mapposi / scalingfactor,
                16, 16),
      true);
    game->getRenderer().setDrawColor(Color::Gray());
  }

  //renderdoors
  auto doorlist = room->getDoors();
  for (auto &obj : doorlist){
    game->getRenderer().setDrawColor(Color::Red());
    auto renderposx = obj->getCollisionRectWorld().getPosX() / scalingfactor + mapoffset.getPosX() + 32 * mapposj / scalingfactor;
    auto renderposy = obj->getCollisionRectWorld().getPosY() / scalingfactor + mapoffset.getPosY() + 32 * mapposi / scalingfactor;
    game->getRenderer().draw(
      Rectangle(renderposx,
                renderposy,
                16, 16),
      true);
    Texture(Text(obj->getSpecialString()).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(Point(renderposx, renderposy + 16));
    game->getRenderer().setDrawColor(Color::Gray());
    
  }

  //renderSaveStatues
  auto savelist = room->getSaveStatues();
  for (auto &obj : savelist){
    game->getRenderer().setDrawColor(Color::Blue());
    auto renderposx = obj->getCollisionRectWorld().getPosX() / scalingfactor + mapoffset.getPosX() + 32 * mapposj / scalingfactor;
    auto renderposy = obj->getCollisionRectWorld().getPosY() / scalingfactor + mapoffset.getPosY() + 32 * mapposi / scalingfactor;
    game->getRenderer().draw(
      Rectangle(renderposx,
                renderposy,
                16, 16),
      true);
    game->getRenderer().setDrawColor(Color::Gray());
    
  }
  

  std::list<std::string> text{
      "Robert",
      "Map: " + room->getName().substr(RF.getResourcePath("maps").length()),
      "Known Maps " + std::to_string(player->getKnownRooms().size()),
        "","","","","","","","","","",
      "Scroll map: left, right, up, down          Switch map: attack",
      "green = player, red = doors, blue = savepoint"  
  };

  int statOffset = 0;
  while (!text.empty())
  {
    Texture(Text(text.front()).toTexture(*RF.getRenderer(), RF.fontStat, Color::White())).render(Point(20, 20 + statOffset));
    text.pop_front();
    statOffset += 20;
  }
  // std::cout << "entering mapscreen with " << room->getName() << "\n";
  // std::cout << "player LastRoom " << player->getActualRoom() << "\n";
  // std::cout << room->getName().find(player->getActualRoom()) << "\n";

}

void GameStateMap::switchRoom(std::string roomtxt){
  room = RF.rooms.getResource(roomtxt);
}


void GameStateMap::gotoPauseMenu()
{
  game->transitionToPauseMenu();
}
