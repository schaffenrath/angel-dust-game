#include "Action.hpp"

Action::Action(unsigned int time) : childs(), ticker(), maxTime(time)
{
  ticker.start(maxTime);
}

void Action::addChild(TimeDependend::sPtr child)
{
  childs.push_front(std::move(child));
}

void Action::step(double timeStepMS)
{
  ticker.step(timeStepMS);
  for (TimeDependend::sPtr &child : childs)
  {
    child->step(timeStepMS);
  }
}

bool Action::done()
{
  return ticker.isDone();
}