#include "EventKey.hpp"

EventKey::EventKey(const SDL_Event event) : Event(EventType::Key)
{
  this->event = event;
}

const SDL_Event &EventKey::getInfo() const
{
  return event;
}
