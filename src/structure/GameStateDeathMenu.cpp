#include "GameStateDeathMenu.hpp"

#include <memory>
#include <iostream>
#include <string>

#include "EventKey.hpp"
#include "ResourceFacade.hpp"
#include "Text.hpp"
#include "Value.hpp"

GameStateDeathMenu::GameStateDeathMenu(Game *game, Room::sPtr room, Point cameraOffset) : GameState(game), room(room), cameraOffset(cameraOffset)
{
  
}

void GameStateDeathMenu::pollEvents(std::queue<Event::uPtr> &, double) {}

void GameStateDeathMenu::handleEvents(std::queue<Event::uPtr> &events, double timeStep)
{
  while (!events.empty())
  {
    auto evt = events.front().get();
    switch (evt->getEvent())
    {
    case EventType::Function:
      evt->handle(timeStep);
      break;
    case EventType::Key:
      const EventKey *key = static_cast<EventKey *>(evt);
      if (key->getInfo().type != SDL_KEYDOWN && key->getInfo().type != SDL_CONTROLLERBUTTONDOWN)
        break;
      if (key->getInfo().type == SDL_KEYDOWN){
        switch (key->getInfo().key.keysym.sym)
        {
        case SDLK_ESCAPE:
          game->transitionToMainMenu();
          break;
        case SDLK_RETURN:
          game->transitionToMainMenu();
          break;
        default:
          break;
        }
        
      }
      if (key->getInfo().type == SDL_CONTROLLERBUTTONDOWN) {
        switch (key->getInfo().cbutton.button)
        {
        case SDL_CONTROLLER_BUTTON_B:
          game->transitionToMainMenu();
          break;
        case SDL_CONTROLLER_BUTTON_A:
          game->transitionToMainMenu();
          break;
        default:
          break;
        }
      }
      break;
    };
    events.pop();
  }
}

void GameStateDeathMenu::render()
{
  room->renderDeath(-cameraOffset, angle++);
}
