#include "EventDelayed.hpp"

EventDelayed::EventDelayed(Event::uPtr &event_, double delayMS_, double whileMS_)
    : TimeDependend(), event(std::move(event_)), delayMs(delayMS_), whileMs(whileMS_)
{
}
EventDelayed::EventDelayed(Event::uPtr &&event, double delayMS, double whileMs) : EventDelayed(event, delayMS, whileMs) {}

void EventDelayed::step(double timeStepMS)
{
  if (delayMs < 0. && whileMs > 0.)
  {
    event->handle(timeStepMS);
    whileMs -= timeStepMS;
  }
  delayMs -= timeStepMS;
}