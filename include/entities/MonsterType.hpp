#pragma once

#include <memory>
#include <map>

#include "Sound.hpp"
#include "Moveable.hpp"
#include "EntityType.hpp"

class MonsterType : public EntityType
{
private:
  //std::map<AnimationState, Animation::sPtr> animationSet;


  //other info like HP, EXP etc.
  int maxHP = 1;
  int touchDmg = 0;
  int meleeDmg = 0;
  int rangeDmg = 0;
  int XPGain = 0;
  int range = 0;
  int dashRange = 0;
  double rangeSpeed = 0.1;
  bool rangeInvertible = false;
  bool invincible = false;
  bool flies = false;
  //Orientation baseOrientation = Orientation::Right;
  std::string bulletAnimation;
  std::string meleeAttackAnimation;
  std::string interaction;
  std::shared_ptr<Sound> hitsound;

public:
  typedef std::shared_ptr<MonsterType> sPtr;
  MonsterType(const std::string &name_);

  
  int getMaxHP() const { return maxHP; }
  int getTouchDmg() const { return touchDmg; }
  int getMeleeDmg() const { return meleeDmg; }
  int getXPGain() const { return XPGain; }
  int getRange() const { return range; }
  int getRangeDmg() const { return rangeDmg; }
  int getDashRange() const { return dashRange; }
  double getRangeVel() const { return rangeSpeed; }
  //Orientation getBaseOrientation() const { return baseOrientation; }
  bool isRangeInv() const { return rangeInvertible; };
  bool isFlying() const { return flies; };
  const std::string& getBulletAnimation() const { return bulletAnimation; }
  const std::string& getMeleeAttackAnimation() const { return meleeAttackAnimation; }
  Sound* getHitSound() const { return hitsound.get(); }
  bool isInvincible(){ return invincible; }

  //std::map<AnimationState,Animation> getAnimationCopy() const;
  //Animation& getAnimation(AnimationState state);

  std::string getInteraction() { return interaction; }

  static sPtr load(Renderer &ren, const std::string &file);
};
