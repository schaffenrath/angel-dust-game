#pragma once

#include "Renderable.hpp"
#include "Collisionable.hpp"
#include "ObjectType.hpp"
#include "Entity.hpp"

#include <memory>

class Room;

class Object : public Entity, public Collisionable
{
private:
  Point position;
  ObjectType::sPtr type;
  //Room *room = nullptr;
  std::string specialString;
  int lockedValue = 0;
  std::string myText{};

  void warp(const std::string warpTarget);

  bool interactionIsAllowed(Player *player);
  void showInteractionErrorText();

public:
  typedef std::unique_ptr<Object> uPtr;

  Object(ObjectType::sPtr type, const Point &pos, const std::string &switchName, const std::string &specialString);

  //void setRoom(Room* room){ this->room = room; }

  void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  //void invoke(std::queue<Event::uPtr> &events, double timeStep) override;

  Rectangle getCollisionRectWorld() const override;

  void onInteraction(Player *player);

  void handleCollision(const Player *) override;

  void removeMyself() override;

  std::string getSpecialString() {return specialString;};

};
