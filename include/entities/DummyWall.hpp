#pragma once

#include "Collisionable.hpp"
#include "Rectangle.hpp"

#include <memory>

class DummyWall : public Collisionable
{
private:
  Rectangle myArea;

public:
  typedef std::unique_ptr<DummyWall> uPtr;

  DummyWall(Rectangle myArea_) : myArea(myArea_){}
  Rectangle getCollisionRectWorld() const override  { return myArea; }

};