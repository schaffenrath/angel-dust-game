#pragma once

#include "Animation.hpp"
#include "AnimationState.hpp"

#include <map>
#include <string>

class EntityType
{
private:
  std::string name;

protected:
  std::map<AnimationState, Animation::sPtr> animationSet{};

public:
  EntityType(const std::string &name);

  const std::string &getName() const { return name; }
  std::map<AnimationState, Animation> getAnimationCopy() const;
  //Animation& getAnimation(AnimationState state);

  static void parseEntityData(EntityType *entity, const std::string &key, const std::string &value);
};