#pragma once

enum class AnimationState{
  idle,
  move,
  rangeAttack,
  meleeAttack,
  dash,
  dead
};