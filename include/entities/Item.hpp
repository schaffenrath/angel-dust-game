#pragma once

class Room;

#include "ItemType.hpp"
#include "Value.hpp"
#include "Renderable.hpp"
#include "Collisionable.hpp"
#include "Sprite.hpp"
#include "Player.hpp"
#include "Entity.hpp"

#include <vector>
#include <functional>

//const int DEFAULT_ITEM_VALUE = 34;

class Item : public Entity, public Collisionable
{
private:
  ItemType::sPtr type;
  Point position{0, 0};

public:
  typedef std::shared_ptr<Item> sPtr;
  typedef std::unique_ptr<Item> uPtr;

  Item(const ItemType::sPtr type_, const std::string &switchName);

  void moveToX(const int posX);
  void moveToY(const int posY);

  ItemCatagory getItemCatagory() const;
  int getItemValue() const;
  bool isConsumable() const;
  const std::string &getItemDescription() const;
  const std::string getItemShortDescription() const;
  void setPosition(Point position);
  std::string getItemString() const;

  // void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  // void invoke(std::queue<Event::uPtr> &events, double timeStep) override;

  void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;

  void handleCollision(const Player *) override;
  void handleCollision(const Monster *) override{};
  void handleCollision(const Attack *) override{};
  void handleCollision(const Item *) override{};
  Rectangle getCollisionRectWorld() const override { return Rectangle(position, getAnimation(actualState).getActualSize().getWidth(), getAnimation(actualState).getActualSize().getHeight()); };

  void removeMyself() override;
  static uPtr createItemFromString(const std::string &itemString);
};
typedef std::list<Item::uPtr> Inventory;
