#pragma once

#include "Point2.hpp"
#include "Rectangle.hpp"

enum class Orientation
{
  Left,
  Right,
  Up,
  Down
};

class Moveable
{
protected:
  VectorD position;
  VectorD velocity{0., 0.};
  Orientation orientation{Orientation::Down};
  Orientation orientationH{Orientation::Right};

  int factor;
  double jumpTime = 0;
  bool jumping = false;
  bool secondJump = false;
  bool secondJumpEnabled = false;
  bool gravity = true;
  bool fallThroughPlatform = false;

public:
  //Moveable(const Point &startPos, int factor = 1);
  Moveable(const Point &startPos);

  void move(double timeStep);
  void moveYOnly(double timeStep);
  void moveToPosY(const double posY);
  void moveToPosX(const double posX);
  Point testMove(const double timeStep) const;
  void startGoLeft();
  void startGoRight();
  void startGoUp(double gravity = 1.0);
  void startGoDown(double gravity = 1.0);
  void setVelocity(const VectorD &newVelocity);
  void setOrientation(Orientation orientation);
  void fallThrough();
  void stopFallThrough();

  VectorD getPosition();
  Rectangle getRectangleInWorld() const;
  Rectangle getRectangleInWorldTestMove(double timeStep) const;
  bool isGravityAffected() const { return gravity; }

  virtual const Rectangle getActualSize() const = 0;
  VectorD &getVelocity();

  void jump(double gravity = 1.0);
  void stopJump(double gravity = 1.0);
  void stopSecondJump();
  void enableSecondJump() { secondJumpEnabled = true; };
  bool isJumping() const { return jumping; };
  bool shouldFallThrough() const;
};
