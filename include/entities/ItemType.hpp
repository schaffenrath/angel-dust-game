#pragma once

#include <memory>
#include "EntityType.hpp"

const int DEFAULT_ITEM_VALUE = 34;

enum class ItemCatagory
{
  HP,
  XP,
  KEY,
  DOUBLEJUMP,
  AXE,
  LOCK,
  RING
};

class ItemType : public EntityType
{
private:
  ItemCatagory typeClass;
  std::string descripton{};
  std::string shortDescription{};
  int value;
  bool consumable;

public:
  typedef std::shared_ptr<ItemType> sPtr;
  ItemType(const std::string &name_);

  ItemCatagory getTypeClass() const { return typeClass; }
  int getValue() const { return value; }
  bool isConsumable() const { return consumable; }
  const std::string &getDesciption() const { return descripton; }
  const std::string &getShort() const { return shortDescription; }

  static sPtr load(Renderer &ren, const std::string file);
};
