#pragma once

#include <memory>
#include <queue>
#include <set>

#include "MonsterType.hpp"
#include "Moveable.hpp"
#include "Value.hpp"
#include "ItemType.hpp"
#include "Event.hpp"
#include "Renderable.hpp"
#include "Collisionable.hpp"
#include "Action.hpp"
#include "Entity.hpp"

class Attack;

class Room;

class Monster : public Entity, public Moveable, public Collisionable
{
private:
  //Room *room;
  MonsterType::sPtr type;
  Value<int> health;
  Action::uPtr pendingAction;
  std::string itemString;

  std::set<const Attack *> latestHits{};
  void mageKi(std::queue<Event::uPtr> &events, double timeStep);
  void wolfKi(std::queue<Event::uPtr> &events, double timeStep);
  const Point& newRndPosition(const std::vector<Point>& positions) const;

public:
  typedef std::unique_ptr<Monster> uPtr;

  Monster(const Point pos, MonsterType::sPtr type, const std::string &switchName, const std::string &itemString = "");

  int getTouchDmg() const;
  const Rectangle getActualSize() const;
  const MonsterType *getType() const;

  void removeFadingAttacks(const Attack *attack);

  void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  void invoke(std::queue<Event::uPtr> &events, double timeStep) override;

  void handleCollision(const Attack *attack) override;
  void handleCollision(const DummyWall *wall) override;
  Rectangle getCollisionRectWorld() const override;
  Rectangle getCollisionRectWorld(double timeStep) const override;

  VectorD getDistanceToPlayer() const;

  void onDeath(double timeStep);

  void removeMyself() override;
};
