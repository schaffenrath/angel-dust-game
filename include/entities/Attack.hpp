#pragma once

#include "Renderable.hpp"
#include "Moveable.hpp"
#include "Collisionable.hpp"
#include "Animation.hpp"
#include "Item.hpp"

#include <memory>
#include <set>
#include "SDL_render.h"

class Room;

class Attack : public Renderable, public Collisionable, public Moveable
{
private:
  Room *room;
  int touchDmg;
  Rectangle size;
  double lifetime;
  bool harmPlayer;
  bool harmMonster;
  bool invertible;
  bool deflects;
  bool persistent;
  Animation::sPtr attackAnim;

  std::set<const Attack *> latestHits{};

public:
  typedef std::unique_ptr<Attack> uPtr;

  Attack(Room *room, int touchDmg, const Rectangle &positionSize, double lifetime, bool harmPlayer = true,
         bool harmMonster = true, bool invertible = false, bool deflects = false, bool persistent = false);
  Attack(const Attack &) = default;
  Attack(Attack &&) = default;
  virtual ~Attack(){};

  void setAttackAnim(Animation::sPtr attackAnim);
  bool harmsPlayer() const { return harmPlayer; }
  bool harmsMonster() const { return harmMonster; }
  int getTouchDmg() const { return touchDmg; }

  void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  void invoke(std::queue<Event::uPtr> &events, double timeStep) override;

  const Rectangle getActualSize() const override;

  void handleCollision(const Player *player) override;
  void handleCollision(const Monster *monster) override;
  void handleCollision(const Attack *attack) override;
  void handleCollision(const DummyWall *wall) override;
  Rectangle getCollisionRectWorld() const override;
  Rectangle getCollisionRectWorld(double timeStep) const override;

  void removeFadingAttacks(const Attack *attack);
};