#pragma once

#include "Renderable.hpp"
#include "EntityType.hpp"
#include "Switch.hpp"

class Room;

class Entity : public Renderable
{
private:
  EntityType *type;
  std::string switchName;

protected:
  Room *room = nullptr;
  Switch::uPtr mySwitch{nullptr};
  std::map<AnimationState, Animation> animationSet;
  AnimationState actualState = AnimationState::idle;
  const Animation &getAnimation(AnimationState state) const;
  Animation &getAnimation(AnimationState state);

public:
  Entity(EntityType *type, const std::string &switchName);

  std::string getEntityString() const;

  void setRoom(Room *room);
  virtual void invoke(std::queue<Event::uPtr> &events, double timeStep) override;
  virtual void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;

  virtual void removeMyself() = 0;
};