#pragma once

#include "Rectangle.hpp"

class Player;
class Monster;
class Attack;
class Item;
class DummyWall;
class Object;

class Collisionable
{
public:
  void handleCollision(const Collisionable *other);
  virtual void handleCollision(const Player *) {}
  virtual void handleCollision(const Monster *) {}
  virtual void handleCollision(const Attack *) {}
  virtual void handleCollision(const Item *) {}
  virtual void handleCollision(const DummyWall *) {}
  virtual void handleCollision(const Object *) {}
  virtual Rectangle getCollisionRectWorld() const = 0;
  virtual Rectangle getCollisionRectWorld(double ) const { return getCollisionRectWorld(); }
};