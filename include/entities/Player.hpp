#pragma once

#include <memory>
#include <queue>
#include <list>
#include <set>

#include "Rectangle.hpp"
#include "Animation.hpp"
#include "Moveable.hpp"
#include "Event.hpp"
#include "Value.hpp"
#include "Action.hpp"
#include "Renderable.hpp"
#include "Collisionable.hpp"
#include "PersistentInfo.hpp"
#include "Attack.hpp"
#include "Item.hpp"
#include "Blinker.hpp"

class GameStateInGame;
class Player : public Moveable, public Renderable, public Collisionable
{
private:
  const int INVUL_TIME_MS = 1000;

  GameStateInGame *gameState;
  Room *room = nullptr;
  Animation::sPtr rest;
  Animation::sPtr go;
  Animation::sPtr attack;
  Animation::sPtr attackRange;
  Animation::sPtr superAttack;
  Animation *actualAnimation;
  Value<int> health;
  Value<int> experience;
  Value<double> superCharge;
  const Rectangle colSize;
  Action::uPtr pendingAction{};
  std::list<std::unique_ptr<Item>> inventory{};
  std::set<std::string> knownRooms{};

  bool interacts = false;
  bool axeEnabled = false;
  bool superActive = false;
  int attDmg;
  int rngDmg;
  int sprDmg = 50;
  int attDmgPreSuper;
  int rngDmgPreSuper;

  const double superChargeFactor = 0.002;

  Timer lastHit{};
  Timer superDuration{};
  Blinker hitBlinker{50};

  std::string actualRoom;

public:
  typedef std::shared_ptr<Player> sPtr;

  Player(GameStateInGame *gameState, const Point &startPosition, const PersistentInfo &persistent);
  void enableItemStats();

  void setRoom(Room *room_);

  void invoke(std::queue<Event::uPtr> &events, double timeStep) override;

  void startAttack();
  void startRangeAttack();
  void startSuperAttack();
  void stopSuperAttack();
  void interact();
  void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  void save(PersistentInfo &saveData);

  const Value<int> &getHealth() const { return health; }
  const Value<int> &getExp() const { return experience; }
  const Value<double> &getSuperCharge() const { return superCharge; }
  int getAttackDamage() const { return attDmg; }
  int getRangeDamage() const { return rngDmg; }
  void addExp(int newXP);
  void addHealth(int newHP);
  void addHealthPercent(int percent);
  void addItem(const ItemType::sPtr itemType, const int itemValue);
  void useKey(const int keyValue);
  void removeKey(const int keyValue);
  void enableAxe();
  //void setInteracting(bool val){ interacts = val; }
  bool isInteracting() const { return interacts; }

  std::list<std::unique_ptr<Item>> &getInventory();
  void removeItemFromInventory(const Item *toRemoveItem);
  std::string inventoryToString();
  void loadInventoryFromString(std::string savedString);

  std::string knownRoomsToString();
  std::set<std::string> &getKnownRooms();
  std::string getActualRoom();
  void loadRoomsFromString(std::string savedRooms);

  const Rectangle
  getActualSize() const override;
  void flipRoomGravity();

  void handleCollision(const Player *) override{};
  void handleCollision(const Monster *monster) override;
  void handleCollision(const Attack *attack) override;
  void handleCollision(const Item *item) override;
  void handleCollision(const Object *object) override;
  Rectangle getCollisionRectWorld() const override;
};
