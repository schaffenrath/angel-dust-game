#pragma once

#include "EntityType.hpp"

#include <memory>

enum class Interaction
{
  None,
  Heal,
  Warp,
  Text,
  Save,
  Take,
  Flip
};

class ObjectType : public EntityType
{
private:
  Interaction interactionType = Interaction::None;
  std::string text;
  bool onlyOnce = false;

public:
  typedef std::shared_ptr<ObjectType> sPtr;

  ObjectType(const std::string &name);

  Interaction getInteractionType() const { return interactionType; }
  const std::string &getText() const { return text; }
  bool isOnlyOnceInteractable() const { return onlyOnce; }

  static sPtr load(Renderer &ren, const std::string &file);
};
