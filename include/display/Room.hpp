#pragma once

#include "Music.hpp"
#include "Player.hpp"
#include "Rectangle.hpp"
#include "Sprite.hpp"
#include "Texture.hpp"
#include "Monster.hpp"
#include "Renderable.hpp"
#include "Attack.hpp"
#include "Item.hpp"
#include "DummyWall.hpp"
#include "Object.hpp"

#include <memory>
#include <string>
#include <vector>
#include <map>

class GameStateInGame;

class Room : public Renderable
{
private:
  std::string name;
  std::unique_ptr<std::vector<std::string>> structure;
  std::unique_ptr<std::map<char, Sprite::sPtr>> tilemap;
  Animation::sPtr background;
  Texture::sPtr foreground;
  Music::sPtr bgmusic;
  Rectangle size;
  Player::sPtr player;
  unsigned long maxlevel; //max amout of chars in loader layout lines
  int fganim;
  double gravity = 1.0;
  GameStateInGame *ingame;
  //std::list<std::string> text;
  std::list<Item::uPtr> items{};
  std::list<Object::uPtr> objects;
  std::list<Monster> monsters;
  std::list<Attack::uPtr> attacks{};

  std::list<Item *> toRemoveItems{};
  std::list<Attack *> toRemoveAttacks{};
  std::list<Monster *> toRemoveMonsters{};
  std::list<DummyWall::uPtr> tempWalls{};

  static const int TILESIZE = 32;

  bool areObjectsBellowAbove(const Rectangle &character, const VectorD &vel);
  bool areObjectsLeftRight(const Rectangle &character, const VectorD &vel);

  void removeAttack(Attack *attack);
  void removeMonster(Monster *monster);
  void removeItem(Item *item);

  static Point parsePosition(std::stringstream &ent);

public:
  typedef std::shared_ptr<Room> sPtr;

  Room(const std::string &name,
       std::unique_ptr<std::vector<std::string>> &structure,
       std::unique_ptr<std::map<char, Sprite::sPtr>> &tilemap,
       const Animation::sPtr background,
       const std::shared_ptr<Texture> foreround,
       const std::shared_ptr<Music> bgmusic,
       unsigned long maxlevel,
       std::list<Monster> &monsters,
       std::list<Object::uPtr> &objects);
  Room(std::unique_ptr<std::vector<std::string>> &structure);
  ~Room();

  void invoke(std::queue<Event::uPtr> &events, double timeStep) override;
  void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  void renderDeath(const Point &offset, const double angle, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);
  void renderMap(const Point &offset,int i_start, int j_start, double scalingFactor, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);

  void setPlayer(Player::sPtr player);
  Player *getPlayer();

  int getPixelWidth() const;
  int getPixelHeight() const;
  Point getFarestPoint() const;
  const std::string &getName() const { return name; }

  void checkWallCollisions(std::queue<Event::uPtr> &events, Collisionable *collisionable, double timeStep);
  void collisionRoom(Moveable *character, double timeStep);
  void itemGravity(Item *item, double timeStep);

  void moveItem(Item::uPtr &item);
  void addAttack(Attack::uPtr &&newAttack);
  void applyEffect(const std::string &type, const int value);
  /* void addText(std::string);
  void removeTextBox();
  size_t getTextSize() { return text.size(); }; */
  Item::uPtr transferItemFromRoom(const Item *item);
  void removeKey(const int keyValue) { player->removeKey(keyValue); };

  void screenShake(int intensity = 5);

  void removeObsoleteObjects();

  void stopMusic(){bgmusic->stop();};

  void setMusicVolume(int volume);

  void setIngame(GameStateInGame *_ingame);
  GameStateInGame *getIngame() { return ingame; }

  void addRemove(Monster *monster);
  void addRemove(Attack *attack);
  void addRemove(Object *object);
  void addRemove(Item *item);

  std::list<Object*> getDoors();
  std::list<Object*> getSaveStatues();

  double getGravity(){return gravity;};
  void flipGravity();

  static void parseSwitch(std::string &type, std::string &mSwitch);
  static sPtr load(Renderer &ren, std::string filename);
};
