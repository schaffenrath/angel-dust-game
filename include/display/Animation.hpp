#pragma once

#include "Sprite.hpp"
#include "TimeDependend.hpp"
#include "Renderable.hpp"

#include <vector>

class Animation : public TimeDependend, public Renderable
{
private:
  std::vector<Sprite::sPtr> sprites;
  std::vector<double> times;
  std::vector<Sprite::sPtr>::iterator actualSprite;
  std::vector<double>::iterator actualTime;
  double counter = 0.0;

public:
  void init();
  typedef std::shared_ptr<Animation> sPtr;

  Animation();
  Animation(const Animation &copy);
  Animation(std::vector<Sprite::sPtr> &sprites, std::vector<double> &times);
  void render(const Point &point, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  void invoke(std::queue<Event::uPtr> &, double ) override {}
  void step(double timeStepMS) override;

  void push(Sprite::sPtr sprite, double time);

  double getAnimationTime();

  static sPtr load(Renderer &ren, std::string filename);

  const Rectangle getActualSize() const;

  Animation& operator=(const Animation& anim);
};
