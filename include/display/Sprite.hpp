#pragma once

#include <memory>
#include "Texture.hpp"
#include "Rectangle.hpp"
#include "Point2.hpp"
#include "Renderable.hpp"

class Sprite : public Renderable
{
private:
  Rectangle area;
  Texture::sPtr texture;
  VectorF scaling{1.0f, 1.0f};
  SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE;

  
  void setScalingUniform(float val){ scaling.setPosX(val); scaling.setPosY(val); }
  void addFlip(int flip_) { flip = static_cast<SDL_RendererFlip>(flip | flip_); }

public:
  typedef std::shared_ptr<Sprite> sPtr;
  double getScalingX(){return scaling.getPosX();};
  double getScalingY(){return scaling.getPosY();};
  void setScalingX(float x){ scaling.setPosX(x); }
  void setScalingY(float y){ scaling.setPosY(y); }

  Sprite(const Texture::sPtr texture);
  Sprite(const Rectangle area, const Texture::sPtr texture);

  void render(const Point &point, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) override;
  void render(const Point &point, const double angle, const Point anglepoint, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);
  void invoke(std::queue<Event::uPtr> &, double ) override {}

  static sPtr load(Renderer &ren, const std::string &file);

  const Rectangle getActualSize() const;
};
