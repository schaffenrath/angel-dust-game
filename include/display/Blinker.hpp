#pragma once

#include "TimeDependend.hpp"

class Blinker : public TimeDependend{

  private:
    bool blink = false;
    bool isOn = false;
    double restTime;
    const double blinkTime;

  public:
    Blinker(double blinkTime);
    void setOn(){ isOn = true; }
    void setOff(){ isOn = false; }
    void step(double timeStepMS) override;
    bool getState() const;

};