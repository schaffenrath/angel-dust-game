#pragma once

#include <queue>
#include <SDL.h>
#include "Point2.hpp"
#include "Event.hpp"

class Renderable{
  public:
    virtual void render(const Point &offset, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE) = 0;
    virtual void invoke(std::queue<Event::uPtr> &events, double timeStep) = 0;
};