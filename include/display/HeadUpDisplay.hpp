#pragma once

#include <memory>

#include "Font.hpp"
#include "Texture.hpp"
#include "Sprite.hpp"
#include "Ticker.hpp"

class Renderer;
class Player;

class HeadUpDisplay
{
private:
  //GameStateMainMenu* gameState;
  Renderer *ren;
  const Player *player;
  Font uiFont;
  Texture textHP;
  Texture textEXP;
  Texture textATT;

  Sprite::sPtr textbox;
  Texture::sPtr someText{};
  Ticker textTimer{};
  int timeToPass = 0;

public:
  typedef std::unique_ptr<HeadUpDisplay> uPtr;

  //HeadUpDisplay(GameStateMainMenu* gameState_);
  HeadUpDisplay(Renderer &ren, const Player *player);

  void displayText(const std::string &text, int timeMS = 2000);
  void render();
};