#pragma once

#include <memory>
#include "Room.hpp"
#include "Renderer.hpp"
#include "Event.hpp"
#include "Point2.hpp"

class Camera{
  private:
    Room::sPtr room;
    Rectangle position;
    Rectangle playerArea;
    unsigned int shakeIntensity;
    bool followPlayer;

  public:
    Camera(const Renderer &ren);

    void pollEvents(std::queue<Event::uPtr> &events, double timeStep);
    void move(const int dx, const int dy = 0);
    void setRoom(const std::shared_ptr<Room> room);
    void clearRoom();
    void shake(const unsigned int intesity);
    void setPosition(const int x, const int y = 0);
    Point getPosition() {return Point(position.getPosX(), position.getPosY());};
    std::shared_ptr<Room> getRoom();
    void render();
};
