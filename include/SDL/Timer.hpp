#pragma once

#include "TimeDependend.hpp"

#include <memory>

class Timer : public TimeDependend{
  private:
    double restTime = 0;

  public:
    typedef std::shared_ptr<Timer> sPtr;

    Timer(){};
    void start(double timeDelay);
    virtual void step(double timeStepMS) override;
    bool isDone() const;

};