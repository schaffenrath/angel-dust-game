#pragma once
#include <SDL.h>
#include <SDL_mixer.h>
#include <string>
#include <memory>
#include "Renderer.hpp"

/* Class that handles playing music */

class Music
{

private:
    Mix_Music *music;

public:
    typedef std::shared_ptr<Music> sPtr;

    Music(const std::string &file);
    Music(const Music &copy) = delete;
    Music(Music &&copy);
    ~Music();

    bool isGood() const{ return music != nullptr; }
    void play(const int loops = 0);
    void pauseResume();
    void stop();
    void volume(const int volume);
    static sPtr load(Renderer &ren, const std::string filename);
};
