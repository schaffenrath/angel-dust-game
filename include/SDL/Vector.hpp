#pragma once

#include "SDL_rect.h"
#include <algorithm>

template <typename T>
class Vector
{
private:
  //SDL_Point point;
  T x;
  T y;

public:
  Vector(T x, T y);

  template <typename U>
  Vector(const Vector<U> &oth) : Vector(oth.getPosX(), oth.getPosY()) {}

  void setPosX(T value);
  void setPosY(T value);
  void addPosX(T value);
  void addPosY(T value);
  T getPosX() const { return x; }
  T getPosY() const { return y; }

  bool isZero() const { return x == 0 && y == 0; }

/*   template <typename U>
  Vector<U> operator+(const Vector<U> &othVector) const;
  template <typename U>
  Vector<U> operator-(const Vector<U> &othVector) const;
  template <typename U>
  Vector<U> operator*(U scalar) const;
  template <typename U>
  Vector<U> operator*(const Vector<U> &othVector) const;
  template <typename U>
  Vector<U> operator/(U scalar) const;
  Vector<T> operator-() const; */

  template <typename U>
  void operator+=(const Vector<U> &othVector);
  template <typename U>
  void operator-=(const Vector<U> &othVector);
  template <typename U>
  void operator*=(U scalar);
  template <typename U>
  void operator*=(const Vector<U> &othVector);
  template <typename U>
  void operator/=(U scalar);

  T dot(const Vector<T> &othVector) const;

  Vector<T> max(const Vector<T> &othVector) const;
  Vector<T> min(const Vector<T> &othVector) const;

  T length();
  void normalize();
};

template <typename T>
Vector<T>::Vector(T x_, T y_)
{
  x = x_;
  y = y_;
}

template <typename T>
void Vector<T>::setPosX(T value)
{
  x = value;
}

template <typename T>
void Vector<T>::setPosY(T value)
{
  y = value;
}

template <typename T>
void Vector<T>::addPosX(T value)
{
  x += value;
}

template <typename T>
void Vector<T>::addPosY(T value)
{
  y += value;
}

template <typename T>
template <typename U>
void Vector<T>::operator+=(const Vector<U> &othVector)
{
  x += othVector.getPosX();
  y += othVector.getPosY();
}

template <typename T>
template <typename U>
void Vector<T>::operator-=(const Vector<U> &othVector)
{
  y -= othVector.getPosY();
  x -= othVector.getPosX();
}

template <typename T>
template <typename U>
void Vector<T>::operator*=(U scalar)
{
  x *= scalar;
  y *= scalar;
}

template <typename T>
template <typename U>
void Vector<T>::operator*=(const Vector<U> &othVector)
{
  x *= othVector.getPosX();
  y *= othVector.getPosY();
}

template <typename T>
template <typename U>
void Vector<T>::operator/=(U scalar)
{
  x /= scalar;
  y /= scalar;
}

template <typename T>
Vector<T> Vector<T>::max(const Vector<T> &othVector) const
{
  return Vector<T>(std::max(x, othVector.getPosX()), std::max(y, othVector.getPosY()));
}

template <typename T>
Vector<T> Vector<T>::min(const Vector<T> &othVector) const
{
  return Vector<T>(std::min(x, othVector.getPosX()), std::min(y, othVector.getPosY()));
}

template <typename T>
T Vector<T>::length()
{
  return std::sqrt(x * x + y * y);
}

template <typename T>
void Vector<T>::normalize()
{
  T len = length();
  x /= len;
  y /= len;
}

template <typename T>
T Vector<T>::dot(const Vector<T> &othVector) const
{
  return x * othVector.getPosX() + y * othVector.getPosY();
}

//general operator functions for all type of vectors
template <typename T, typename U>
Vector<U> operator+(const Vector<T> &vec, const Vector<U> &othVector)
{
  return Vector<U>(vec.getPosX() + othVector.getPosX(), vec.getPosY() + othVector.getPosY());
}

template <typename T, typename U>
Vector<U> operator-(const Vector<T> &vec, const Vector<U> &othVector)
{
  return Vector<U>(vec.getPosX() - othVector.getPosX(), vec.getPosY() - othVector.getPosY());
}

template <typename T, typename U>
Vector<U> operator*(const Vector<T> &vec, U scalar)
{
  return Vector<U>(vec.getPosX() * scalar, vec.getPosY() * scalar);
}

template <typename T, typename U>
Vector<U> operator*(const Vector<T> &vec, const Vector<U> &othVector)
{
  return Vector<U>(vec.getPosX() * othVector.getPosX(), vec.getPosY() * othVector.getPosY());
}

template <typename T, typename U>
Vector<U> operator/(const Vector<T> &vec, U scalar)
{
  return Vector<U>(vec.getPosX() / scalar, vec.getPosY() / scalar);
}

template <typename T>
Vector<T> operator-(const Vector<T> &vec)
{
  return Vector<T>(-vec.getPosX(), -vec.getPosY());
}