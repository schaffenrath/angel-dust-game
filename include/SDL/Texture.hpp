#pragma once
#include <SDL.h>
#include <string>
#include "Renderer.hpp"
#include "Point2.hpp"
#include "Rectangle.hpp"
#include <memory>

class Texture
{

private:
  Renderer *renderer; 
  SDL_Texture *texture;

  int getCenterPos(int actualSize, int centerSize);

public:
  typedef std::shared_ptr<Texture> sPtr;
  typedef std::unique_ptr<Texture> uPtr;

  Texture(Renderer* renderer, const Uint32 format, const int access, const int w, const int h);
  Texture(Renderer* renderer, const std::string &file);
  Texture(Renderer* renderer, SDL_Texture* tex);
  Texture(const Texture &copy) = delete;
  Texture(Texture &&copy);
  ~Texture();

  bool isGood() const{ return texture != nullptr; }
  Rectangle getSize() const;

  int getCenterPosX(int size);
  int getCenterPosY(int size);

  void render(const Rectangle &dst, const Rectangle &clip, const double angle, const Point &center, const SDL_RendererFlip flip);
  void render(const Rectangle &dst, const Rectangle &clip, const SDL_RendererFlip flip);
  void render(const Rectangle &dst, const Rectangle &clip);
  void render(const Point &position, const Rectangle &clip);
  void render(const Point &position, const Rectangle &clip, const SDL_RendererFlip flip);
  void render(const Point &position, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE){ render(position, Rectangle(Point(0,0), getSize().getWidth(), getSize().getHeight()), flip); }

  static sPtr load(Renderer &ren, const std::string &filename);
};
