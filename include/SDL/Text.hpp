#pragma once
#include "Font.hpp"
#include "Texture.hpp"
#include "Color.hpp"

#include <string>
#include <memory>

class Text{

  private:
    std::string text;

  public:
    typedef std::unique_ptr<Text> uPtr;
    typedef std::shared_ptr<Text> sPtr;

    Text(const std::string &text);
    ~Text(){}
    Texture toTexture(Renderer &renderer, const Font& font, const Color &color);

};