#pragma once
#include <SDL_rect.h>
#include <memory>

#include "Point2.hpp"

class Rectangle
{
private:
  SDL_Rect rec;
  //Rectangle(){};

public:
  typedef std::shared_ptr<Rectangle> sPtr;

  Rectangle(){};
  Rectangle(const Point &position, const int w, const int h);
  Rectangle(const Point &position, const Point &otherCorner);
  Rectangle(const int x, const int y, const int w, const int h);
  Rectangle(const SDL_Rect rec);

  Rectangle(const Rectangle &oth) = default;
  Rectangle(Rectangle &&oth) = default;

  Rectangle& operator=(const Rectangle &oth) = default;
  Rectangle& operator=(Rectangle &&oth) = default;

  SDL_bool intersects(const Rectangle &otherRectangle);
  Rectangle getIntersetion(const Rectangle &otherRectangle);

  void setPosX(const int x);
  void setPosY(const int y);
  void setWidth(const int w);
  void setHeight(const int h);
  int getPosX() const { return rec.x; }
  int getPosY() const { return rec.y; }
  int getWidth() const { return rec.w; }
  int getHeight() const { return rec.h; }
  VectorI getSize() const;

  Point getBase() const;
  Point getOppositePoint() const;
  Point getCenter() const;

  void move(const int dx, const int dy = 0);

  void render();

  const SDL_Rect *operator*() const
  {
    return &rec;
  }

  void operator+=(const Point offset);
  void operator-=(const Point offset);
};