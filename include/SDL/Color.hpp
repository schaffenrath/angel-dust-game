#pragma once

#include "SDL_render.h"

struct Color
{
  const unsigned char red;
  const unsigned char green;
  const unsigned char blue;
  const unsigned char alpha;

  Color(unsigned char red, unsigned char green, unsigned char blue) : Color(red, green, blue, 255) {}
  Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) : red(red), green(green), blue(blue), alpha(alpha) {}

  static const Color &White();
  static const Color &Blue();
  static const Color &Red();
  static const Color &Green();
  static const Color &Black();
  static const Color &Gray();

  SDL_Color toSDL() const { return SDL_Color{red, green, blue, alpha}; }
};