#pragma once

#include "Vector.hpp"

template <>
class Vector<int>
{
private:
  //SDL_Point point;
  SDL_Point pt;

public:
  Vector(int x_, int y_) : pt()
  {
    pt.x = x_;
    pt.y = y_;
  }

  template <typename U>
  Vector(const Vector<U> &oth) : Vector(oth.getPosX(), oth.getPosY()) {}

  void setPosX(int value)
  {
    pt.x = value;
  }
  void setPosY(int value)
  {
    pt.y = value;
  }
  void addPosX(int value)
  {
    pt.x += value;
  }
  void addPosY(int value)
  {
    pt.y += value;
  }

  int getPosX() const { return pt.x; }
  int getPosY() const { return pt.y; }

  const SDL_Point *operator*() const
  {
    return &pt;
  }

  bool isZero() const { return pt.x == 0 && pt.y == 0; }

/*   template <typename U>
  Vector<U> operator+(const Vector<U> &othVector) const
  {
    return Vector<U>(pt.x + othVector.getPosX(), pt.y + othVector.getPosY());
  }
  template <typename U>
  Vector<U> operator-(const Vector<U> &othVector) const
  {
    return Vector<U>(pt.x - othVector.getPosX(), pt.y - othVector.getPosY());
  }
  template <typename U>
  Vector<U> operator*(U scalar) const
  {
    return Vector<U>(pt.x * scalar, pt.y * scalar);
  }
  template <typename U>
  Vector<U> operator/(U scalar) const
  {
    return Vector<U>(pt.x / scalar, pt.y / scalar);
  }
  Vector<int> operator-() const
  {
    return Vector<int>(-pt.x, -pt.y);
  } */

  template <typename U>
  void operator+=(const Vector<U> &othVector)
  {
    pt.x += othVector.getPosX();
    pt.y += othVector.getPosY();
  }
  template <typename U>
  void operator-=(const Vector<U> &othVector)
  {
    pt.y -= othVector.getPosY();
    pt.x -= othVector.getPosX();
  }
  template <typename U>
  void operator*=(U scalar)
  {
    pt.x *= scalar;
    pt.y *= scalar;
  }

  template <typename U>
  void operator/=(U scalar)
  {
    pt.x /= scalar;
    pt.y /= scalar;
  }

  Vector<int> max(const Vector<int> &othVector) const
  {
    return Vector<int>(std::max(pt.x, othVector.getPosX()), std::max(pt.y, othVector.getPosY()));
  }
  Vector<int> min(const Vector<int> &othVector) const
  {
    return Vector<int>(std::min(pt.x, othVector.getPosX()), std::min(pt.y, othVector.getPosY()));
  }

  int dot(const Vector<int> &othVector) const
  {
    return pt.x * othVector.getPosX() + pt.y * othVector.getPosY();
  }
};

/* template<>
Vector<int>::Vector(int x_, int y_) : pt()
{
  pt.x = x_;
  pt.y = y_;
} */

/*const SDL_Point *Vector<int>::operator*() const

void Vector<int>::setPosX(int value)

void Vector<int>::setPosY(int value)

void Vector<int>::addPosX(int value)

void Vector<int>::addPosY(int value)
template<>
template <typename U>
Vector<U> Vector<int>::operator+(const Vector<U> &othVector) const

template<>
template <typename U>
Vector<U> Vector<int>::operator-(const Vector<U> &othVector) const

template<>
template <typename U>
Vector<U> Vector<int>::operator*(U scalar) const
template<>
template <typename U>
Vector<U> Vector<int>::operator/(U scalar) const

template<>
template <typename U>
void Vector<int>::operator+=(const Vector<U> &othVector)

template<>
template <typename U>
void Vector<int>::operator-=(const Vector<U> &othVector)

Vector<int> Vector<int>::operator-() const

Vector<int> Vector<int>::max(const Vector<int> &othVector) const

Vector<int> Vector<int>::min(const Vector<int> &othVector) const */

using VectorI = Vector<int>;
using VectorF = Vector<float>;
using VectorD = Vector<double>;

using Point = Vector<int>;