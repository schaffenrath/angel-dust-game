#pragma once
#include <SDL_ttf.h>
#include <string>
#include <memory>

class Font
{
private:
  TTF_Font *font = nullptr;

public:
  typedef std::shared_ptr<Font> sPtr;

  Font(){};
  Font(const std::string &fontFile, const int fontSize);
  Font(const Font &oth) = delete;
  Font(Font &&oth);
  ~Font();

  Font& operator=(const Font &oth) = delete;
  Font& operator=(Font &&oth);
  TTF_Font *operator*() const { return font; }
  
};