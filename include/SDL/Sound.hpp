#pragma once

#include <SDL_mixer.h>
#include <string>
#include <memory>
#include "Renderer.hpp"

class Sound
{

private:
  Mix_Chunk *sound;

public:
  typedef std::shared_ptr<Sound> sPtr;

  Sound(const std::string &file);
  Sound(const Sound &copy) = delete;
  Sound(Sound &&copy);
  ~Sound();

  bool isGood() const{ return sound != nullptr; }
  void play(const int channel = -1, const int loops = 0);
  static sPtr load(Renderer &ren, const std::string filename);
};
