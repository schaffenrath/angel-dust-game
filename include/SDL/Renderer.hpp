#pragma once
#include <SDL.h>
#include "Window.hpp"
#include <memory>
#include "Color.hpp"

class Renderer
{
private:
  SDL_Renderer *ren;
  Window::sPtr window;

public:
  typedef std::shared_ptr<Renderer> sPtr;

  Renderer(Window::sPtr window, const int index, const Uint16 flags);
  ~Renderer();

  SDL_Renderer *operator*() const { return ren; }
  void setLogicalSize(const int xPixels, const int yPixels);

  int getPixelWidth() const;
  int getPixelHeight() const;

  void setDrawColor(const Color& color);

  void draw(const Rectangle &rect, bool fill = false);
  void draw(const Point &pt);

  void clear();
  void present();
};