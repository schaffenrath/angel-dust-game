#pragma once
#include <string>
#include <SDL.h>
#include "Rectangle.hpp"
#include <memory>

class Window
{
private:
  SDL_Window *window;
  Rectangle size;

public:
  typedef std::shared_ptr<Window> sPtr;

  Window(const std::string &windowText, const int x, const int y, const int width, const int height, const Uint16 flags);
  ~Window();

  SDL_Window *operator*() const { return window; }

  int getPixelWidth() const;
  int getPixelHeight() const;

  static float getCurrentDisplayRatio();

  //Renderer createRenderer(const int index = -1, const Uint16 flags = 0);
};