#pragma once

#include <memory>

class Ticker{
  private:
    unsigned int startTime;

  public:
    typedef std::shared_ptr<Ticker> sPtr;

    Ticker(){};
    void start();
    unsigned int getTicks() const;
    bool areTicksPassed(const unsigned int ticks) const;
    void delay(const unsigned int totalTicks) const;

};