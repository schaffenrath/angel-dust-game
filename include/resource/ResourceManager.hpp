#pragma once

#include <map>
#include <string>
#include <algorithm>
#include <memory>
#include "Renderer.hpp"

template <class T, bool bufferOn = true>
class ResourceManager
{
private:
  std::map<std::string, std::shared_ptr<T>> buffer;
  std::string path;
  Renderer::sPtr ren;

public:
  ResourceManager(const std::string path) : buffer(), path(path){};

  std::shared_ptr<T> getResource(const std::string &keyName);
  void releaseResource(const std::string &keyName);
  void setRenderer(Renderer::sPtr ren);
  //Renderer* getRenderer();
  std::string &checkString(std::string &&aString);
};

template <class T, bool bufferOn>
std::shared_ptr<T> ResourceManager<T, bufferOn>::getResource(const std::string &keyName)
{
  if constexpr (bufferOn)
  {
    if (buffer.find(keyName) == buffer.end())
    {
      buffer.insert(std::pair<std::string, std::shared_ptr<T>>(keyName, T::load(*ren, checkString(path + keyName))));
    }
    return buffer[keyName];
  }
  else
  {
    return T::load(*ren, checkString(path + keyName));
  }
}

template <class T, bool bufferOn>
void ResourceManager<T, bufferOn>::releaseResource(const std::string &keyName)
{
  if constexpr (bufferOn)
  {
    buffer.erase(keyName);
  }
}

template <class T, bool bufferOn>
void ResourceManager<T, bufferOn>::setRenderer(Renderer::sPtr ren)
{
  this->ren = std::move(ren);
}

/* template <class T, bool bufferOn>
Renderer* ResourceManager<T, bufferOn>::getRenderer()
{
  return ren.get();
} */

template <class T, bool bufferOn>
std::string &ResourceManager<T, bufferOn>::checkString(std::string &&aString)
{
#ifdef _WIN32
  const char lookFor = '/';
  const char shouldBe = '\\';
#else
  const char shouldBe = '/';
  const char lookFor = '\\';
#endif

  while (aString.find(lookFor) != std::string::npos)
  {
    aString.replace(aString.find(lookFor), 1, &shouldBe, 1);
  }
  return aString;
}