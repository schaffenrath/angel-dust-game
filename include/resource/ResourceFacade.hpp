#pragma once

#include "ResourceManager.hpp"
#include "Texture.hpp"
#include "Sprite.hpp"
#include "Renderer.hpp"
#include "MonsterType.hpp"
#include "ObjectType.hpp"
#include "Sound.hpp"
#include "Music.hpp"
#include "Room.hpp"
#include "Item.hpp"
#include "Font.hpp"

#include <memory>

class ResourceFacade
{
public:
  static ResourceFacade &getI();
  //static ResourceFacade Instance;

  Renderer::sPtr actualRenderer;

  ResourceManager<Sprite> sprites;
  ResourceManager<Texture> texture;
  ResourceManager<MonsterType> monsterTypes;
  ResourceManager<ItemType> itemTypes;
  ResourceManager<ObjectType> objectTypes;
  ResourceManager<Sound> sounds;
  ResourceManager<Music> musics;
  ResourceManager<Room, false> rooms;
  ResourceManager<Animation, false> animations;
  std::string fontsPath;

  Font fontMenu;
  //Font fontPause;
  Font fontStat;
  Font fontText;

  ResourceFacade() : actualRenderer(),
                     sprites(getResourcePath("sprites")),
                     texture(getResourcePath("textures")),
                     monsterTypes(getResourcePath("monsters")),
                     itemTypes(getResourcePath("items")),
                     objectTypes(getResourcePath("objects")),
                     sounds(getResourcePath("sounds")),
                     musics(getResourcePath("musics")),
                     rooms(getResourcePath("maps")),
                     animations(getResourcePath("animations")),
                     fontsPath(getResourcePath("fonts"))
                     /* fontMenu(fontsPath + "statFont.ttf", 24),
                     //fontPause(fontsPath + "pauseFont.ttf", 18),
                     fontStat(fontsPath + "statFont.ttf", 16),
                     fontText(fontsPath + "statFont.ttf", 20) */
  {
  }

  ResourceFacade(const ResourceFacade &) = delete;
  void operator=(const ResourceFacade &) = delete;

  void init(Renderer::sPtr ren);

  Renderer *getRenderer() const;
  //ResourceManager<Texture> &textures();
  //ResourceManager<Sprite> &sprites();

  static std::string getResourcePath(const std::string &subDir = "");
};

extern ResourceFacade RF;