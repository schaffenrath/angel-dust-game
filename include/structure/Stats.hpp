#pragma once

#include "Value.hpp"

class Stats{
private:
  Value<int> lvl;
  Value<int> health;
  Value<int> experience;

public:
  Stats();
  ~Stats();

  void giveExp(const int amount);

  const Value<int>& getLvl() const { return lvl; }
  const Value<int>& getHealth() const { return health; }
  const Value<int>& getExp() const { return experience; }
};
