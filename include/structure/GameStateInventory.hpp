#pragma once

#include "Game.hpp"
#include "Texture.hpp"
#include "Player.hpp"
#include "Item.hpp"

#include <vector>
#include <functional>

class GameStateInventory : public GameState
{
private:
  class Entry
  {
  private:
    Texture::uPtr textTexture;
    Item *item;
    std::function<void()> callback;
    bool isEntryText;

  public:
    Entry(const std::string textS, std::function<void()> callback);
    Entry(Item *item, std::function<void()> callback);
    void run();
    void render(const Point offset);
    void render(const int width, const int offset);
    Texture *getTexture();
    Item *getItem() { return item; };
    bool isText() { return isEntryText; };
    void adjustOffsetByHeight(int &offset);
    void adjustOffset(Point &offset, Sprite::sPtr frameSize);
  };

  std::vector<Entry> entries;
  std::vector<Entry>::iterator actualEntry;
  Player::sPtr player;

  void up();
  void down();
  void left();
  void right();
  void enter();

public:
  GameStateInventory(Game *game, Player::sPtr player);
  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  void render() override;

  void useItem(Item *item);

  void gotoPauseMenu();
};
