#pragma once
#include <string>

class PersistentValue
{
private:
  std::string value;

public:
  PersistentValue() : value(){};
  PersistentValue(const std::string &init) : value(init){};
  ~PersistentValue(){};

  //void setValue(const std::string &key, const T value);
  std::string get() const;
  float getFloat() const;
  bool getBool() const;
  int getInt() const;
  double getDouble() const;

  PersistentValue &operator=(bool f);
  PersistentValue &operator=(float f);
  PersistentValue &operator=(int i);
  PersistentValue &operator=(const std::string &string);
};