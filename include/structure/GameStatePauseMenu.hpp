#pragma once

#include "Game.hpp"
#include "Texture.hpp"
#include "Player.hpp"
#include "MenuEntry.hpp"
#include "Room.hpp"

#include <vector>
#include <functional>

class GameStatePauseMenu : public GameState
{
private:
  std::vector<MenuEntry> entries;
  std::vector<MenuEntry>::iterator actualEntry;
  Player::sPtr player;

  void up();
  void down();
  void enter();

public:
  GameStatePauseMenu(Game *game);
  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  void render() override;

  void setPlayer(Player::sPtr player_) { player = player_; }

  void continueGame();
  void openInventory();
  void openMap();
  void restart();
  void gotoMainMenu();
};
