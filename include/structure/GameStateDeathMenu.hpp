#pragma once

#include "Game.hpp"
#include "GameStateInGame.hpp"
#include "Room.hpp"
#include "Texture.hpp"

#include <vector>
#include <functional>

class GameStateDeathMenu : public GameState
{
private:
  Room::sPtr room;
  double angle = 0;
  Point cameraOffset;

public:
  GameStateDeathMenu(Game *game, Room::sPtr room, Point cameraOffset);
  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  void render() override;
};
