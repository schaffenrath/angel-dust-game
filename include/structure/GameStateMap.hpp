#pragma once

#include "Game.hpp"
#include "Room.hpp"
#include "Texture.hpp"
#include "Player.hpp"

#include <vector>
#include <functional>

class GameStateMap : public GameState
{
private:
  Player::sPtr player;
  Room::sPtr room;
  std::set<std::string>::iterator mapIterator;
  int mapposi=0;
  int mapposj=0;

public:
  GameStateMap(Game *game, Player::sPtr player, Room::sPtr room);
  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  void render() override;
  void switchRoom(std::string room);

  void gotoPauseMenu();
};
