#pragma once

#include "Game.hpp"
#include "Texture.hpp"
#include "MenuEntry.hpp"
#include "Timer.hpp"

#include <vector>
#include <functional>
#include <string>
#include <list>

class GameStateEnd : public GameState
{
private:
  std::list<std::string> finalText = {"Thank you for playing", "We knew you could do it!", "What a journey", "It wasn't easy", "But it's over now", "Well done", "You Rock!", "Developed by: ", "Daniel Gogl", "Markus Kopp", "Robert Schaffenrath", "Music composed by:", "Gernot Baumgartner"};

  std::list<std::string> displayText;

  void enter();
  Timer steadyText{};

public:
  GameStateEnd(Game *game);

  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  void render() override;

  void gotoMainMenu();
};