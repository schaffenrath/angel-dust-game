#pragma once

#include <memory>

enum class EventType{
  Key,
  Function,
  Collision,
};

class Event{
  private:
    EventType type;

  public:
    typedef std::unique_ptr<Event> uPtr;

    Event(EventType type) : type(type){}
    virtual ~Event(){}

    EventType getEvent() const { return type; }
    virtual void handle(double ){}

};
