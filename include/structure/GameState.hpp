#pragma once

#include "Event.hpp"
//#include "Game.hpp"

#include <queue>
#include <memory>

class Game;

class GameState{
  protected:
    //std::queue<Event> events;
    Game *game;

  public:
    typedef std::unique_ptr<GameState> uPtr;

    GameState(Game* game) : game(game){}
    virtual ~GameState(){};

    Game *getGame(){ return game; }

    virtual void pollEvents(std::queue<Event::uPtr> &events, double timeStep) = 0;
    virtual void handleEvents(std::queue<Event::uPtr> &events, double timeStep) = 0;

    virtual void render() = 0;
};
