#pragma once

#include <string>
#include <memory>

class PersistentInfo;
class PersistentValue;

/*!
@brief Checks whether a value is set in the persistent info or not

If the value is not stored yet, it will be set default as false
*/
class Switch{
  private:
    std::string name;
    PersistentValue *myInfo = nullptr;

  public:
    typedef std::unique_ptr<Switch> uPtr;

    Switch(const std::string &name, PersistentInfo *info);

    bool isOn() const;
    void setOn();
    void setOff();
};