#pragma once

#include <map>
#include <string>
#include <memory>

#include "PersistentValue.hpp"

class PersistentInfo
{
private:
  std::map<std::string, PersistentValue> values;
  PersistentValue empty{""};
  PersistentInfo();
  PersistentInfo(std::map<std::string, PersistentValue> &values);

public:
  typedef std::unique_ptr<PersistentInfo> uPtr;
  //
  PersistentInfo(const PersistentInfo &) = default;
  PersistentInfo(PersistentInfo &&) = default;

  PersistentInfo &operator=(const PersistentInfo &) = default;
  PersistentInfo &operator=(PersistentInfo &&) = default;

  static PersistentInfo::uPtr loadFromFile(const std::string &filename);

  const PersistentValue &operator[](const std::string &key) const;
  PersistentValue &operator[](const std::string &key);

  const std::map<std::string, PersistentValue> &getValues() const;

  /* template <typename T>
  void setValue(const std::string &key, const T value);
  std::string getValueString(const std::string &key) const;
  float getValueF(const std::string &key) const;
  bool getValueB(const std::string &key) const;
  int getValueI(const std::string &key) const;
  double getValueD(const std::string &key) const; */
};

std::ostream &operator<<(std::ostream &stream, const PersistentInfo &info);

/* template <typename T>
void PersistentInfo::setValue(const std::string &key, const T value)
{
  values[key] = std::to_string(value);
} */