#pragma once

#include "TimeDependend.hpp"
#include "Event.hpp"

class EventDelayed : public TimeDependend
{
private:
  Event::uPtr event;
  double delayMs;
  double whileMs;

public:
  EventDelayed(Event::uPtr &event, double delayMS, double whileMs = 0.1);
  EventDelayed(Event::uPtr &&event, double delayMS, double whileMs = 0.1);
  EventDelayed(const EventDelayed& cpy) = delete;
  EventDelayed(EventDelayed&& mv) = default;
  EventDelayed& operator=(const EventDelayed& cpy) = delete;
  EventDelayed& operator=(EventDelayed&& mv) = default;
  virtual ~EventDelayed(){}
  void step(double timeStepMS) override;
};