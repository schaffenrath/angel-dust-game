#pragma once

#include <algorithm>

template <typename T>
class Value
{
private:
  T value;
  T maxValue;
  T minValue;

public:
  Value(T startValue) : Value(startValue, startValue, 0) {}
  Value(T maxValue, T startValue) : Value(maxValue, startValue, 0) {}
  Value(T maxValue, T startValue, T minValue) : value(startValue), maxValue(maxValue), minValue(minValue) {}

  void set(T value_) { value = std::min(value_, maxValue); }
  T get() const { return value; }
  T getMax() const { return maxValue; }
  T getMin() const { return minValue; }
  bool isMin() const { return value <= minValue; }
  bool isMax() const { return value >= maxValue; }

  void operator+=(const T increase);
  void operator-=(const T decrease);

  void reset();

  void increaseMax(const T increase);
template<typename U>
  void changeMaxFactor(const U factor);
  void decreaseMax(const T decrease);
};

template <typename T>
void Value<T>::operator+=(const T increase)
{
  value += increase;
  value = std::min(value, maxValue);
}

template <typename T>
void Value<T>::operator-=(const T decrease)
{
  value -= decrease;
  value = std::max(value, minValue);
}

template <typename T>
void Value<T>::increaseMax(const T increase)
{
  maxValue += increase;
  value += increase;
}

template <typename T>
void Value<T>::decreaseMax(const T decrease)
{
  maxValue -= decrease;
  value -= decrease;
}

template <typename T>
template <typename U>
void Value<T>::changeMaxFactor(const U factor){
  maxValue *= factor;
  value = std::min(maxValue, value);
}

template <typename T>
void Value<T>::reset(){
  value = minValue;
}
