#pragma once

#include "Event.hpp"
#include "TimeDependend.hpp"
#include <functional>

class EventFunction : public Event, public TimeDependend{
  private:
    std::function<void(double)> callback;

  public:
    EventFunction(std::function<void(double)> callback_) : Event(EventType::Function), callback(callback_){};
    void handle(double timeStep) override { callback(timeStep); }
    void step(double timeStep) override { callback(timeStep); }

};