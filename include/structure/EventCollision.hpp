#pragma once

#include "Event.hpp"
#include "Collisionable.hpp"

class EventCollision : public Event{
private:
  Collisionable *a;
  Collisionable *b;

public:
    EventCollision(Collisionable* a, Collisionable* b);

    void handle(double timeStep) override;
};
