#pragma once

#include "Event.hpp"
#include "SDL_events.h"

class EventKey : public Event{
  private:
    SDL_Event event;

  public:
    EventKey(const SDL_Event event);

    const SDL_Event& getInfo() const;
};
