#pragma once

#include <memory>

class TimeDependend{
  private:
    
  public:
    typedef std::shared_ptr<TimeDependend> sPtr;

    virtual ~TimeDependend(){}
    virtual void step(double timeStepMS) = 0;
};
