#pragma once

#include "Texture.hpp"

#include <functional>

class MenuEntry
{
private:
  Texture::uPtr textTexture;
  std::function<void()> callback;

public:
  MenuEntry(const std::string textS, std::function<void()> callback);
  void run();
  void render(const int offset, const int width);
  Texture *getTexture();
  void adjustOffsetByHeight(int &offset);
};