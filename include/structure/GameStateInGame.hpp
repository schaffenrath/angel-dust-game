#pragma once

#include "Game.hpp"
#include "Player.hpp"
#include "Camera.hpp"
#include "EventKey.hpp"
#include "HeadUpDisplay.hpp"
#include "PersistentInfo.hpp"

class GameStateInGame : public GameState
{
private:
  Player::sPtr player;
  //Room::sPtr room;
  Camera camera;
  HeadUpDisplay::uPtr hud;
  PersistentInfo::uPtr persistenceData;
  Transition paused;

  Point lvlChangePoint{0, 0};
  std::string newLevel;

  void changeLevel(Point entryPoint, const std::string &map);

public:
  GameStateInGame(Game *game, PersistentInfo::uPtr &&gameData);

  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  bool handleEventKey(const EventKey *EventKey);

  PersistentInfo *getPersistentData() { return persistenceData.get(); };

  void registerLevelChange(Point entryPoint, const std::string &map);

  void screenShake(int intensity);

  void quicksave();

  void render() override;

  Player::sPtr getPlayer();
  Room::sPtr getRoom();
  std::shared_ptr<Camera> getCamera() {return std::make_shared<Camera>(camera);};
  HeadUpDisplay* getHUD(){ return hud.get(); };
};
