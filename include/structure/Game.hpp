#pragma once

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "Window.hpp"
#include "Renderer.hpp"
#include "Ticker.hpp"
#include "GameState.hpp"
#include "Player.hpp"
#include "Event.hpp"

#include <iostream>
#include <memory>

enum class Transition
{
  None,
  NewGame,
  LoadGame,
  Pause,
  Unpause,
  ToMainMenu,
  ToPauseMenu,
  Inventory,
  Continue,
  Map,
  ToDeath,
  End
  //changeLevel
};

class Game
{
private:
  Window::sPtr win;
  Renderer::sPtr ren;
  GameState *actualState;
  GameState::uPtr mainMenu;
  GameState::uPtr pauseMenu;
  GameState::uPtr inventory;
  GameState::uPtr map;
  GameState::uPtr inGame;
  GameState::uPtr deathMenu;
  GameState::uPtr end;
  Ticker ticker;
  Transition transition;
  std::string loadFile;
  double dt = 1000.0 / 60.0;
  SDL_Haptic *haptic;

  bool quit = false;

  void gameLoopStep();
  void handleEvent(std::queue<Event::uPtr> &events);

public:
  Game();
  ~Game();

  void init(bool fullscreen = false);
  void gameLoop();

  void transitionNewGame();
  void transitionLoadGame(const std::string &loadFile);
  void transitionToMainMenu();
  void transitionToPauseMenu();
  void transitionToInventory();
  void transitionContinue();
  void transitionToPause();
  void transitionToUnpause();
  void transitionToDeath();
  void transitionToMap();
  void transitionToEnd();
  //void transitionToChangeLevel();

  GameState *checkStateTransition();

  Window &getWindow();
  Renderer &getRenderer();
  SDL_Haptic *getHaptic();

  const Window &getWindow() const;
  const Renderer &getRenderer() const;

  void doQuit(double timeStep);
};
