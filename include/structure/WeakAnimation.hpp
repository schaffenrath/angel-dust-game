#pragma once

#include "TimeDependend.hpp"
#include "Animation.hpp"

class WeakAnimation : public TimeDependend{
  private:
    Animation* animation;

  public:
    WeakAnimation(Animation* animation_) : animation(animation_){}
    ~WeakAnimation(){}
    
    void step(double timeStepMS) override { animation->step(timeStepMS); }
};