#pragma once

#include "Game.hpp"
#include "Texture.hpp"
#include "MenuEntry.hpp"

#include <vector>
#include <functional>

class GameStateMainMenu : public GameState
{

private:
  std::vector<MenuEntry> entries;
  std::vector<MenuEntry>::iterator actualEntry;
  Texture::sPtr titlescreen;

  void up();
  void down();
  void enter();

public:
  GameStateMainMenu(Game *game);

  void pollEvents(std::queue<Event::uPtr> &events, double timeStep) override;
  void handleEvents(std::queue<Event::uPtr> &events, double timeStep) override;

  void render() override;

  void startNewGame();
  void loadGame();
  void exitGame();
};
