#pragma once

#include <list>

#include "TimeDependend.hpp"
//#include "Ticker.hpp"
#include "Timer.hpp"

class Action : public TimeDependend{
private:
  std::list<TimeDependend::sPtr> childs;
  //Ticker ticker;
  Timer ticker;
  unsigned int maxTime;

public:
  typedef std::unique_ptr<Action> uPtr;

  Action(unsigned int time);
  void addChild(TimeDependend::sPtr child);
  virtual void step(double timeStepMS) override;
  bool done();

};
